//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Oct 27 09:02:16 2017 by ROOT version 6.10/04
// from TTree Pythia8Jets/Pythia8 particle data.
// found on file: pjets_pythia8_dijet_1000000.root
//////////////////////////////////////////////////////////

#ifndef Pythia8Jets_h
#define Pythia8Jets_h
#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TMath.h>
#include <TGraph.h>
#include <TLine.h>
#include <TText.h>
#include <TF1.h>
#include <TMatrixD.h>

// Header file for the classes stored in the TTree if any.
#include <vector>
#include <iostream>
#include <map>
#include <string>
#include <algorithm>
#include <type_traits>
#include <cstdint>
#include <typeinfo>

using std::endl;
using std::cout;
using std::vector;
using std::cerr;
using std::map;
using std::to_string;

struct Tag {
    int flav;
    int lvl;
    float ratio;
    float dr;
    int idx;
    int fIdx;
};

class Pythia8Jets {
public :
    TTree          *fChain;   //!pointer to the analyzed TTree or TChain
    Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

    // Declaration of leaf types
    Char_t          info;
    Char_t          bnucount;
    Char_t          onucount;
    Char_t          nuob;
    Char_t          nuoc;
    Char_t          nuolept;
    Char_t          nuoother;
    Char_t          nub;
    Char_t          nuc;
    Char_t          nulept;
    Char_t          nuother;
    Float_t         weight;
    Float_t         pthat;
    vector<float>   *isolation;
    vector<unsigned char> *prtcl_jet;
    vector<int>     *prtcl_pdgid;
    vector<float>   *prtcl_pt;
    vector<float>   *prtcl_eta;
    vector<float>   *prtcl_phi;
    vector<float>   *prtcl_e;
    vector<char>    *prtn_jet;
    vector<int>     *prtn_ptn;
    vector<int>     *prtn_own;
    vector<int>     *prtn_pdgid;
    vector<char>    *prtn_tag;
    vector<float>   *prtn_pt;
    vector<float>   *prtn_eta;
    vector<float>   *prtn_phi;
    vector<float>   *prtn_e;
    vector<float>   *prtn_dr;
    vector<float>   *jet_pt;
    vector<float>   *jet_eta;
    vector<float>   *jet_phi;
    vector<float>   *jet_e;
    vector<int>     *jet_ptn;
    vector<int>     *jet_constituents;
    vector<float>   *jet_ptd;
    vector<float>   *jet_sigma2;
    Float_t         met;
    Float_t         metphi;
    TLorentzVector  mMet;
    bool            mDoHw;

    static constexpr float    mUE = 0.02;
    static constexpr float    LeptPt = 30;
    static constexpr float    LeptEta = 2.1;
    static constexpr float    JetPt = 20;
    static constexpr float    MinPt = 15;
    static constexpr float    JetEta = 2.4;
    static constexpr float    MaxEta = 5.0;

    vector<pair<string,float>> mATitles;

    vector<Double_t>          mARange;
    unsigned                  mABins;

    vector<int>              mBParts;
    vector<int>              mWParts;
    map<int,Tag>             mTmpTags;
    vector<int>              mLeaders;
    map<int, vector<int> >   mFollowers;
    map<int,int>             mTagName;
    map<string,double>       mErrorList;
    vector<string>           mInsertions;
    vector<std::pair<TLorentzVector,bool> >   mJets;
    vector<int>              mBJets;
    map<string, TH1D*>       mHistos;
    map<string, TProfile*>   mProfs;
    vector<string>           mAlphaNames;
    vector<double>            mPts;
    vector<double>            mEtas;
    unsigned                  mNPts;
    unsigned                  mNEtas;
    TMatrixD                 *mSquare;
    TMatrixD                 *mColumn;
    TMatrixD                 *mSingle;
    TMatrixD                 *mCount;

    TH2D* grid1;
    TH2D* grid2;
    TH2D* grid3;
    TF1* f1;
    TF1* f2;

    void TagTree(int id, int lvl, int info = -1);
    Pythia8Jets(bool hw = false, TTree *tree=0);
    virtual ~Pythia8Jets();
    virtual Int_t    GetEntry(Long64_t entry);
    virtual Long64_t LoadTree(Long64_t entry);
    virtual void     Init(TTree *tree);
    virtual void     Loop(float lowMass, float dMass);
    template<class T>
    void Drawer(T *h, bool stats = true, bool logY = false);
    void AddMessage(string msg,double wgt);
    bool Test(string msg,bool test);
    bool Problem(string msg,bool test);
    void Filler(float alpha, float pt, float ratio);
    int PhaseBin(double pt, double eta);
};

#endif

#ifdef Pythia8Jets_cxx
int Pythia8Jets::PhaseBin(double pt, double eta) {
    eta = fabs(eta);
    int ebin = -1;
    int ptbin = -1;
    for (unsigned ieta = 0; ieta < mNEtas; ++ieta) {
        if (eta < mEtas[ieta+1]) {
            ebin = ieta;
            break;
        }
    }
    for (unsigned ipt = 0; ipt < mNPts; ++ipt) {
        if (pt < mPts[ipt+1]) {
            ptbin = ipt;
            break;
        }
    }
    if (ebin==-1 or ptbin==-1) return -1;

    return ebin*mNPts+ptbin;
}

Pythia8Jets::Pythia8Jets(bool hw, TTree *tree) : fChain(0)
{
    mDoHw = hw;
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
     string file = mDoHw ? "../../gen_handle/pjets_herwig7_dijet_4M.root" : "../../gen_handle/pjets_pythia8_dijet_4M2.root";
    if (tree == 0) {
        TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(file.c_str());
        if (!f or !f->IsOpen()) f = new TFile(file.c_str());
        if (mDoHw) f->GetObject("HerwigTree",tree);
        else       f->GetObject("Pythia8Jets",tree);
    }
    Init(tree);

    mARange = {-0.05,0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75,0.8,0.85,0.9,0.95};
    for (unsigned i = 0; i < mARange.size(); ++i) mARange[i] += 0.05;
    mABins = mARange.size()-1;
    Double_t* aRange = &mARange[0];

    mInsertions.push_back("All");
    mInsertions.push_back("PtLead");
    mInsertions.push_back("PhiLead");
    mInsertions.push_back("Pt3");
    mInsertions.push_back("Assoc");
    mInsertions.push_back("Ratios 2");
    mInsertions.push_back("Ratios 0");

    mHistos["control1"] = new TH1D("ctrl1","ctrl1",50,0,2);
    mHistos["control2"] = new TH1D("ctrl2","ctrl2",50,0,2);
    mHistos["control3"] = new TH1D("ctrl3","ctrl3",50,0,2);
    mHistos["actrl"] = new TH1D("actrl","actrl",50,0,2);
    mHistos["acount1"] = new TH1D("acount1","acount",50,0,2);
    mHistos["acount2"] = new TH1D("acount2","acount",50,0,2);

    mProfs["alphaAve"] = new TProfile("alpha_pta","alpha ptratio ave",mABins,aRange);
    mProfs["alphaBoth"] = new TProfile("alpha_ptb","alpha ptratio bave",mABins,aRange);
    mProfs["alpha1"] = new TProfile("alpha_pt1","alpha ptratio 1",mABins,aRange);
    mProfs["alpha2"] = new TProfile("alpha_pt2","alpha ptratio 2",mABins,aRange);
    mProfs["beta1"] = new TProfile("beta_pt1","beta ptratio 1",mABins,aRange);

    mProfs["ealphaAve"] = new TProfile("alpha_ea","alpha eratio ave",mABins,aRange);
    mProfs["ealphaBoth"] = new TProfile("alpha_eb","alpha eratio bave",mABins,aRange);
    mProfs["ealpha1"] = new TProfile("alpha_e1","alpha eratio 1",mABins,aRange);
    mProfs["ealpha2"] = new TProfile("alpha_e2","alpha eratio 2",mABins,aRange);
    mProfs["ebeta1"] = new TProfile("beta_e1","beta eratio 1",mABins,aRange);

    mProfs["olphaAve"] = new TProfile("olpha_pta","olpha ptratio ave",mABins,aRange);
    mProfs["olphaBoth"] = new TProfile("olpha_ptb","olpha ptratio bave",mABins,aRange);
    mProfs["olpha1"] = new TProfile("olpha_pt1","olpha ptratio 1",mABins,aRange);
    mProfs["olpha2"] = new TProfile("olpha_pt2","olpha ptratio 2",mABins,aRange);

    mProfs["eolphaAve"] = new TProfile("olpha_ea","olpha eratio ave",mABins,aRange);
    mProfs["eolphaBoth"] = new TProfile("olpha_eb","olpha eratio bave",mABins,aRange);
    mProfs["eolpha1"] = new TProfile("olpha_e1","olpha eratio 1",mABins,aRange);
    mProfs["eolpha2"] = new TProfile("olpha_e2","olpha eratio 2",mABins,aRange);

    mProfs["jvg"] = new TProfile("jvsgamma","jetptpergamma",mABins,aRange);
    mProfs["pvg"] = new TProfile("pvsgamma","MEpartonptpergamma",mABins,aRange);
    mProfs["tpvg"] = new TProfile("tpvsgamma","Fpartonptpergamma",mABins,aRange);
    mProfs["MEpvg"] = new TProfile("ttpvsgamma","TheMEpartonptpergamma",mABins,aRange);

    mAlphaNames = {"","c1","c2","c1c2","sum","sumc1","sumcs","sumc1cs"};
    for (auto &aname : mAlphaNames) {
        mProfs[aname+"alpha30"] = new TProfile(Form("incl_alpha%s_pt30to50",aname.c_str()),"alpha ptratio 30-50",mABins,aRange);
        mProfs[aname+"alpha50"] = new TProfile(Form("incl_alpha%s_pt50to80",aname.c_str()),"alpha ptratio 50-80",mABins,aRange);
        mProfs[aname+"alpha80"] = new TProfile(Form("incl_alpha%s_pt80to120",aname.c_str()),"alpha ptratio 80-120",mABins,aRange);
        mProfs[aname+"alpha120"] = new TProfile(Form("incl_alpha%s_pt120to200",aname.c_str()),"alpha ptratio 120-200",mABins,aRange);
        mProfs[aname+"alpha200"] = new TProfile(Form("incl_alpha%s_pt200to400",aname.c_str()),"alpha ptratio 200-400",mABins,aRange);
        mProfs[aname+"alpha400"] = new TProfile(Form("incl_alpha%s_pt400toInf",aname.c_str()),"alpha ptratio 400-Inf",mABins,aRange);
        mProfs[aname+"alphaone30"] = new TProfile(Form("incl_alphaone%s_pt30to50",aname.c_str()),"alpha ptratio 30-50",mABins,aRange);
        mProfs[aname+"alphaone50"] = new TProfile(Form("incl_alphaone%s_pt50to80",aname.c_str()),"alpha ptratio 50-80",mABins,aRange);
        mProfs[aname+"alphaone80"] = new TProfile(Form("incl_alphaone%s_pt80to120",aname.c_str()),"alpha ptratio 80-120",mABins,aRange);
        mProfs[aname+"alphaone120"] = new TProfile(Form("incl_alphaone%s_pt120to200",aname.c_str()),"alpha ptratio 120-200",mABins,aRange);
        mProfs[aname+"alphaone200"] = new TProfile(Form("incl_alphaone%s_pt200to400",aname.c_str()),"alpha ptratio 200-400",mABins,aRange);
        mProfs[aname+"alphaone400"] = new TProfile(Form("incl_alphaone%s_pt400toInf",aname.c_str()),"alpha ptratio 400-Inf",mABins,aRange);
        mProfs[aname+"aleph30"] = new TProfile(Form("raw_alpha%s_pt30to50",aname.c_str()),"raw alpha ptratio 30-50",mABins,aRange);
        mProfs[aname+"aleph50"] = new TProfile(Form("raw_alpha%s_pt50to80",aname.c_str()),"raw alpha ptratio 50-80",mABins,aRange);
        mProfs[aname+"aleph80"] = new TProfile(Form("raw_alpha%s_pt80to120",aname.c_str()),"raw alpha ptratio 80-120",mABins,aRange);
        mProfs[aname+"aleph120"] = new TProfile(Form("raw_alpha%s_pt120to200",aname.c_str()),"raw alpha ptratio 120-200",mABins,aRange);
        mProfs[aname+"aleph200"] = new TProfile(Form("raw_alpha%s_pt200to400",aname.c_str()),"raw alpha ptratio 200-400",mABins,aRange);
        mProfs[aname+"aleph400"] = new TProfile(Form("raw_alpha%s_pt400toInf",aname.c_str()),"raw alpha ptratio 400-Inf",mABins,aRange);
        mProfs[aname+"alephone30"] = new TProfile(Form("raw_alphaone%s_pt30to50",aname.c_str()),"raw alpha ptratio 30-50",mABins,aRange);
        mProfs[aname+"alephone50"] = new TProfile(Form("raw_alphaone%s_pt50to80",aname.c_str()),"raw alpha ptratio 50-80",mABins,aRange);
        mProfs[aname+"alephone80"] = new TProfile(Form("raw_alphaone%s_pt80to120",aname.c_str()),"raw alpha ptratio 80-120",mABins,aRange);
        mProfs[aname+"alephone120"] = new TProfile(Form("raw_alphaone%s_pt120to200",aname.c_str()),"raw alpha ptratio 120-200",mABins,aRange);
        mProfs[aname+"alephone200"] = new TProfile(Form("raw_alphaone%s_pt200to400",aname.c_str()),"raw alpha ptratio 200-400",mABins,aRange);
        mProfs[aname+"alephone400"] = new TProfile(Form("raw_alphaone%s_pt400toInf",aname.c_str()),"raw alpha ptratio 400-Inf",mABins,aRange);
    }

    Int_t ptBins = 40;
    Double_t ptRange[] = {40,60,80,100,120,140,160,180,200,220,240,260,280,300,320,340,360,380,400,420,440,460,480,500,520,540,560,580,600,620,640,660,680,700,720,740,760,780,800,820,840};
    //mPts = {15, 21, 28, 37, 49, 64, 84, 114, 153, 196, 272, 330, 395, 468, 548, 686, 846, 1032, 1248, 1588, 2500, 100000};
    //mEtas = {0.0,0.5,1.0,1.5,2.0,2.5,3.5,5.0};
    //mPts = {0,50,100,200,20000};
    //mEtas = {0.0,1.3,2.5,5.0};
    mPts = {0,100,200,20000};
    mEtas = {0.0,1.3,5.0};
    mNPts = mPts.size()-1;
    mNEtas = mEtas.size()-1;
    int ntot = mNPts*mNEtas;
    mSquare = new TMatrixD(ntot,ntot);
    mColumn = new TMatrixD(ntot,4);
    mSingle = new TMatrixD(1,4);
    mCount = new TMatrixD(ntot,ntot+1);

    mHistos["a05"]=new TH1D("weights05","",ptBins,ptRange);
    mHistos["a10"]=new TH1D("weights10","",ptBins,ptRange);
    mHistos["a15"]=new TH1D("weights15","",ptBins,ptRange);
    mHistos["a20"]=new TH1D("weights20","",ptBins,ptRange);
    mHistos["a25"]=new TH1D("weights25","",ptBins,ptRange);
    mHistos["a30"]=new TH1D("weights30","",ptBins,ptRange);
    mHistos["a35"]=new TH1D("weights35","",ptBins,ptRange);
    mHistos["a40"]=new TH1D("weights40","",ptBins,ptRange);

    mProfs["a05"]=new TProfile("prof05","",ptBins,ptRange);
    mProfs["a10"]=new TProfile("prof10","",ptBins,ptRange);
    mProfs["a15"]=new TProfile("prof15","",ptBins,ptRange);
    mProfs["a20"]=new TProfile("prof20","",ptBins,ptRange);
    mProfs["a25"]=new TProfile("prof25","",ptBins,ptRange);
    mProfs["a30"]=new TProfile("prof30","",ptBins,ptRange);
    mProfs["a35"]=new TProfile("prof35","",ptBins,ptRange);
    mProfs["a40"]=new TProfile("prof40","",ptBins,ptRange);

    grid1 = new TH2D("g1","g1",mABins,aRange,50,0.0,2.0);
    grid2 = new TH2D("g2","g2",mABins,aRange,50,0.0,2.0);
    grid3 = new TH2D("g3","g3",mABins,aRange,50,0.0,2.0);


    mATitles.push_back(std::make_pair("a40",0.4));
    mATitles.push_back(std::make_pair("a35",0.35));
    mATitles.push_back(std::make_pair("a30",0.3));
    mATitles.push_back(std::make_pair("a25",0.25));
    mATitles.push_back(std::make_pair("a20",0.2));
    mATitles.push_back(std::make_pair("a15",0.15));
    mATitles.push_back(std::make_pair("a10",0.1));   mATitles.push_back(std::make_pair("a05",0.05));
}

Pythia8Jets::~Pythia8Jets()
{
    if (!fChain) return;
    delete fChain->GetCurrentFile();
}

Int_t Pythia8Jets::GetEntry(Long64_t entry)
{
// Read contents of entry.
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}
Long64_t Pythia8Jets::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
        fCurrent = fChain->GetTreeNumber();
    }
    return centry;
}

void Pythia8Jets::Init(TTree *tree)
{
    // The Init() function is called when the selector needs to initialize
    // a new tree or chain. Typically here the branch addresses and branch
    // pointers of the tree will be set.
    // It is normally not necessary to make changes to the generated
    // code, but the routine can be extended by the user if needed.
    // Init() will be called many times when running on PROOF
    // (once per file to be processed).
    isolation = 0;
    prtcl_jet = 0;
    prtcl_pdgid = 0;
    prtcl_pt = 0;
    prtcl_eta = 0;
    prtcl_phi = 0;
    prtcl_e = 0;
    prtn_jet = 0;
    prtn_ptn = 0;
    prtn_own = 0;
    prtn_pdgid = 0;
    prtn_tag = 0;
    prtn_pt = 0;
    prtn_eta = 0;
    prtn_phi = 0;
    prtn_e = 0;
    prtn_dr = 0;
    jet_pt = 0;
    jet_eta = 0;
    jet_phi = 0;
    jet_e = 0;
    jet_ptn = 0;
    jet_constituents = 0;
    jet_ptd = 0;
    jet_sigma2 = 0;

    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);

    fChain->SetBranchAddress("info", &info);
    fChain->SetBranchAddress("bnucount", &bnucount);
    fChain->SetBranchAddress("onucount", &onucount);
    fChain->SetBranchAddress("nuob", &nuob);
    fChain->SetBranchAddress("nuoc", &nuoc);
    fChain->SetBranchAddress("nuolept", &nuolept);
    fChain->SetBranchAddress("nuoother", &nuoother);
    fChain->SetBranchAddress("nub", &nub);
    fChain->SetBranchAddress("nuc", &nuc);
    fChain->SetBranchAddress("nulept", &nulept);
    fChain->SetBranchAddress("nuother", &nuother);
    fChain->SetBranchAddress("weight", &weight);
    fChain->SetBranchAddress("pthat", &pthat);
    fChain->SetBranchAddress("isolation", &isolation);
    fChain->SetBranchAddress("prtcl_jet", &prtcl_jet);
    fChain->SetBranchAddress("prtcl_pdgid", &prtcl_pdgid);
    fChain->SetBranchAddress("prtcl_pt", &prtcl_pt);
    fChain->SetBranchAddress("prtcl_eta", &prtcl_eta);
    fChain->SetBranchAddress("prtcl_phi", &prtcl_phi);
    fChain->SetBranchAddress("prtcl_e", &prtcl_e);
    fChain->SetBranchAddress("prtn_jet", &prtn_jet);
    fChain->SetBranchAddress("prtn_ptn", &prtn_ptn);
    fChain->SetBranchAddress("prtn_own", &prtn_own);
    fChain->SetBranchAddress("prtn_pdgid", &prtn_pdgid);
    fChain->SetBranchAddress("prtn_tag", &prtn_tag);
    fChain->SetBranchAddress("prtn_pt", &prtn_pt);
    fChain->SetBranchAddress("prtn_eta", &prtn_eta);
    fChain->SetBranchAddress("prtn_phi", &prtn_phi);
    fChain->SetBranchAddress("prtn_e", &prtn_e);
    fChain->SetBranchAddress("prtn_dr", &prtn_dr);
    fChain->SetBranchAddress("jet_pt", &jet_pt);
    fChain->SetBranchAddress("jet_eta", &jet_eta);
    fChain->SetBranchAddress("jet_phi", &jet_phi);
    fChain->SetBranchAddress("jet_e", &jet_e);
    fChain->SetBranchAddress("jet_ptn", &jet_ptn);
    fChain->SetBranchAddress("jet_constituents", &jet_constituents);
    fChain->SetBranchAddress("jet_ptd", &jet_ptd);
    fChain->SetBranchAddress("jet_sigma2", &jet_sigma2);
    fChain->SetBranchAddress("met", &met);
}

#endif // #ifdef Pythia8Jets_cxx
