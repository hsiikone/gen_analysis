#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TMath.h>
#include <TGraph.h>
#include <TLine.h>
#include <TText.h>
#include <TLatex.h>

// Header file for the classes stored in the TTree if any.
#include <TH3.h>
#include <TH2.h>
#include <TF1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>

#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <iomanip>

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Math/IFunction.h"

#include "../tdrstyle_mod15.C"

void DoBox(double xlo, double xhi, double yval, double yerr, int col, int style) {
    TBox *box = new TBox(xlo, yval-yerr, xhi, yval+yerr);
    box->SetFillStyle(style);
    box->SetFillColor(col);
    box->Draw("SAME");
}

void ReadFile(string loc, vector<vector<double>> &kjes) {
    ifstream fink(loc);
    if (!fink.is_open()) return false;

    int noRun = 4;
    int noEst = 5;
    double mass;

    string name = "";
    vector<vector<double>> medvect2;
    for (int jw = 0; jw != noEst; ++jw) {
        vector<double> invect;
        for (int kw = 0; kw != noRun; ++kw) {
            fink >> mass;
            invect.push_back(mass);
        } // for kw
        kjes.emplace_back(invect);
    } // for jw
}

void Plotter(string name, vector<vector<double>> &vals, double ymin, double ymax, const vector<string> &d0eras, const vector<string> &meths) {
    vector<string> labels;
    for (const auto &era : d0eras) {
        for (const auto &meth : meths) {
            labels.push_back(Form("%s (%s)", meth.c_str(), era.c_str()));
        }
    }
    const int eras = d0eras.size();
    const int ests = meths.size();
    const int n = eras*ests;
    setTDRStyle();
    TH1D *dummy = new TH1D(Form("d_%s",name.c_str()),"d",n,0,n);
    dummy->GetYaxis()->SetRangeUser(ymin,ymax);
    //dummy->GetYaxis()->SetLabelSize(0.03);
    dummy->GetXaxis()->SetLabelSize(0.03);
    for (int i = 0; i < n; ++i) dummy->GetXaxis()->SetBinLabel(dummy->GetXaxis()->FindBin(i), labels[i].c_str());
    TCanvas *c1 = tdrCanvas(Form("c_%s",name.c_str()),dummy,0,9);

    TGraphErrors* gr = new TGraphErrors(n);
    for (int eidx = 0; eidx < eras; ++eidx) {
        for (int est = 0; est < ests; ++est) {
            int i = ests*eidx+est;
            gr->SetPoint(i, dummy->GetBinCenter(i+1), vals[est][eidx]);
        }
    }
    gStyle->SetOptStat(0);
    gStyle->SetOptTitle(1);
    gStyle->SetOptFit(true);
    tdrDraw(gr,"P",kFullCircle);
    c1->SaveAs(Form("%s.pdf",name.c_str()));
}

void Plotter3(string name, vector<vector<vector<double>>> &vals, vector<vector<vector<double>>> &errs, double ymin, double ymax, const vector<string> &d0eras, const vector<string> &meths, const vector<int> &methis) {
    vector<string> labels;
    for (const auto &era : d0eras) {
        for (const auto &meth : meths) {
            labels.push_back(meth);
        }
    }
    const int eras = d0eras.size();
    const int ests = meths.size();
    const int n = eras*ests;
    TH1D *dummy = new TH1D(Form("d_%s",name.c_str()),";;K_{JES,New}^{Rel}/K_{JES,D#slashO}^{Rel}",n,0,n);
    dummy->GetYaxis()->SetRangeUser(ymin,ymax);
    dummy->GetYaxis()->SetTitleSize(0.047);
    dummy->GetYaxis()->SetLabelSize(0.027);
    dummy->GetYaxis()->SetMaxDigits(4);
    dummy->GetXaxis()->SetLabelSize(0.05);
    dummy->GetYaxis()->SetTitleOffset(3.);
    for (int i = 0; i < n; ++i)
        dummy->GetXaxis()->SetBinLabel(dummy->GetXaxis()->FindBin(i), labels[i].c_str());
    TCanvas *c1 = tdrCanvas(Form("c_%s",name.c_str()),dummy,0,9);
    gStyle->SetOptStat(0);
    gStyle->SetOptTitle(1);
    gStyle->SetOptFit(true);
    gStyle->SetTitleYOffset(1.5);

    // TGraphErrors* gr = new TGraphErrors(dummy);
    TGraphErrors* grel = new TGraphErrors(n);
    TGraphErrors* grmu = new TGraphErrors(n);
    grel->GetYaxis()->SetTitleOffset(2.);
    grel->SetTitle(";;K_{JES,New}^{Rel}/K_{JES,D#slashO}^{Rel}");
    int i = 0;
    for (int eidx = 0; eidx < eras; ++eidx) {
        double sumwEl = 0, sumwMu = 0;
        double sumwPEl = 0, sumwPMu = 0;
        for (auto &est : methis) {
            double xpos = i + 0.5;
            // gr->SetPoint  (i, xpos, vals[0][est][eidx]);
            // gr->SetPointError(i, 0., errs[0][est][eidx]);
            grel->SetPoint(i, xpos+0.05, vals[1][est][eidx]);
            grel->SetPointError(i, 0., errs[1][est][eidx]);
            grmu->SetPoint(i, xpos-0.05, vals[2][est][eidx]);
            grmu->SetPointError(i, 0., errs[2][est][eidx]);
            const double wEl = pow(errs[1][est][eidx],-2);
            const double wMu = pow(errs[2][est][eidx],-2);
            sumwEl += wEl;
            sumwMu += wMu;
            sumwPEl += wEl * vals[1][est][eidx];
            sumwPMu += wMu * vals[2][est][eidx];
            ++i;
        }
        const double xlo = ests * eidx;
        const double xhi = ests * (eidx + 1);
        DoBox(xlo,xhi,sumwPEl/sumwEl,1/sqrt(sumwEl),kBlue,3004);
        DoBox(xlo,xhi,sumwPMu/sumwMu,1/sqrt(sumwMu),kRed,3005);
    }
    // gPad->Update();
    tdrDraw(grel,"P",kFullDotSmall, kBlue);
    tdrDraw(grmu,"P",kFullDotSmall, kRed);
    // tdrDraw(gr,"P",kFullDotSmall, kMagenta);
    TLine *l = new TLine();
    for (int i = 1; i < eras; ++i)
        l->DrawLine(i * ests, ymin, i * ests, ymax);

    TLegend* leg0 = new TLegend(0.78, 0.60, 1.0, 0.78);
    leg0->SetTextSize(0.036);
    leg0->SetFillStyle(0);
    leg0->SetBorderSize(0);
    leg0->AddEntry(grel, "e+jets", "LP");
    leg0->AddEntry(grmu, "#mu+jets", "LP");
    // leg0->Add(gr, "l+jets", "LP");
    leg0->Draw("SAME");
    gPad->Update();
  
    TLatex latex;
    latex.SetTextSize(0.05);
    latex.SetTextAlign(13);  //align at top
    latex.DrawLatex(0.4*ests, ymin + 0.95*(ymax-ymin), "IIa");
    latex.DrawLatex(1.35*ests, ymin + 0.95*(ymax-ymin), "IIb1");
    latex.DrawLatex(2.35*ests, ymin + 0.95*(ymax-ymin), "IIb2");
    latex.DrawLatex(3.3*ests, ymin + 0.95*(ymax-ymin), "IIb34");

    c1->SaveAs(Form("%s.pdf",name.c_str()));
}

void simpleplots() {
    vector<double> yups = {1.005, 1.0045, 1.006, 1.006};
    vector<double> ylos = {0.999, 0.9995, 1.000, 1.000};
    vector<string> versions = {"1","2","3","4"};
    vector<string> subdirs  = {"","El","Mu"};
    vector<string> eras = {"a", "b1", "b2", "b34"};
    // vector<string> methods = {"Fit", "Ave", "Itg", "Max", "Med"};
    // vector<int> methodidx = {0,1,2,3,4};
    vector<string> methods = {"Ave", "Itg", "Med"};
    vector<int> methodidx = {1,2,4};
    for (int iv = 0; iv < 4; ++iv) {
        auto &version = versions[iv];
        vector<vector<vector<double>>> rels;
        vector<vector<vector<double>>> errs;
        for (auto &subdir : subdirs) {
            string dir = subdir=="" ? "" : "/"+subdir;
            vector<vector<double>> kjes, kjeserr;
            ReadFile("../non2v" + version + dir + "/doublekjes.txt", kjes);
            ReadFile("../non2v" + version + dir + "/doublekjeserr.txt", kjeserr);
            //Plotter("kjes_non" +version+subdir, kjes_non, 1.01, 1.028);
            //Plotter("kjes_toni"+version+subdir, kjes_toni, 1.01, 1.028);
            //Plotter("kjes_rel" +version+subdir, kjes_rel, ylos[iv], yups[iv]);
            rels.emplace_back(kjes);
            errs.emplace_back(kjeserr);
        }
        Plotter3("kjes_rel_all" +version, rels, errs, ylos[iv], yups[iv], eras, methods, methodidx);
    }
}
