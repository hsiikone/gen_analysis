//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Sep  1 11:06:22 2017 by ROOT version 6.08/06
// from TTree TopJets/Pythia8 particle data.
// found on file: pjets_pythia8_powheg_ttbar.root
//////////////////////////////////////////////////////////

#ifdef DEBUG
#undef DEBUG
#endif
// #define DEBUG

#ifndef TopJets_h
#define TopJets_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TMath.h>
#include <TGraph.h>
#include <TLine.h>
#include <TText.h>
#include <TRandom1.h>

// Header file for the classes stored in the TTree if any.
#include <TH2.h>
#include <TF1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>

#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <iomanip>

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Math/IFunction.h"

#include "../tdrstyle_mod15.C"

using std::pair;
using std::endl;
using std::cout;
using std::vector;
using std::cerr;
using std::map;
using std::to_string;

template<typename T, typename V>
inline bool includes(vector<T> &vect, V val) {
    return std::find(vect.begin(),vect.end(),val) != vect.end();
}

/* We look how P must be scaled. This is conveyed to Pt. */
void ScaleMomentum(TLorentzVector &v, double mass) {
    double ptNew = v.Pt()*TMath::Sqrt(pow(v.E(),2)-pow(mass,2))/v.P();
    v.SetPtEtaPhiE(ptNew,v.Eta(),v.Phi(),v.E());
}

void ScaleEnergy(TLorentzVector &v, double mass) {
    double eNew = TMath::Sqrt(pow(v.P(),2)+pow(mass,2));
    v.SetPtEtaPhiE(v.Pt(),v.Eta(),v.Phi(),eNew);
}

template<typename T>
int EtaIndex(vector<T> &etas, double eta) {
    eta = fabs(eta);

    unsigned eidx = 1;
    while (eidx<etas.size() and eta > etas[eidx]) ++eidx;
    if (eidx==etas.size()) {
        if (eta-0.1<etas.back()) {
            eidx = etas.size()-1;
        } else {
            cerr << "Eta value out of range: " << eta << " vs. " << etas.back() << endl;
            return -4;
        }
    }
    return eidx-1;
}

class CKJES {
public:
    CKJES(double s = 1.0, double o = 0.0) {
        slope  = s;
        offset = o;
    }
    double Eval(double KJES) {
        return 1+slope*(KJES-1)+offset;
    }

    double InvEval(double KJES) {
        return 1+(KJES-1-offset)/slope;
    }
private:
    double slope;
    double offset;
};

class TransferFunction {
public:
    TransferFunction() : mInit(false) {}
    TransferFunction(vector<float>::iterator abeg, vector<float>::iterator bbeg, double Ey) {
        a1 = abeg;
        b1 = bbeg;

        SetEy(Ey);

        mInit = true;
        mCount = 0;
        mSum = 0;
    }

    void SetEy(double Ey) {
        mEy = Ey;
        p1 = LF(1);
        p2 = LF(2);
        p3 = LF(3);
        p4 = LF(4);
        p5 = LF(5);
    }

    double Eval(double Ex) {
        return mInit ? (1/(TMath::Sqrt(2*TMath::Pi())*(p2 + p3*p5)))*
               (TMath::Exp(-(1/2)*pow((Ex - mEy - p1)/p2,2)) + p3*
                TMath::Exp(-(1/2)*pow((Ex - mEy - p4)/p5,2))) : 0.0;
    }

    double Cumul(double Ex) {
        return mInit ? (p2*   TMath::Erfc((mEy + p1 - Ex)/(TMath::Sqrt(2.0)*p2)) +
                        p3*p5*TMath::Erfc((mEy + p4 - Ex)/(TMath::Sqrt(2.0)*p5)))/(2*(p2 + p3*p5)) : 0.0;
    }

    // Function to find the root
    double NewtonRaphson(double Ex, double prob) {
        double eps = 0.00001;

        double div = Eval(Ex);
        if (div == 0) {
            Ex += eps;
            div = Eval(Ex);
        }
        double diff = Cumul(Ex) - prob;
        int iters = 0;
        while (fabs(diff) >= eps and iters<1000) {
            ++iters;
            // x(i+1) = x(i) - f(x) / f'(x)
            Ex -= diff/div;

            div = Eval(Ex);
            if (div == 0) {
                Ex += eps;
                div = Eval(Ex);
            }
            diff = Cumul(Ex) - prob;
        }

        return Ex;
    }

    double Bisection(double a, double b, double prob) {
        double eps = 0.00001;
        if ((Cumul(a)-prob) * (Cumul(b)-prob) >= 0) {
            cout << "You have not assumed right a and b\n";
            return -4;
        }

        double Ex = (a+b)/2.0;
        double diff = Cumul(Ex)-prob;
        int iters = 0;
        while (fabs(diff) >= eps and iters<1000) {
            ++iters;
            // Decide the side to repeat the steps
            if (diff*(Cumul(a)-prob) < 0) b = Ex;
            else                          a = Ex;
            // Find middle point
            Ex = (a+b)/2.0;
            diff = Cumul(Ex)-prob;
        }

        return Ex;
    }

    double Combo(double a, double b, double prob) {
        double eps = 0.00001;
        if ((Cumul(a)-prob) * (Cumul(b)-prob) >= 0) { return -4; }

        double Ex = (a+b)/2.0;
        double diff = Cumul(Ex) - prob;
        int iters = 0;
        while (fabs(diff)>=eps and iters<1000) {
            // Decide the side to repeat the steps
            if (diff*(Cumul(a)-prob) < 0) b = Ex;
            else                          a = Ex;
            if (++iters<10 or iters>25) {
                // Find middle point
                Ex = (a+b)/2.0;
            } else {
                double div = Eval(Ex);
                if (div == 0) {
                    Ex += eps;
                    div = Eval(Ex);
                }
                Ex -= diff/div;
            }
            diff = Cumul(Ex) - prob;
        }

        return Ex;
    }

    double InvCumul(double prob) {
        if (!mInit) { cerr << "Not initialized!" << endl; return -4; }
        return Combo(0,1960,prob);
    }

    double DrawEx() {
        double rnd = 0, Ex = -4;
        while (Ex==-4) {
            rnd = mR1.Rndm();
            Ex = InvCumul(rnd);
        }
        if (Ex>20) {
            mSum += Ex;
            ++mCount;
        }
        return Ex;
    }

    double AveEx() {
        double spi = TMath::Sqrt(TMath::Pi());
        double s2  = TMath::Sqrt(2);
        return (
            p2*(  p2*s2*( TMath::Exp(-pow((-20 + mEy + p1)/p2,2)/2) - TMath::Exp(-pow((mEy + p1)/p2,2)/2))
            + ((pow(mEy,2) + 2*mEy*(-10 + p1) + (-20 + p1)*p1)*spi*TMath::Erf(fabs(-20 + mEy + p1)/(s2*p2)))/fabs(-20 + mEy + p1)
            + (mEy + p1)*spi*(1 - TMath::Erf((mEy + p1)/(s2*p2)) + TMath::Gamma(-0.5, pow((mEy + p1)/p2,2)/2)) )
       + p3*p5*(  p5*s2*( TMath::Exp(-pow((-20 + mEy + p4)/p5,2)/2) - TMath::Exp(-pow((mEy + p4)/p5,2)/2))
            + ((pow(mEy,2) + 2*mEy*(-10 + p4) + (-20 + p4)*p4)*spi*TMath::Erf(fabs(-20 + mEy + p4)/(s2*p5)))/fabs(-20 + mEy + p4)
            + (mEy + p4)*spi*(1 - TMath::Erf((mEy + p4)/(s2*p5)) + TMath::Gamma(-0.5, pow((mEy + p4)/p5,2)/2)) )
        )/(2*(p2 + p3*p5)*spi);
    }

    double AveEx2() {
        double spi = TMath::Sqrt(TMath::Pi());
        double s2  = TMath::Sqrt(2);
        double s2pi = spi*s2;
        return (
            p2*(
                4*(-1 + TMath::Exp(-pow((-20 + mEy + p1)/p2,2)/2))*(mEy+p1)*p2
              - 4*(-1 + TMath::Exp(-pow((      mEy + p1)/p2,2)/2))*(mEy+p1)*p2
              + 4*TMath::Exp(-pow((mEy + p1)/p2,2)/2)*(mEy + p1)*p2
              + (pow(mEy + p1,2) + pow(p2,2))*s2pi*(1+TMath::Gamma(0.5, pow((mEy + p1)/p2,2)/2))
              - (pow(mEy,2) + 2*mEy*p1 + pow(p1,2) + pow(p2,2))*s2pi*TMath::Erf(          (mEy + p1)/(s2*p2))
              + (pow(mEy,2) + 2*mEy*p1 + pow(p1,2))            *s2pi*TMath::Erf(fabs(-20 + mEy + p1)/(s2*p2))*TMath::Sign(1,-20 + mEy + p1)
              - s2*TMath::Exp(-(pow((-20 + mEy + p1)/p2,2)/2))*p2*TMath::Sign(1,-20 + mEy + p1)*
                 (s2*fabs(-20 + mEy + p1) - TMath::Exp(pow((-20 + mEy + p1)/p2,2)/2)*p2*spi*TMath::Erf(fabs(-20 + mEy + p1)/(s2*p2)))
              )
        + p3*p5*(4*(-1 + TMath::Exp(-pow((-20 + mEy + p4)/p5,2)/2))*(mEy+p4)*p5
               - 4*(-1 + TMath::Exp(-pow((      mEy + p4)/p5,2)/2))*(mEy+p4)*p5
               + 4*TMath::Exp(-pow((mEy + p4)/p5,2)/2)*(mEy + p4)*p5
               + (pow(mEy + p4,2) + pow(p5,2))*s2pi*(1 + TMath::Gamma(0.5, pow((mEy + p4)/p5,2)/2))
               - (pow(mEy,2) + 2*mEy*p4 + pow(p4,2) + pow(p5,2))*s2pi*TMath::Erf(          (mEy + p4)/(s2*p5))
               + (pow(mEy,2) + 2*mEy*p4 + pow(p4,2))            *s2pi*TMath::Erf(fabs(-20 + mEy + p4)/(s2*p5))*TMath::Sign(1,-20 + mEy + p4)
               - (s2*TMath::Exp(-pow((-20 + mEy + p4)/p5,2)/2))*p5*TMath::Sign(1,-20 + mEy + p4)*
                  (s2*fabs(-20 + mEy + p4) - TMath::Exp(pow((-20 + mEy + p4)/p5,2)/2)*p5*spi*TMath::Erf(fabs(-20 + mEy + p4)/(s2*p5)))
               )
        )/(2*(p2 + p3*p5)*s2pi);
    }

    double AssistedShiftCorr(double ave1perEx) {
        return 1 + (mEy - AveEx())*ave1perEx;
    }

    double ShiftCorr() {
        double ae  = AveEx();
        double ssc = mEy/ae;
        return ssc + (ssc-1)*(AveEx2()/pow(ae,2)-1);
    }

    double SimpleShiftCorr() {
        return mEy/AveEx();
    }

    double MCShiftCorr() {
        return mEy*mCount/mSum;
    }
private:
    double LF(unsigned idx) {
        if (idx==0 or idx>5) { cout << "Wrong index!" << endl; return 0; }
        idx -= 1;
        return *(a1+idx)+(*(b1+idx))*mEy;
    }

    bool mInit;
    double mEy;
    TRandom1 mR1;

    vector<float>::iterator a1;
    vector<float>::iterator b1;
    double p1;
    double p2;
    double p3;
    double p4;
    double p5;
    int mCount;
    double mSum;
};


class Neutrino {
public:
    Neutrino() : mInit(false), mMW(80.4) {}
    Neutrino(const TLorentzVector &l, const TLorentzVector &m, const TLorentzVector &n);

    int Quality1() const { return mInit ? mQual1 : -2; }
    int Quality2() const { return mInit ? mQual2 : -2; }
    const TLorentzVector &Nu1() const { return mNu1; }
    const TLorentzVector &Nu2() const { return mNu2; }
    double PtR1() const { return mNu1.Pt()/mL.Pt(); }
    double PtR2() const { return mNu2.Pt()/mL.Pt(); }
    double PR1() const { return mNu1.P()/mL.P(); }
    double PR2() const { return mNu2.P()/mL.P(); }
    double Base() const { return mBase; }
    double Det() const { return mPosDet ? mDet : -mDet; }
private:
    TLorentzVector mL;
    TLorentzVector mM;
    TLorentzVector mNu;
    TLorentzVector mNu1;
    TLorentzVector mNu2;

    int mQual1;
    int mQual2;

    bool mInit;
    bool mPosDet;

    const double mMW;
    double mBase;
    double mDet;
};

struct Tag {
    int   flav;
    int   lvl;
    float ratio;
    float dr;
    int   idx;
    int   fIdx;
};

struct Dtr {
    int jetI;
    int id;
    Tag tag;
};

struct FCorr {
    TF1 fun;
    double maxpt;
    double valmax;
};

struct PtCorr {
    TF1 fun;
    double offset;
};

class TopJets {
public :
    TopJets(string fname="pjets_pythia8_amcnlo.root", string tname="TopJets", string masstag="_1725", bool toni = false, int toniv = 0, string loc = ".", TTree *tree=0);
    virtual         ~TopJets();
    virtual Int_t    GetEntry(Long64_t entry);
    virtual Long64_t LoadTree(Long64_t entry);
    virtual void     Init(TTree *tree);
    virtual void     Loop(Long64_t = -1, bool drawMode=true);
    void             MassFill(const TLorentzVector &lepton, const TLorentzVector &truMet, const vector<int> &wParts, const vector<int> &wList, int bhPart, int blPart);
    void             SingleFill(const char *ftag, bool nucase,TLorentzVector &hW1, TLorentzVector &hW2, TLorentzVector &hB, TLorentzVector &lB, const TLorentzVector &lW, const TLorentzVector &l,const TLorentzVector &TM);
    void             EraKJESFill(const char* ft, const char* et,double kj, bool nu, TLorentzVector hW1C,TLorentzVector hW2C, TLorentzVector hBC,TLorentzVector lBC, TLorentzVector hW1, TLorentzVector hW2, TLorentzVector h,const TLorentzVector &lW, const TLorentzVector &l,const TLorentzVector &TM);

    void             InitFCorr(int bins, bool toni);
    void             InitHistos(string tag);

    void             AddMessage(string msg,double wgt);
    bool             Test(string msg,bool test);
    bool             Problem(string msg,bool test);

    bool             SubText(string s1, string s2);
    template<class T>
    void             Saver(T *h);

    void             TagTree(int id, int lvl, int fIdx, int info = 0);
    void             Descend(int id,  int lvl,  int fIdx,  int info = 0);
    void             Eliminate();
    void             DElim(int id);
    void             MElim(int id);
    void             LeftoverTree(int id, int lvl, int fIdx, int info = 0);

    void             ScaleEnergy(TLorentzVector &v, double scale);
    void             SetPartonEnergy(TLorentzVector &v, string era, bool b = false);
    float            Interpolator(vector<float> &vec, float val);
    double           GetFCorr(double pt, double eta, string era, int type=0);
    double           GetPtCorr(double pt, double eta, string eratype);

    void ConvoluteGauss(string name, double sigma, double mass) {
        unsigned GridN = 1000;
        for (unsigned i = 0; i<GridN; ++i)
            mHistos[name]->Fill(gRandom->Gaus(mass,sigma));
    }

    void ConvolutePartonMass(string name1, string name2, const TLorentzVector &v1, const TLorentzVector &v2, const TLorentzVector &bh, string era, bool bnu = false) {
        auto &va = mParAMaps[era];
        auto &vb = mParBMaps[era];
        int eta1 = EtaIndex(mD0Etas,v1.Eta());
        int eta2 = EtaIndex(mD0Etas,v2.Eta());
        int etab = EtaIndex(mD0Etas,bh.Eta());
        TLorentzVector w1 = v1;
        TLorentzVector w2 = v2;
        TLorentzVector b = bh;
        if (eta1!=-4 and eta2!=-4 and etab!=-4) {
            unsigned GridN = 10;
            int boff = bnu ? 10 : 5;
            TransferFunction tf1(va[eta1].begin(),vb[eta1].begin(),v1.E());
            TransferFunction tf2(va[eta2].begin(),vb[eta2].begin(),v2.E());
            TransferFunction tfb(va[etab].begin()+boff,vb[etab].begin()+boff,b.E());

            const char *ftag = mFTag.c_str();
            const char *etag = era.c_str();

            vector<double> E1G, E2G, EBG, E1F, E2F, EBF;
            for (unsigned i = 0; i < GridN; ++i) {
                double kjes = 1;//mKJESCorr[mFTag][era].InvEval(mKJES[mFTag][era]); /* TODO */
                double Ex1 = -4, Ex2 = -4, Exb = -4;
                unsigned repeat = 0;
                while (Ex1<0 and ++repeat<=GridN) {
                    Ex1 = tf1.DrawEx();
                    w1.SetE(Ex1);
                    if (w1.Pt()<mJetPt) Ex1 = -4;
                }
                if (repeat>GridN) return;
                E1G.push_back(Ex1*kjes);
                E1F.push_back(0);//pow(w1.M()/(Ex1*kjes),2)/2.0);
                repeat = 0;
                while (Ex2<0 and ++repeat<=GridN) {
                    Ex2 = tf2.DrawEx();
                    w2.SetE(Ex2);
                    if (w2.Pt()<mJetPt) Ex2 = -4;
                }
                if (repeat>GridN) return;
                E2G.push_back(Ex2*kjes);
                E2F.push_back(0);//pow(w2.M()/(Ex2*kjes),2)/2.0);
                repeat = 0;
                while (Exb<0 and ++repeat<=GridN) {
                    Exb = tfb.DrawEx();
                    b.SetE(Exb);
                    if (b.Pt()<mJetPt) Exb = -4;
                }
                if (repeat>GridN) return;
                EBG.push_back(Exb*kjes);
                EBF.push_back(pow(b.M()/(Exb*kjes),2)/2.0);

                if (Ex1>20) mHistos[Form("tfCorr_%s_%s",etag,ftag)]->Fill(Ex1/v1.E(),mWeight);
                if (Ex2>20) mHistos[Form("tfCorr_%s_%s",etag,ftag)]->Fill(Ex2/v2.E(),mWeight);
                if (Exb>20) {
                    if (bnu) mHistos[Form("tfbnuCorr_%s_%s",etag,ftag)]->Fill(Exb/bh.E(),mWeight);
                    else     mHistos[Form("tfbCorr_%s_%s",  etag,ftag)]->Fill(Exb/bh.E(),mWeight);
                }
            }

//             mHistos[Form("tfCorr_%s_%s",etag,ftag)]->Fill(tf1.MCShiftCorr(),mWeight);
//             mHistos[Form("tfCorr_%s_%s",etag,ftag)]->Fill(tf2.MCShiftCorr(),mWeight);
//             mHistos[Form("tfbCorr_%s_%s",etag,ftag)]->Fill(tfb.MCShiftCorr(),mWeight);

            double c12  = TMath::Cos(v1.Angle(v2.Vect()));
            double c1b  = TMath::Cos(v1.Angle(bh.Vect()));
            double c2b  = TMath::Cos(v2.Angle(bh.Vect()));
            double massesw = 0;//pow(w1.M(),2)+pow(w2.M(),2);
            double massest = pow(bh.M(),2); // massesw +
            for (unsigned i1 = 0; i1 < E1G.size(); ++i1) {
                auto &Ex1 = E1G[i1];
                auto &C1  = E1F[i1];
                for (unsigned i2 = 0; i2 < E2G.size(); ++i2) {
                    auto &Ex2 = E2G[i2];
                    auto &C2  = E2F[i2];
                    double wterms = 2*Ex1*Ex2*(1-c12*(1-C1-C2));
                    double mw = TMath::Sqrt(massesw + wterms);
                    mHistos[Form("tfKJES_%s_%s",etag,ftag)]->Fill(mw/80.4);
                    mHistos[name1]->Fill(mw);
                    for (unsigned ib = 0; ib < EBG.size(); ++ib) {
                        auto &Exb = EBG[ib];
                        auto &CB  = EBF[ib];
                        mHistos[name2]->Fill(TMath::Sqrt(massest + wterms + 2*Ex1*Exb*(1-c1b*(1-C1-CB)) + 2*Ex2*Exb*(1-c2b*(1-C2-CB))));
                    }
                }
            }
        }
        //     for (unsigned bin = 1; bin <= th.GetNbinsX(); ++bin) {
        //         TLorentzVector w1 = v1;
        //         TLorentzVector w2 = v2;
        //         mEy = v1.E();
        //
        //         double ctr = th1.GetBinCenter(bin);
        //     }
    }

private:
    static constexpr float                  mNuisanceLeptPt = 10;
    static constexpr float                  mNuisanceElEta  = 2.5;
    static constexpr float                  mNuisanceMuEta  = 2.0;
    static constexpr float                  mLeptPt         = 20;
    static constexpr float                  mElEta          = 1.1;
    static constexpr float                  mMuEta          = 2.0;
    static constexpr float                  mFirstJetPt     = 40;
    static constexpr float                  mJetPt          = 20;
    static constexpr float                  mJetEta         = 2.5;
    static constexpr float                  mMinPt          = 15;
    static constexpr float                  mMaxEta         = 5.0;
    static constexpr float                  mMinMET         = 20;
    static constexpr float                  mMaxMET         = 250;
    static constexpr float                  mBQMass         = 4.18;
    static constexpr float                  mWQMass         = 0;

    static constexpr bool                   mGuardCorr      = false;
    static constexpr bool                   mScale          = true;
    static constexpr bool                   mOffset         = false;
    static constexpr bool                   mDoSemiCorr     = false;
    static constexpr bool                   mDoTFShift      = false;
    static constexpr bool                   mDoTF           = false;
    static constexpr bool                   mDoPartonLvl    = true;
    static constexpr bool                   mDoPseudoEta    = true;
    static constexpr bool                   mDoGammaJets    = true;
    static constexpr bool                   mDoNuJets       = true;
    static constexpr bool                   mDoNuSim        = true;

    TTree                                  *mChain;
    Int_t                                   mCurrent;
    TFile                                  *mElFile;
    TFile                                  *mMuFile;

    /* Declaration of leaf types */
    Char_t                                  mInfo;
    Char_t                                  mBNuCount;
    Char_t                                  mNuB;
    Char_t                                  mNuC;
    Char_t                                  mNuLept;
    Char_t                                  mNuOther;
    Float_t                                 mWeight;
    Float_t                                 mPtHat;
    vector<bool>                           *mIsolation;
    vector<char>                           *mPJet;
    vector<int>                            *mPPtn;
    vector<int>                            *mPOwn;
    vector<int>                            *mPPDG;
    vector<char>                           *mPTag;
    vector<float>                          *mPPt;
    vector<float>                          *mPEta;
    vector<float>                          *mPPhi;
    vector<float>                          *mPE;
    vector<float>                          *mPDR;
    vector<float>                          *mJPt;
    vector<float>                          *mJEta;
    vector<float>                          *mJPhi;
    vector<float>                          *mJE;
    vector<int>                            *mJCst;
    vector<float>                          *mJPTD;
    vector<float>                          *mJSm2;
    Float_t                                 mMet;

    string                                  mMTag;
    string                                  mFTag;

    Long64_t                                mJentry;
    int                                     mHadrWSign;
    int                                     mRealLept;

    vector<Float_t>                         mARange;
    vector<Double_t>                        mPtRange;
    unsigned                                mABins;
    unsigned                                mPtBins;

    bool                                    mInit;
    bool                                    mToni;
    int                                     mToniV;

    vector<std::pair<TLorentzVector,bool> > mJets;
    vector<TLorentzVector>                  mRefJets;
    vector<TLorentzVector>                  mNuSum;
    vector<TLorentzVector>                  mMuSum;

    vector <int>                            mPartonCombo;
    vector<Dtr>                             mCandidates;
    vector<int>                             mGammas;
    map<int,Tag>                            mTmpTags;
    map<int,Tag>                            mTags;
    vector<int>                             mLeaders;
    map<int, vector<int> >                  mFollowers;
    vector<int>                             mWFollowers;
    map<int,int>                            mTagName;
    map<string,double>                      mErrorListEl;
    map<string,double>                      mErrorListMu;
    vector<string>                          mInsertions;

    map<string, TH1D*>                      mHistos;
    map<string, TH2D*>                      m2DHistos;
    map<string, TProfile*>                  mProfs;
    map<string, double>                     mFitLim;

    map<string, float>                      mKJESVals;
    map<string, map<string,float>>          mKJES;
    map<string, map<string,float>>          mKJESSlope;
    map<string, map<string,float>>          mKJESOffset;
    map<string, map<string,CKJES>>          mKJESCorr;
    /* FCorr settings */
    map<string, vector<float>>              mBParamsT;
    map<string, vector<float>>              mGParamsT;
    map<string, vector<float>>              mLParamsT;
    map<string, vector<float>>              mBParams;
    map<string, vector<float>>              mGParams;
    map<string, vector<float>>              mLParams;
    map<string, vector<FCorr>>              mFBCorrT;
    map<string, vector<FCorr>>              mFGCorrT;
    map<string, vector<FCorr>>              mFLCorrT;
    map<string, vector<FCorr>>              mFBCorr;
    map<string, vector<FCorr>>              mFGCorr;
    map<string, vector<FCorr>>              mFLCorr;
    /* TF average shifts */
    vector<float>                           mExVals;
    map<string, vector<vector<float>>>      mBMaps;
    map<string, vector<vector<float>>>      mLMaps;
    map<string, vector<vector<float>>>      mParAMaps;
    map<string, vector<vector<float>>>      mParBMaps;
    map<string, vector<TF1>>                mLTFs;
    vector<double>                          mD0Etas;
    vector<double>                          mEtaLims;
    vector<string>                          mEras;

    map<string, string>                     mBMap;
    map<string, string>                     mGMap;
    map<string, string>                     mLMap;
    map<string, vector<float>>              mPtParams;
    map<string, vector<float>>              mPtEtas;
    map<string, vector<PtCorr>>             mPtCorr;

    vector<double>                          mKJESRange;
};
#endif

#ifdef TopJets_cxx

TopJets::TopJets(string fname, string tname, string masstag, bool toni, int toniv, string loc, TTree *tree) : mChain(0), mInit(false) {
    TH1::SetDefaultSumw2(kTRUE);
    mMTag  = masstag;
    mToni  = toni;
    mToniV = toniv;
    if (tree == 0) {
        TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(fname.c_str());
        if (!f or !f->IsOpen()) f = new TFile(fname.c_str());
        f->GetObject(tname.c_str(),tree);
    }
    Init(tree);
    mKJESRange = {1.000,1.005,1.010,1.015,1.020};
    mARange    = {0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4};
    mABins = mARange.size()-1;
    Float_t* aRange = &mARange[0];
    mPtRange =
    {15, 18, 21, 24, 28, 32, 37, 43, 49, 56, 64, 74, 84, 97, 114, 133, 153, 174, 196, 220, 245, 272, 300, 330, 362, 395, 430, 468,
     507, 548, 592, 638, 686, 737, 790, 846, 905, 967, 1032, 1101, 1172, 1248, 1327, 1410, 1497, 1588, 1684, 1784, 1890, 2000,
     2116, 2238, 2366, 2500, 2640, 2787, 2941, 3103, 3273, 3450, 3637, 3832, 4037};
    mPtBins = mPtRange.size()-1;

//     mD0Etas = {2.5};
    mD0Etas = {0.0,0.4,0.8,1.6,2.5};
    mEras = {"IIa","IIb1","IIb2","IIb34"};
    if (mToni) {
        cout << "Using toni's parameters!" << endl;
        mEtaLims = {0.0,2.5};
        if (mToniV==0) {
            cout << "Using the original toni's p6 parameters!" << endl;
            // Eta ranges:       0.0-2.6
            mBParamsT["IIa"]   = {1.02962,0.160669,0.660677};
            mGParamsT["IIa"]   = {1.25053,0.242020,0.966231};
            mLParamsT["IIa"]   = {1.12523,0.123869,0.962592};

            mBParamsT["IIb1"]  = {1.02149,0.169581 ,0.568026};
            mGParamsT["IIb1"]  = {1.05221,0.115763 ,0.844849};
            mLParamsT["IIb1"]  = {1.01018,0.0263858,0.644289};

            mBParamsT["IIb2"]  = {1.02212,0.305445 ,0.442063};
            mGParamsT["IIb2"]  = {1.03997,0.156756 ,0.740699};
            mLParamsT["IIb2"]  = {1.00998,0.0394526,0.480637};

            mBParamsT["IIb34"] = {1.02171,0.275125 ,0.460443};
            mGParamsT["IIb34"] = {1.04136,0.146141 ,0.759925};
            mLParamsT["IIb34"] = {1.00998,0.0358863,0.516991};
        } else if (mToniV==1) {
            cout << "Using the enhanced toni's p6 parameters!" << endl;
            // Eta ranges:       0.0-2.6
            mBParamsT["IIa"]   = {1.16575,0.211365,0.923299};
            mGParamsT["IIa"]   = {1.16213,0.185577,0.951696};
            mLParamsT["IIa"]   = {1.10171,0.104425,0.960113};

            mBParamsT["IIb1"]  = {1.02597,0.158204 ,0.623519};
            mGParamsT["IIb1"]  = {1.11835,0.160246 ,0.923861};
            mLParamsT["IIb1"]  = {1.03400,0.0381224,0.937101};

            mBParamsT["IIb2"]  = {1.02800,0.289652 ,0.503932};
            mGParamsT["IIb2"]  = {1.03252,0.175944 ,0.686407};
            mLParamsT["IIb2"]  = {1.01605,0.0242206,0.806435};

            mBParamsT["IIb34"] = {1.02658,0.257210 ,0.519501};
            mGParamsT["IIb34"] = {1.03232,0.158038 ,0.705536};
            mLParamsT["IIb34"] = {1.01613,0.0240468,0.817459};
        } else if (mToniV==2) {
            cout << "Using the D0-sigma'd toni's p6 parameters!" << endl;
            // Eta ranges:       0.0-2.6
            mBParamsT["IIa"]   = {1.19343,0.228825,0.933729};
            mGParamsT["IIa"]   = {1.17736,0.194933,0.956562};
            mLParamsT["IIa"]   = {1.10260,0.105366,0.959913};

            mBParamsT["IIb1"]  = {1.03398,0.141304 ,0.706627};
            mGParamsT["IIb1"]  = {1.12033,0.159182 ,0.928627};
            mLParamsT["IIb1"]  = {1.08632,0.0846364,0.974260};

            mBParamsT["IIb2"]  = {1.03292,0.244353 ,0.581453};
            mGParamsT["IIb2"]  = {1.04201,0.158996 ,0.751165};
            mLParamsT["IIb2"]  = {1.03410,0.0398293,0.915612};

            mBParamsT["IIb34"] = {1.03072,0.222657 ,0.586483};
            mGParamsT["IIb34"] = {1.04022,0.146497 ,0.758737};
            mLParamsT["IIb34"] = {1.03562,0.0407912,0.924718};
        } else if (mToniV==3) {
            cout << "Using the enhanced toni's h7 parameters!" << endl;
            // Eta ranges:       0.0-2.6
            mBParamsT["IIa"]   = {1.37129,0.290693,0.986533};
            mGParamsT["IIa"]   = {1.10670,0.115012,0.971464};
            mLParamsT["IIa"]   = {1.17859,0.156934,0.988099};

            mBParamsT["IIb1"]  = {1.00274,0.0404017,0.389599};
            mGParamsT["IIb1"]  = {1.00566,0.0318759,0.680361};
            mLParamsT["IIb1"]  = {1.00142,0.0115077,0.150975};

            mBParamsT["IIb2"]  = {1.00541,0.210330 , 0.114090};
            mGParamsT["IIb2"]  = {1.00646,0.113283 , 0.473176};
            mLParamsT["IIb2"]  = {1.00274,0.0430719,-0.139806};

            mBParamsT["IIb34"] = {1.00439,0.132141 ,0.192872};
            mGParamsT["IIb34"] = {1.00598,0.0773232,0.530291};
            mLParamsT["IIb34"] = {1.00236,0.0227811,0.0591825};
        } else if (mToniV==4) {
            cout << "Using the D0-sigma'd toni's h7 parameters!" << endl;
            // Eta ranges:       0.0-2.6
            mBParamsT["IIa"]   = {1.14332,0.149368 ,0.967528};
            mGParamsT["IIa"]   = {1.06078,0.0770220,0.952779};
            mLParamsT["IIa"]   = {1.16264,0.146387 ,0.985315};

            mBParamsT["IIb1"]  = {1.00343,0.0323835 ,0.504105};
            mGParamsT["IIb1"]  = {1.00679,0.0292848 ,0.732646};
            mLParamsT["IIb1"]  = {1.00193,0.00659692,0.493740};

            mBParamsT["IIb2"]  = {1.00625,0.145384 ,0.250399};
            mGParamsT["IIb2"]  = {1.00648,0.106359 ,0.490448};
            mLParamsT["IIb2"]  = {1.00306,0.0296559,0.0717985};

            mBParamsT["IIb34"] = {1.00508,0.0984636,0.306097};
            mGParamsT["IIb34"] = {1.00598,0.0738458,0.542433};
            mLParamsT["IIb34"] = {1.00259,0.0194056,0.181470};
        } else {
            cout << "Erroneous mToniV param! " << mToniV << endl;
            return;
        }
    }
    if (!mToni or mDoPseudoEta) {
        cout << "Using D0's parameters!" << endl;
        if (!mToni) mEtaLims = mD0Etas;
        // Eta ranges:       0.0-0.4                           0.4-0.8                           0.8-1.6                           1.6-2.5
        mBParams["IIa"]   = {0.970084,0.000234787, 2.58402e-08,0.971161,0.00020467 ,-1.83704e-08,0.949167,0.000419675, 1.60527e-08,0.969763,0.000261364, 7.3912e-09 };
        mGParams["IIa"]   = {0.983459,7.64048e-05, 3.50195e-09,0.985379,1.22063e-05, 4.52792e-07,0.931708,0.000694939,-2.32829e-06,0.952751,0.000723493,-3.42777e-06};
        mLParams["IIa"]   = {0.999494,7.07541e-05,-1.45269e-07,1.00036 ,4.68032e-05,-1.56102e-08,0.999736,0.000259825,-9.23827e-07,1.00556 ,7.75845e-05,-4.21275e-08};

        mBParams["IIb1"]  = {0.907158,0.00148826 ,-6.07181e-06,0.941923,0.000967988,-4.14093e-06,0.911252,0.00135347 ,-5.68893e-06,0.882368, 0.00167452,-3.39724e-06};
        mGParams["IIb1"]  = {0.946558,0.000747613,-3.05711e-06,0.972534,0.000322853,-1.10988e-06,0.958175,0.000524708,-1.98458e-06,0.934414, 0.00126379,-6.86699e-06};
        mLParams["IIb1"]  = {1.00329 ,8.06946e-05,-2.69637e-07,1.00087 ,8.72119e-05,-3.51343e-07,1.00146 ,0.000183863,-8.93431e-07,1.01421 ,-9.3816e-06, 4.4304e-07 };

        mBParams["IIb2"]  = {0.895682,0.00151564 ,-5.96675e-06,0.905677,0.00150538 ,-6.49127e-06,0.875531,0.00193077 ,-8.25493e-06,0.875161, 0.00137978 ,-5.47355e-08};
        mGParams["IIb2"]  = {0.95582 ,0.000435297,-1.16437e-06,0.963341,0.000361803,-1.00702e-06,0.941051,0.000789451,-3.25819e-06,0.90449 , 0.00207789 ,-1.2088e-05 };
        mLParams["IIb2"]  = {1.00098 ,0.000164347,-7.05496e-07,1.00057 ,0.000158458,-6.51052e-07,1.0028  ,0.000226434,-1.00477e-06,1.01991 ,-5.92319e-05, 4.01166e-08};

        mBParams["IIb34"] = {0.922772,0.000965453,-3.49557e-06,0.897908, 0.00167565 ,-7.41905e-06,0.852565,0.00243029 ,-1.10229e-05,0.900138, 0.00115205 , 4.6663e-08 };
        mGParams["IIb34"] = {0.972868,0.000429794,-2.41643e-06,0.973974,-2.29939e-05, 1.61564e-06,0.940709,0.000750899,-2.9997e-06 ,0.916924, 0.00203593 ,-1.31268e-05};
        mLParams["IIb34"] = {1.00089 ,6.53228e-05, 7.23081e-08,0.998067, 0.000269549,-1.50666e-06,0.997562,0.000420081,-2.17188e-06,1.01954 ,-0.000243533, 2.02894e-06};
    }

    /* Mapping from the era-wise thinking to ptcorr thinking. */
    mLMap["IIa"]   = "IIa_lq";
    mLMap["IIb1"]  = "IIb_lq";
    mLMap["IIb2"]  = "IIb_lq";
    mLMap["IIb34"] = "IIb_lq";
    if (mToniV==0) {
        cout << "Setting up the old kind of ptcorr params!" << endl;
        /* In the old mode, only light quark corrections are used. */
        mBMap["IIa"]   = "IIa_lq";
        mGMap["IIa"]   = "IIa_lq";
        mBMap["IIb1"]  = "IIb_lq";
        mGMap["IIb1"]  = "IIb_lq";
        mBMap["IIb2"]  = "IIb_lq";
        mGMap["IIb2"]  = "IIb_lq";
        mBMap["IIb34"] = "IIb_lq";
        mGMap["IIb34"] = "IIb_lq";

        mPtEtas  ["IIa_lq"] = {0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5};
        mPtEtas  ["IIb_lq"] = {0.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1,1.1,1.2,1.3,1.4,1.5,1.6,1.7,1.8,1.9,2,2.1,2.2,2.3,2.4,2.5};
        mPtParams["IIa_lq"] = {0.846813,1.45457,0.429688,0.838281,1.48824,0.41867,0.837538,1.49697,0.416602,0.828164,1.79391,0.356977,0.84504,1.55061,0.413835,0.827995,1.57967,0.39759,0.81948,1.62397,0.387136,0.805038,1.94463,0.329272,0.824946,1.88201,0.363378,0.838704,1.55559,0.444612,0.853863,1.31169,0.523816,0.920081,1.25666,0.595413,0.944565,1.1234,0.630575,0.845358,1.38189,0.490669,0.808601,1.2061,0.497993,0.776507,1.73674,0.328942,0.79319,1.48314,0.382419,0.767814,2.06584,0.232779,0.760134,2.23273,0.193072,0.761282,2.13905,0.198157,0.745221,3.02239,0.0682905,0.749095,2.03277,0.2,0.742266,2.9023,0.0734203,0.737458,3.88248,-0.0276475,0.725702,4.86166,-0.139378};
        mPtParams["IIb_lq"] = {0.896787,1.8358,0.383672,0.888111,1.80597,0.384683,0.891682,1.82026,0.383599,0.881918,2.05838,0.344948,0.890354,1.61111,0.423276,0.885529,1.75445,0.396741,0.881526,1.66974,0.4112,0.868526,1.86916,0.377162,0.888199,1.72265,0.424129,0.901522,1.50517,0.485568,0.914712,1.27887,0.553842,0.948249,1.36508,0.558067,0.998078,1.29542,0.584019,0.874594,1.64743,0.422242,0.872763,1.15303,0.548522,0.83984,1.64206,0.399162,0.844312,1.23375,0.482252,0.809015,1.62553,0.355408,0.801797,1.84077,0.305339,0.800952,1.99086,0.264921,0.789443,2.82168,0.139543,0.789198,2.29426,0.2,0.782255,2.85257,0.116783,0.769352,3.49478,0.0326327,0.763087,4.18268,-0.0514641};
        for (auto &ptpars : mPtParams) {
            string ptname = ptpars.first;
            auto &parvec  = ptpars.second;
            float offset = SubText(ptname,"IIa") ? 2 : 4;
            unsigned bins = TMath::Max(int(mPtEtas[ptname].size())-1,0);
            if (parvec.size() != 3*bins) { cout << "PtCorr parameters erroneous! " << parvec.size() << " vs. " << bins << endl; return; }

            mPtCorr[ptname] = vector<PtCorr>();
            auto &PtVec = mPtCorr[ptname];
            for (unsigned bin = 0; bin < bins; ++bin) {
                PtVec.emplace_back(PtCorr{TF1(Form("fit_PtCorr_%s_%d",ptname.c_str(),bin+1),"[0]*(1 - [1]*pow(x,[2]-1))",20,2000),offset});
                PtVec.back().fun.SetParameters(parvec[3*bin],parvec[3*bin+1],parvec[3*bin+2]);
            }
        }
    } else {
        cout << "Setting up the new kind of ptcorr params!" << endl;
        /* In the new mode, also other corrections are available. */
        mBMap["IIa"]   = "IIa_bq";
        mGMap["IIa"]   = "IIa_g";
        mBMap["IIb1"]  = "IIb_bq";
        mGMap["IIb1"]  = "IIb_g";
        mBMap["IIb2"]  = "IIb_bq";
        mGMap["IIb2"]  = "IIb_g";
        mBMap["IIb34"] = "IIb_bq";
        mGMap["IIb34"] = "IIb_g";

        mPtEtas["IIa_lq"]  = {0.0,2.5};
        mPtEtas["IIa_g"]   = {0.0,2.5};
        mPtEtas["IIa_bq"]  = {0.0,2.5};
        mPtEtas["IIa_all"] = {0.0,2.5};
        mPtEtas["IIb_lq"]  = {0.0,2.5};
        mPtEtas["IIb_g"]   = {0.0,2.5};
        mPtEtas["IIb_bq"]  = {0.0,2.5};
        mPtEtas["IIb_all"] = {0.0,2.5};

        if (mToniV==1) {
            mPtParams["IIa_all"] = {-4.19084,0.781317};
            mPtParams["IIa_bq"]  = {-5.88405,0.786317};
            mPtParams["IIa_g"]   = {-5.49117,0.776909};
            mPtParams["IIa_lq"]  = {-4.99114,0.803033};
            mPtParams["IIb_all"] = {-4.83383,0.82882 };
            mPtParams["IIb_bq"]  = {-6.94377,0.835993};
            mPtParams["IIb_g"]   = {-6.48518,0.824181};
            mPtParams["IIb_lq"]  = {-5.75274,0.854398};
        } else if (mToniV==2) {
            mPtParams["IIa_all"] = {-4.19084,0.781317};
            mPtParams["IIa_bq"]  = {-5.88405,0.786317};
            mPtParams["IIa_g"]   = {-5.49117,0.776909};
            mPtParams["IIa_lq"]  = {-4.99114,0.803033};
            mPtParams["IIb_all"] = {-4.83136,0.82878};
            mPtParams["IIb_bq"]  = {-6.8926,0.835461};
            mPtParams["IIb_g"]   = {-6.48579,0.824167};
            mPtParams["IIb_lq"]  = {-5.74974,0.854348};
        } else if (mToniV==3) {
            mPtParams["IIa_all"] = {-4.32544,0.782667};
            mPtParams["IIa_bq"]  = {-5.31473,0.784748};
            mPtParams["IIa_g"]   = {-5.48483,0.780233};
            mPtParams["IIa_lq"]  = {-4.87342,0.79992};
            mPtParams["IIb_all"] = {-4.89472,0.829418};
            mPtParams["IIb_bq"]  = {-6.21085,0.834661};
            mPtParams["IIb_g"]   = {-6.40955,0.827073};
            mPtParams["IIb_lq"]  = {-5.53746,0.850336};
        } else if (mToniV==4) {
            mPtParams["IIa_all"] = {-4.32544,0.782667};
            mPtParams["IIa_bq"]  = {-5.31473,0.784748};
            mPtParams["IIa_g"]   = {-5.48483,0.780233};
            mPtParams["IIa_lq"]  = {-4.87342,0.79992};
            mPtParams["IIb_all"] = {-4.89472,0.829418};
            mPtParams["IIb_bq"]  = {-6.21085,0.834661};
            mPtParams["IIb_g"]   = {-6.40955,0.827073};
            mPtParams["IIb_lq"]  = {-5.53746,0.850336};
        } else {
            cout << "Erroneous mToniV param! " << mToniV << endl;
            return;
        }

        for (auto &ptpars : mPtParams) {
            string ptname = ptpars.first;
            auto &parvec  = ptpars.second;
            unsigned bins = TMath::Max(int(mPtEtas[ptname].size())-1,0);
            if (parvec.size() != 2*bins) { cout << "PtCorr parameters erroneous! " << ptname << " " << parvec.size() << " vs. " << bins << endl; return; }

            mPtCorr[ptname] = vector<PtCorr>();
            auto &PtVec = mPtCorr[ptname];
            for (unsigned bin = 0; bin < bins; ++bin) {
                PtVec.emplace_back(PtCorr{TF1(Form("fit_PtCorr_%s_%d",ptname.c_str(),bin+1),"[0]/x+[1]",20,2000),0});
                PtVec.back().fun.SetParameters(parvec[2*bin],parvec[2*bin+1]);
            }
        }
    }
    cout << "Setup of ptcorr params successful!" << endl;

    if (mDoTFShift) {
        cout << "Performing Transfer function energy shift for jets!" << endl;
        mExVals = {10,11,12,13,14,15,16,18,20,22,24,26,28,30,35,40,45,50,55,60,65,70,85,100,115,130,
                   160,190,220,250,280,310,340,370,400,430,460,490,520,550,580,610,640,670,700,850,
                   1000,1150,1300,1450,1600,1750};
        mBMaps["IIa"] =
        {{18.7897,19.7107,20.6539,21.615,22.59,23.5757,24.5696,26.5738,28.5908,30.6132,32.6366,34.6585,36.6771,38.6916,43.7072,48.6929,53.6511,58.5852,63.4989,68.3955,73.2778,78.1485,92.7085,107.218,121.698,136.162,165.062,193.946,222.825,251.701,280.577,309.454,338.331,367.208,396.086,424.965,453.845,482.725,511.605,540.486,569.367,598.249,627.13,656.012,684.895,829.308,973.725,1118.14,1262.56,1406.99,1551.41,1695.83},
         {7.41215,8.95415,19.7111,20.6885,21.6824,22.6892,23.7058,25.7591,27.8285,29.9058,31.9856,34.065,36.1419,38.2153,43.3794,48.5139,53.6201,58.7005,63.7585,68.7972,73.8194,78.8276,93.7882,108.683,123.538,138.367,167.985,197.573,227.148,256.718,286.284,315.849,345.413,374.978,404.542,434.107,463.672,493.237,522.802,552.368,581.934,611.5,641.066,670.633,700.2,848.036,995.875,1143.72,1291.56,1439.4,1587.24,1735.09},
         {18.5615,19.6467,20.733,21.8205,22.9091,23.9986,25.0892,27.2734,29.4614,31.653,33.8482,36.0465,38.248,40.4521,45.9731,51.5064,57.0488,62.5981,68.1523,73.71,79.2702,84.8322,101.523,118.216,134.908,151.596,184.965,218.323,251.674,285.019,318.359,351.695,385.029,418.36,451.69,485.018,518.345,551.671,584.996,618.321,651.644,684.968,718.291,751.613,784.935,951.541,1118.14,1284.74,1451.34,1617.93,1784.53,1951.12},
         {-0.848128,0.622028,2.09341,3.5657,5.03856,6.51164,7.98457,10.9285,13.8673,16.7984,19.7192,22.6278,25.5225,28.4022,35.531,42.5567,49.4819,56.3125,63.0551,69.7157,76.2995,82.8107,101.936,120.473,138.419,155.743,188.411,218.66,247.67,276.735,306.33,336.429,366.907,397.656,428.599,459.684,490.876,522.148,553.483,584.868,616.293,647.751,679.236,710.743,742.268,900.098,1058.13,1216.29,1374.51,1532.77,1691.07,1849.4}};
        mBMaps["IIb1"] =
        {{16.8991,17.9739,19.0407,20.1005,21.1539,22.2014,23.2435,25.313,27.3648,29.401,31.4236,33.4339,35.4334,37.4233,42.3627,47.2622,52.1324,56.9811,61.8142,66.6358,71.4493,76.2569,90.659,105.047,119.432,133.817,162.594,191.379,220.172,248.969,277.771,306.576,335.383,364.192,393.003,421.814,450.627,479.44,508.255,537.069,565.885,594.7,623.516,652.332,681.149,825.235,969.324,1113.42,1257.51,1401.6,1545.7,1689.79},
         {16.2778,17.3267,18.3798,19.4342,20.4881,21.5406,22.591,24.6842,26.7663,28.8369,30.8964,32.9454,34.9845,37.0145,42.0542,47.0514,52.0151,56.9525,61.8694,66.7704,71.6593,76.5388,91.14,105.71,120.267,134.818,163.914,193.012,222.114,251.22,280.329,309.44,338.554,367.669,396.786,425.903,455.022,484.142,513.262,542.383,571.504,600.626,629.748,658.87,687.993,833.61,979.23,1124.85,1270.48,1416.1,1561.73,1707.36},
         {3.67954,4.97661,6.27633,7.58153,8.89719,10.2318,11.6001,26.2127,28.2411,30.3041,32.3898,34.4903,36.5999,38.7148,44.0096,49.2989,54.5724,59.8262,65.0589,70.2706,75.4624,80.6355,96.0572,111.362,126.582,141.74,171.941,202.049,232.109,262.142,292.159,322.168,352.172,382.173,412.172,442.17,472.168,502.166,532.163,562.161,592.159,622.157,652.155,682.154,712.153,862.151,1012.15,1162.16,1312.17,1462.18,1612.2,1762.21},
         {10.9914,12.3143,13.6384,14.9634,16.2891,17.6154,18.9419,21.5949,24.2459,26.8929,29.5341,32.1676,34.7919,37.4057,43.888,50.2892,56.6072,62.8444,69.0053,75.0953,81.1195,87.0824,104.64,121.75,138.451,154.755,186.191,216.148,244.942,273.096,301.081,329.154,357.4,385.824,414.403,443.11,471.919,500.811,529.77,558.784,587.842,616.938,646.064,675.217,704.392,850.505,996.867,1143.37,1289.96,1436.6,1583.28,1730.}};
        mBMaps["IIb2"] =
        {{17.2119,18.2645,19.3166,20.3668,21.4143,22.4586,23.4996,25.5712,27.6292,29.6743,31.7074,33.7295,35.7416,37.7447,42.7186,47.6536,52.5592,57.443,62.3105,67.166,72.0126,76.8527,91.3488,105.827,120.299,134.77,163.714,192.664,221.62,250.58,279.544,308.51,337.479,366.449,395.421,424.393,453.367,482.341,511.316,540.292,569.268,598.244,627.221,656.198,685.175,830.064,974.956,1119.85,1264.75,1409.64,1554.54,1699.44},
         {16.7231,17.7941,18.8673,19.9397,21.0099,22.0771,23.1409,25.2575,27.3594,29.4472,31.5217,33.584,35.6348,37.6753,42.7365,47.7503,52.7265,57.6735,62.5976,67.504,72.3966,77.2786,91.8822,106.45,121.002,135.546,164.628,193.711,222.797,251.887,280.98,310.075,339.173,368.273,397.374,426.476,455.58,484.684,513.789,542.894,572.001,601.107,630.214,659.321,688.429,833.97,979.515,1125.06,1270.61,1416.16,1561.71,1707.27},
         {4.20464,5.49895,6.79443,8.09261,9.3963,10.7104,12.0433,14.836,29.1755,31.2164,33.2871,35.3776,37.4811,39.5926,44.8872,50.1833,55.4675,60.734,65.9805,71.2066,76.4125,81.5993,97.0574,112.389,127.627,142.795,172.993,203.08,233.106,263.099,293.071,323.031,352.984,382.932,412.878,442.823,472.766,502.709,532.651,562.594,592.536,622.479,652.422,682.365,712.308,862.028,1011.75,1161.48,1311.21,1460.95,1610.68,1760.42},
         {10.3159,11.6938,13.0731,14.4535,15.8346,17.216,18.5973,21.3581,24.1137,26.8612,29.5978,32.3212,35.0299,37.7225,44.3792,50.9283,57.3753,63.7292,69.9997,76.1953,82.3237,88.3908,106.268,123.714,140.761,157.41,189.452,219.733,248.443,276.25,303.887,331.739,359.892,388.316,416.958,445.769,474.709,503.75,532.87,562.053,591.286,620.56,649.868,679.204,708.564,855.611,1002.91,1150.35,1297.87,1445.44,1593.05,1740.69}};
        mBMaps["IIb34"] =
        {{16.6075,17.5643,18.5322,19.509,20.4928,21.4822,22.4758,24.4717,26.4746,28.4807,30.4875,32.4939,34.4989,36.5022,41.502,46.4901,51.4686,56.4394,61.4044,66.3649,71.3223,76.2771,91.1325,105.981,120.826,135.671,165.361,195.053,224.746,254.442,284.138,313.836,343.535,373.234,402.934,432.634,462.334,492.035,521.737,551.438,581.14,610.842,640.544,670.246,699.948,848.46,996.974,1145.49,1294.,1442.52,1591.04,1739.55},
         {17.1992,18.1444,19.1044,20.0764,21.0578,22.0465,23.0408,25.0405,27.0488,29.0604,31.0722,33.0824,35.0898,37.0937,42.0876,47.0592,52.0113,56.9474,61.8705,66.7833,71.6879,76.5861,91.2544,105.899,120.533,135.16,164.407,193.652,222.896,252.142,281.389,310.637,339.887,369.137,398.387,427.639,456.89,486.143,515.395,544.648,573.902,603.155,632.409,661.663,690.917,837.189,983.463,1129.74,1276.02,1422.29,1568.57,1714.85},
         {-0.270636,1.08254,2.43572,3.78891,5.14214,24.988,25.9497,27.8932,29.8627,31.8561,33.8705,35.9031,37.9508,40.011,45.2009,50.4256,55.6664,60.9118,66.1552,71.3927,76.6221,81.8424,97.4482,112.98,128.453,143.881,174.649,205.342,235.993,266.621,297.233,327.836,358.433,389.027,419.617,450.206,480.794,511.382,541.968,572.554,603.14,633.726,664.312,694.898,725.484,878.413,1031.34,1184.28,1337.21,1490.14,1643.08,1796.01},
         {12.646,13.8724,15.099,16.3259,17.5531,18.7804,20.0078,22.4628,24.9176,27.3717,29.8245,32.2757,34.7246,37.1707,43.2713,49.3457,55.3892,61.3981,67.3702,73.3043,79.2,85.0572,102.404,119.427,136.147,152.581,184.643,215.727,245.985,275.622,304.86,333.891,362.848,391.811,420.817,449.884,479.014,508.203,537.448,566.741,596.078,625.452,654.859,684.294,713.755,861.344,1009.25,1157.34,1305.55,1453.84,1602.19,1750.58}};
        mLMaps["IIa"] =
        {{11.0476,12.026,13.0044,13.9827,14.9611,15.9394,16.9178,18.8745,20.8311,22.7877,24.7444,26.701,28.6576,30.6142,35.5056,40.397,45.2886,50.1802,55.072,59.9641,64.8566,69.7494,84.4313,99.1197,113.816,128.522,157.963,187.446,216.971,246.535,276.134,305.767,335.431,365.123,394.843,424.588,454.358,484.151,513.968,543.808,573.671,603.557,633.465,663.396,693.35,843.469,994.205,1145.61,1297.77,1450.76,1604.69,1759.71},
         {10.6796,11.6787,12.6779,13.677,14.6761,15.6752,16.6743,18.6725,20.6707,22.6688,24.6669,26.665,28.6631,30.6611,35.6561,40.6511,45.646,50.641,55.6361,60.6315,65.6272,70.6233,85.6153,100.615,115.624,130.645,160.726,190.865,221.063,251.32,281.634,312.002,342.422,372.892,403.411,433.977,464.591,495.25,525.955,556.707,587.504,618.348,649.238,680.176,711.162,866.843,1023.88,1182.44,1342.76,1505.12,1669.83,1837.32},
         {5.98642,7.07023,8.15361,9.23653,10.3189,11.4008,12.4822,14.643,16.8011,18.9564,21.1085,23.2574,25.4027,27.5445,32.8823,38.1949,43.4813,48.7407,53.9726,59.1768,64.3533,69.5021,84.784,99.8251,114.636,129.229,157.844,185.86,213.496,240.94,268.322,295.712,323.144,350.627,378.163,405.748,433.377,461.044,488.744,516.473,544.227,572.002,599.795,627.605,655.429,794.711,934.174,1073.75,1213.39,1353.08,1492.8,1632.55},
         {-8.00359,-6.70121,-5.39818,-4.09458,-2.7905,-1.48607,-0.181391,2.42817,5.03709,7.64426,10.2485,12.8489,15.4442,18.0336,24.4762,30.867,37.2,43.4722,49.6825,55.8311,61.9188,67.9466,85.6789,102.895,119.593,135.764,166.538,195.575,223.778,251.982,280.522,309.42,338.608,368.015,397.585,427.278,457.066,486.928,516.848,546.814,576.819,606.854,636.916,666.999,697.101,847.81,998.728,1149.77,1300.88,1452.03,1603.23,1754.44}};
        mLMaps["IIb1"] =
        {{11.8889,12.8656,13.8423,14.819,15.7957,16.7724,17.7491,19.7025,21.656,23.6095,25.563,27.5165,29.47,31.4236,36.3078,41.1922,46.0768,50.9617,55.8469,60.7325,65.6183,70.5044,85.1647,99.8279,114.494,129.162,158.507,187.861,217.225,246.598,275.98,305.371,334.77,364.178,393.594,423.019,452.452,481.894,511.344,540.803,570.27,599.746,629.23,658.722,688.223,835.856,983.704,1131.77,1280.06,1428.57,1577.3,1726.27},
         {11.5425,12.5331,13.5237,14.5143,15.5049,16.4955,17.4861,19.4673,21.4486,23.4298,25.4111,27.3924,29.3738,31.3552,36.3088,41.2626,46.2168,51.1714,56.1263,61.0816,66.0373,70.9935,85.8646,100.74,115.619,130.502,160.279,190.072,219.877,249.696,279.528,309.373,339.229,369.099,398.98,428.873,458.779,488.697,518.626,548.568,578.522,608.488,638.467,668.457,698.46,848.658,999.167,1149.99,1301.15,1452.63,1604.45,1756.62},
         {4.60955,5.82698,7.04666,8.26833,9.49165,10.7162,11.9416,14.3927,16.8407,19.2811,21.7095,24.1225,26.517,28.891,34.7291,40.4262,45.9893,51.4301,56.7612,61.9948,67.1423,72.2145,87.0807,101.607,115.977,130.301,159.,187.837,216.795,245.842,274.952,304.109,333.298,362.513,391.746,420.995,450.255,479.525,508.803,538.088,567.377,596.672,625.97,655.271,684.576,831.129,977.718,1124.33,1270.95,1417.58,1564.22,1710.86},
         {4.7541,5.87649,6.99883,8.1211,9.24327,10.3653,11.4873,13.7306,15.9733,18.2149,20.4556,22.6949,24.9329,27.1694,32.7528,38.3237,43.8802,49.4208,54.9442,60.4493,65.9354,71.4018,87.6786,103.767,119.666,135.375,166.229,196.348,225.78,254.626,283.034,311.171,339.183,367.172,395.198,423.288,451.452,479.689,507.994,536.361,564.782,593.251,621.761,650.308,678.886,822.137,965.782,1109.66,1253.68,1397.81,1542.,1686.24}};
        mLMaps["IIb2"] =
        {{11.8967,12.8724,13.8481,14.8239,15.7996,16.7754,17.7511,19.7026,21.6541,23.6057,25.5573,27.5089,29.4605,31.4121,36.2914,41.171,46.0507,50.9308,55.8112,60.6918,65.5728,70.454,85.0997,99.7481,114.399,129.053,158.369,187.694,217.028,246.372,275.724,305.085,334.454,363.832,393.218,422.613,452.016,481.427,510.847,540.275,569.712,599.156,628.609,658.071,687.541,835.016,982.704,1130.61,1278.72,1427.06,1575.62,1724.41},
         {11.525,12.5115,13.498,14.4846,15.4711,16.4576,17.4442,19.4173,21.3904,23.3635,25.3366,27.3098,29.283,31.2562,36.1895,41.1231,46.057,50.9912,55.9259,60.8609,65.7965,70.7324,85.5432,100.358,115.177,130.001,159.659,189.332,219.019,248.719,278.431,308.156,337.892,367.64,397.4,427.172,456.955,486.749,516.556,546.373,576.202,606.043,635.896,665.76,695.635,845.19,995.043,1145.2,1295.67,1446.46,1597.58,1749.04},
         {8.00931,9.03515,10.061,11.0868,12.1126,13.1384,14.1642,16.2157,18.2673,20.3187,22.3702,24.4217,26.4731,28.5245,33.6529,38.7814,43.9098,49.0384,54.1671,59.2961,64.4254,69.555,84.9467,100.344,115.748,131.159,162.009,192.898,223.828,254.802,285.819,316.881,347.987,379.136,410.329,441.566,472.846,504.168,535.534,566.941,598.391,629.883,661.417,692.992,724.608,883.3,1042.99,1203.66,1365.26,1527.78,1691.17,1855.39},
         {3.21953,4.40217,5.57354,6.7338,7.88401,9.0255,10.1596,12.4102,14.6431,16.8633,19.0741,21.2777,23.4756,25.6687,31.1343,36.5784,42.0017,47.4022,52.7767,58.121,63.4301,68.6986,84.2013,99.1638,113.642,127.865,156.177,184.584,213.098,241.687,270.33,299.009,327.715,356.441,385.183,413.937,442.701,471.473,500.251,529.034,557.823,586.615,615.41,644.208,673.009,817.039,961.101,1105.18,1249.27,1393.37,1537.48,1681.59}};
        mLMaps["IIb34"] =
        {{11.6471,12.6281,13.609,14.59,15.571,16.5519,17.5329,19.4948,21.4568,23.4187,25.3807,27.3426,29.3046,31.2666,36.1718,41.0771,45.9826,50.8883,55.7944,60.7008,65.6075,70.5146,85.2381,99.9653,114.696,129.432,158.914,188.412,217.925,247.453,276.996,306.554,336.126,365.712,395.312,424.925,454.553,484.194,513.849,543.518,573.201,602.897,632.607,662.331,692.068,840.964,990.208,1139.81,1289.77,1440.09,1590.8,1741.89},
         {11.5109,12.4945,13.4781,14.4617,15.4453,16.4289,17.4125,19.3797,21.3469,23.3141,25.2813,27.2486,29.2158,31.1831,36.1014,41.0198,45.9385,50.8575,55.7768,60.6965,65.6165,70.5369,85.3008,100.069,114.842,129.619,159.188,188.775,218.379,248.001,277.64,307.294,336.964,366.65,396.351,426.067,455.799,485.545,515.306,545.082,574.873,604.68,634.501,664.337,694.188,843.67,993.535,1143.79,1294.46,1445.53,1597.04,1748.99},
         {8.60745,9.62617,10.6449,11.6636,12.6822,13.7009,14.7196,16.7569,18.7941,20.8313,22.8685,24.9056,26.9427,28.9798,34.0723,39.1647,44.257,49.3492,54.4415,59.5338,64.6263,69.7189,84.9985,100.282,115.57,130.864,161.475,192.121,222.806,253.532,284.302,315.117,345.976,376.881,407.83,438.823,469.862,500.944,532.069,563.238,594.449,625.702,656.997,688.333,719.709,877.185,1035.6,1194.9,1355.01,1515.88,1677.43,1839.63},
         {3.83791,4.95797,6.07798,7.19792,8.31779,9.43756,10.5572,12.7962,15.0346,17.2723,19.5092,21.7452,23.9803,26.2142,31.7937,37.3644,42.9251,48.4745,54.0117,59.5359,65.0464,70.5425,86.9405,103.196,119.305,135.264,166.724,197.564,227.783,257.404,286.497,315.176,343.58,371.841,400.062,428.307,456.611,484.989,513.443,541.97,570.565,599.219,627.928,656.683,685.481,829.941,974.922,1120.21,1265.69,1411.3,1557.,1702.77}};

        for (auto &vecs : mBMaps) {
            auto &era = vecs.first;
            int eta = 1;
            for (auto &vec : vecs.second) {
                if (vec.size() != mExVals.size()) cout << "Hox! Size mismatch in B era " << era << " in eta section " << eta << endl;
                ++eta;
                float prev = -4000;
                for (auto &Ey : vec) {
                    if (prev!=-4000 and prev > Ey)
                        cout << "Hox! E getting smaller (" << prev << " vs. " << Ey << ") in B era " << era << " in eta section " << eta << endl;
                    prev = Ey;
                }
            }
        }
        for (auto &vecs : mLMaps) {
            auto &era = vecs.first;
            int eta = 1;
            for (auto &vec : vecs.second) {
                if (vec.size() != mExVals.size()) cout << "Hox! Size mismatch in L era " << era << " in eta section " << eta << endl;
                ++eta;
                float prev = -4000;
                for (auto &Ey : vec) {
                    if (prev!=-4000 and prev > Ey)
                        cout << "Hox! E getting smaller (" << prev << " vs. " << Ey << ") in L era " << era << " in eta section " << eta << endl;
                    prev = Ey;
                }
            }
        }
        cout << "Setup of tfshift params successful!" << endl;
    }

    mKJESVals["IIa"] = 0.993;
    mKJESVals["IIb1"] = 1.027;
    mKJESVals["IIb2"] = 1.033;
    mKJESVals["IIb34"] = 1.026;
    mKJES["El"]["IIa"  ] = 1.000;
    mKJES["El"]["IIb1" ] = 1.016;
    mKJES["El"]["IIb2" ] = 1.034;
    mKJES["El"]["IIb34"] = 1.027;
    mKJES["Mu"]["IIa"  ] = 0.986;
    mKJES["Mu"]["IIb1" ] = 1.038;
    mKJES["Mu"]["IIb2" ] = 1.032;
    mKJES["Mu"]["IIb34"] = 1.025;
    mKJESSlope["El"]["IIa"  ] = 0.83;
    mKJESSlope["El"]["IIb1" ] = 0.93;
    mKJESSlope["El"]["IIb2" ] = 0.95;
    mKJESSlope["El"]["IIb34"] = 1.07;
    mKJESSlope["Mu"]["IIa"  ] = 0.89;
    mKJESSlope["Mu"]["IIb1" ] = 0.94;
    mKJESSlope["Mu"]["IIb2" ] = 0.94;
    mKJESSlope["Mu"]["IIb34"] = 1.05;
    mKJESOffset["El"]["IIa"  ] = -0.001;
    mKJESOffset["El"]["IIb1" ] =  0.000;
    mKJESOffset["El"]["IIb2" ] = -0.005;
    mKJESOffset["El"]["IIb34"] = -0.017;
    mKJESOffset["Mu"]["IIa"  ] = -0.007;
    mKJESOffset["Mu"]["IIb1" ] = -0.011;
    mKJESOffset["Mu"]["IIb2" ] = -0.007;
    mKJESOffset["Mu"]["IIb34"] = -0.013;
    for (auto &era : mEras) {
        mKJESCorr["El"][era] = CKJES(mKJESSlope["El"][era],mKJESOffset["El"][era]);
        mKJESCorr["Mu"][era] = CKJES(mKJESSlope["Mu"][era],mKJESOffset["Mu"][era]);
    }
    cout << "Setup of kjes params successful!" << endl;

    if (mDoTF) {
        mParAMaps["IIa"] =
        {{-1.29 , 4.04, 0, 23.7, 18.9,  2.92, 2.77, 0,-9.96, 3.82, 6.06, 1.67, 0, 46.0, 18.3},
         {-0.687, 3.64, 0, 24.8, 18.9,  4.34, 3.13, 0,-8.77, 3.45, 6.88, 1.71, 0, 47.1, 20.9},
         { 4.53 , 3.32, 0, 9.02, 13.0, 12.9 , 5.24, 0,-5.98, 1.63, 6.00, 6.04, 0, 60.0, 10.0},
         {16.2  , 2.78, 0, 18.6, 14.7, 10.7 , 3.61, 0, 5.35, 13.8, 32.8, 8.34, 0, 20.7, 11.8}};
        mParAMaps["IIb1"] =
        {{-2.17, 4.08 , 0, 12.7 , 14.6, 0.874, 2.67, 0,-8.38,  4.01,  5.26, 1.45, 0, 37.8 , 18.5 },
         {-1.65, 3.79 , 0, 16.1 , 14.7, 2.67 , 2.79, 0,-7.79,  3.42,  5.00, 1.33, 0, 40.0 ,  0.  },
         { 6.21, 0.873, 0,  2.57, 10.1, 7.16 , 4.31, 0,-9.69,  3.41,  8.00, 6.04, 0, 60.0 , 10.0 },
         { 5.79, 2.98 , 0,  9.11, 17.2, 1.69 , 3.09, 0,-10.8, 13.3 , 28.0 , 7.99, 0,  9.46,  8.78}};
        mParAMaps["IIb2"] =
        {{-2.19, 4.23, 0, 14.1 , 14.8, 1.22, 2.55, 0, -8.57,  3.85, 1.60, 3.66, 0,  7.48,  4.56},
         {-1.68, 4.12, 0, 16.8 , 15.6, 2.19, 2.53, 0, -8.54,  3.59, 6.00, 1.38, 0, 37.5 , 15.9 },
         { 2.20, 5.77, 0, 21.6 , 19.1, 6.75, 4.14, 0, -10.9,  3.25, 8.00, 5.54, 0,-40.0 , 41.0 },
         { 7.33, 3.16, 0,  5.54, 1.60, 2.55, 4.56, 0, -12.1, 13.2 , 9.25, 3.35, 0, 24.0 , 35.5 }};
        mParAMaps["IIb34"] =
        {{-1.87, 4.11, 0, 17.0, 16.3,  4.64 , 2.78 , 0,-6.31, 3.56,  6.72, 1.56, 0, 32.7 , 13.2},
         {-1.70, 4.17, 0, 18.9, 17.7,  4.22 , 2.91 , 0,-7.71, 3.43,  6.70, 1.64, 0, 36.8 , 16.6},
         { 1.56, 5.96, 0, 21.2, 20.6, 10.2  , 4.92 , 0,-9.51, 3   ,  8.00, 6.04, 0, 60.0 , 10.0},
         { 6.59, 2.77, 0, 12.6, 17.8, -0.329, 0.964, 0,-6.32, 14.7, 23.5 , 6.73, 0,  9.75, 12.9}};
        mParBMaps["IIa"] =
        {{ 0.0221 , 0.109, 0.000235, -0.17  , 0.0964, -0.191, 0.207, 0.0409 , 0.0468, 0.0923,-0.14 , 0.165 , 0.0000868,-0.415, 0.144},
         { 0.00086, 0.119, 0.000273, -0.173 , 0.0984, -0.211, 0.203, 0.034  , 0.0243, 0.0994,-0.156, 0.165 , 0.0000912,-0.341, 0.107},
         {-0.0758 , 0.155, 0.00428 ,  0.079 , 0.0839, -0.158, 0.24 , 0.176  ,-0.0628, 0.182 ,-0.108, 0.130 , 0.001    ,-0.750, 0.200},
         {-0.228  , 0.146, 0.00615 , -0.0013, 0.086 , -0.307, 0.123, 0.00698,-0.0433, 0.0966,-0.428, 0.0776, 0.00706  ,-0.131, 0.108}};
        mParBMaps["IIb1"] =
        {{ 0.0239 , 0.0666, 0.000277,-0.176 , 0.173 ,-0.186, 0.182, 0.0441 , 0.0436 , 0.0524,-0.129, 0.151 , 0.000213,-0.531 , 0.106},
         { 0.00952, 0.0795, 0.000271,-0.206 , 0.172 ,-0.207, 0.182, 0.0365 , 0.034  , 0.0641,-0.115, 0.146 , 0.000499,-0.800 , 0.400},
         {-0.165  , 0.157 , 0.0177  , 0.0267, 0.0636,-0.228, 0.178, 0.0211 , 0.00976, 0.0995,-0.133, 0.108 , 0.001   ,-0.750 , 0.200},
         {-0.107  , 0.140 , 0.00261 , 0.0416, 0.0585,-0.227, 0.127, 0.00569, 0.029  , 0.0871,-0.399, 0.0781, 0.00700 ,-0.0970, 0.113}};
        mParBMaps["IIb2"] =
        {{ 0.0249, 0.0691, 0.000263,-0.181, 0.18  ,-0.192, 0.187, 0.0437, 0.0383, 0.0567,-0.0493, 0.0815, 0.00781 ,-0.191, 0.159},
         { 0.0137, 0.0783, 0.00028 ,-0.216, 0.166 ,-0.202, 0.186, 0.0343, 0.0346, 0.0647,-0.146 , 0.160 , 0.00015 ,-0.359, 0.187},
         {-0.0252, 0.107 , 0.000357,-0.123, 0.119 ,-0.227, 0.178, 0.0193, 0.0123, 0.103 ,-0.139 , 0.128 , 0.000143,-0.222, 0.287},
         {-0.141 , 0.135 , 0.00351 , 0.045, 0.0668,-0.248, 0.113, 0.0059, 0.023 , 0.0886,-0.181 , 0.153 , 0.000168, 0.236,-0.118}};
        mParBMaps["IIb34"] =
        {{ 0.0194, 0.072 , 0.000339,-0.147 , 0.16  ,-0.202, 0.232, 0.0793 , 0.0141, 0.0645,-0.139, 0.153, 0.00026 ,-0.309 , 0.198 },
         { 0.0167, 0.0783, 0.000312,-0.177 , 0.155 ,-0.203, 0.208, 0.0486 , 0.0299, 0.0677,-0.144, 0.156, 0.000198,-0.284 , 0.176 },
         {-0.0184, 0.108 , 0.000387,-0.0965, 0.108 ,-0.261, 0.191, 0.0334 ,-0.0055, 0.116 ,-0.133, 0.108, 0.001   ,-0.750 , 0.200 },
         {-0.106 , 0.137 , 0.00201 , 0.0299, 0.0545,-0.18 , 0.147, 0.00362, 0.0159, 0.0799,-0.325, 0.100, 0.00365 ,-0.0683, 0.0945}};
        auto tl = [](double Ey, float a, float b) { return a + b*Ey; };
    }

    InitFCorr(mEtaLims.size()-1,mToni);
    if (mToni and mDoPseudoEta) InitFCorr(mD0Etas.size()-1,false);
    cout << "Setup of FCorr stuff successful!" << endl;

    /* Ordering of the cuts, important to keep in the correct order! */
    mInsertions.push_back("All");
    mInsertions.push_back("Fake L");
    mInsertions.push_back("Mult L");

    mInsertions.push_back("L kin");
    mInsertions.push_back("L isol");

    mInsertions.push_back("MET");
    mInsertions.push_back("L MET");
    mInsertions.push_back("LM WT");

    mInsertions.push_back("3J Pt");
    mInsertions.push_back("4J Pt");
    mInsertions.push_back("5J Pt");
    mInsertions.push_back("1J Pt");
    mInsertions.push_back("J Eta");
    mInsertions.push_back("J Fail");

    mInsertions.push_back("Bh2+");
    mInsertions.push_back("Bh2-");

    mInsertions.push_back("Tto4");
    mInsertions.push_back("Bin4");
    mInsertions.push_back("Win4");

    mInsertions.push_back("Bsml");
    mInsertions.push_back("Bmtch");

    mElFile = new TFile(Form("%s/results_%s_El.root",loc.c_str(),mMTag.c_str()),"RECREATE");
    InitHistos("El");
    mMuFile = new TFile(Form("%s/results_%s_Mu.root",loc.c_str(),mMTag.c_str()),"RECREATE");
    InitHistos("Mu");

    cout << "Initialization successful!" << endl;
    mInit = true;
}

void TopJets::InitFCorr(int bins, bool toni) {
    for (auto &era : mEras) {
        auto &fbc = toni ? mFBCorrT[era] : mFBCorr[era];
        auto &fgc = toni ? mFGCorrT[era] : mFGCorr[era];
        auto &flc = toni ? mFLCorrT[era] : mFLCorr[era];
        fbc = vector<FCorr>();
        fgc = vector<FCorr>();
        flc = vector<FCorr>();
        for (int pos=0; pos<bins; ++pos) {
            const char* etag = era.c_str();
            /* b jets */
            {
                auto &eraBParams = toni ? mBParamsT[era] : mBParams[era];
                double bp0 = eraBParams[3*pos];
                double bp1 = eraBParams[3*pos+1];
                double bp2 = eraBParams[3*pos+2];
                if (toni) {
                    fbc.emplace_back(FCorr{TF1(Form("fit_Fbcorr_%d_%s",pos,etag),"[0]*(1-[1]*pow(x,[2]-1))",0,140),140,1.0});
                } else {
                    double bmax = -bp1/(2*bp2);
                    if (bmax<0 or bmax>140) bmax = 140;
                    fbc.emplace_back(FCorr{TF1(Form("fit_Fbcorr_%d_%s",pos,etag),"pol2",0,140),bmax,1.0});
                    fbc.back().valmax = fbc.back().fun.Eval(bmax);
                }
                fbc.back().fun.SetParameters(bp0,bp1,bp2);
            }
            /* g jets */
            {
                auto &eraGParams = toni ? mGParamsT[era] : mGParams[era];
                double gp0 = eraGParams[3*pos];
                double gp1 = eraGParams[3*pos+1];
                double gp2 = eraGParams[3*pos+2];
                if (toni) {
                    fgc.emplace_back(FCorr{TF1(Form("fit_Fgcorr_%d_%s",pos,etag),"[0]*(1-[1]*pow(x,[2]-1))",0,140),140,1.0});
                } else {
                    double gmax = -gp1/(2*gp2);
                    if (gmax<0 or gmax>140) gmax = 140;
                    fgc.emplace_back(FCorr{TF1(Form("fit_Fgcorr_%d_%s",pos,etag),"pol2",0,140),gmax,1.0});
                    fgc.back().valmax = fgc.back().fun.Eval(gmax);
                }
                fgc.back().fun.SetParameters(gp0,gp1,gp2);
            }
            /* l jets */
            {
                auto &eraLParams = toni ? mLParamsT[era] : mLParams[era];
                double lp0 = eraLParams[3*pos];
                double lp1 = eraLParams[3*pos+1];
                double lp2 = eraLParams[3*pos+2];
                if (toni) {
                    flc.emplace_back(FCorr{TF1(Form("fit_Fgcorr_%d_%s",pos,etag),"[0]*(1-[1]*pow(x,[2]-1))",0,140),140,1.0});
                } else {
                    double lmax = -lp1/(2*lp2);
                    if (lmax<0 or lmax>140) lmax = 140;
                    flc.emplace_back(FCorr{TF1(Form("fit_Fgcorr_%d_%s",pos,etag),"pol2",0,140),lmax,1.0});
                    flc.back().valmax = flc.back().fun.Eval(lmax);
                }
                flc.back().fun.SetParameters(lp0,lp1,lp2);
            }
        }
    }
}

void TopJets::InitHistos(string tag) {
    unsigned tbins = 1800;
    double   tmin  = 70;
    double   tmax  = 250;

    unsigned wbins = 800;
    double   wmin  = 40;
    double   wmax  = 120;

    unsigned bbins = 90;
    double   bmin  = 20;
    double   bmax  = 300;

    /* Appending histos */
    const char* mtag = mMTag.c_str();
    const char* ftag = tag.c_str();
    mHistos[Form("PtHat_%s",ftag)]  = new TH1D(Form("pthat_%s",mtag),Form("pthat %s",mtag),200,0,500);
    mHistos[Form("NoJets_%s",ftag)] = new TH1D(Form("jets_no_%s",mtag),Form("jetcount %s",mtag),20,0,20);

    mHistos[Form("bDR_%s",ftag)] = new TH1D(Form("bdr_%s",mtag),Form("bj dist %s",mtag),100,0,2.5);
    mHistos[Form("wDR_%s",ftag)] = new TH1D(Form("wdr_%s",mtag),Form("wj dist %s",mtag),100,0,2.5);

    mHistos[Form("jetSectorsL_%s",ftag)] = new TH1D(Form("jetsectorsl_%s",mtag),Form("jetsectorsl %s",mtag),mD0Etas.size()-1,mD0Etas.data());
    mHistos[Form("jetSectorsB_%s",ftag)] = new TH1D(Form("jetsectorsb_%s",mtag),Form("jetsectorsb %s",mtag),mD0Etas.size()-1,mD0Etas.data());

    if (mDoPartonLvl) {
        mHistos[Form("WMassHdrBos_%s",ftag)] = new TH1D(Form("wmass_hadrbos_%s",mtag),Form("wmass hb %s",mtag),wbins,wmin,wmax);
        mHistos[Form("WMassHdrPtn_%s",ftag)] = new TH1D(Form("wmass_hadrptn_%s",mtag),Form("wmass hp %s",mtag),wbins,wmin,wmax);
        mHistos[Form("WMassLptBos_%s",ftag)] = new TH1D(Form("wmass_leptbos_%s",mtag),Form("wmass lb %s",mtag),wbins,wmin,wmax);
    }
    mHistos[Form("TMassHdrConv_%s",ftag)] = new TH1D(Form("tmass_hadrconv_%s",mtag),Form("tmass hconv %s",mtag),tbins,tmin,tmax);

    mHistos[Form("TMassHdr_%s",ftag)] = new TH1D(Form("tmass_hadr_%s",mtag),Form("tmass h %s",mtag),tbins,tmin,tmax);
    mHistos[Form("TMassLpt_%s",ftag)] = new TH1D(Form("tmass_lept_%s",mtag),Form("tmass l %s",mtag),tbins,tmin,tmax);
    mHistos[Form("WMassHdr_%s",ftag)] = new TH1D(Form("wmass_hadr_%s",mtag),Form("wmass h %s",mtag),wbins,wmin,wmax);
    mHistos[Form("WMassLpt_%s",ftag)] = new TH1D(Form("wmass_lept_%s",mtag),Form("wmass l %s",mtag),wbins,wmin,wmax);

    mHistos[Form("BPt_%s",ftag)] = new TH1D(Form("bpt_%s",mtag),Form("bpt %s",mtag),bbins,bmin,bmax);
    mHistos[Form("BjM_%s",ftag)] = new TH1D(Form("bjm_%s",mtag),Form("bjm %s",mtag),100,0,120);
    mHistos[Form("WjM_%s",ftag)] = new TH1D(Form("wjm_%s",mtag),Form("wjm %s",mtag),100,0,120);

    if (mDoNuSim) {
        mHistos[Form("TMassLptReco_%s",ftag)] = new TH1D(Form("tmass_leptreco_%s",mtag),Form("tmass lr %s",mtag),tbins,tmin,tmax);
    }

    if (mDoGammaJets) {
        mHistos[Form("TMassLptG_%s"  ,ftag)] = new TH1D(Form("tmass_leptg_%s"  ,mtag),Form("tmass lg %s"  ,mtag),tbins,tmin,tmax);
        mHistos[Form("WMassLptG_%s"  ,ftag)] = new TH1D(Form("wmass_leptg_%s"  ,mtag),Form("wmass lg %s"  ,mtag),wbins,wmin,wmax);
    }

    if (mDoNuJets) {
        mHistos[Form("TMassHdrNu_%s" ,ftag)] = new TH1D(Form("tmass_hadrnu_%s" ,mtag),Form("tmass hnu %s" ,mtag),tbins,tmin,tmax);
        mHistos[Form("TMassLptNu_%s" ,ftag)] = new TH1D(Form("tmass_leptnu_%s" ,mtag),Form("tmass lnu %s" ,mtag),tbins,tmin,tmax);
        if (mDoGammaJets) mHistos[Form("TMassLptNuG_%s",ftag)] = new TH1D(Form("tmass_leptnug_%s",mtag),Form("tmass lnug %s",mtag),tbins,tmin,tmax);
        mHistos[Form("WMassHdrNu_%s" ,ftag)] = new TH1D(Form("wmass_hadrnu_%s" ,mtag),Form("wmass hnu %s" ,mtag),wbins,wmin,wmax);
        mHistos[Form("BPtNu_%s"      ,ftag)] = new TH1D(Form("bptnu_%s"        ,mtag),Form("bptnu %s"     ,mtag),bbins,bmin,bmax);
    }

    mHistos[Form("wrCorr_%s",ftag)] = new TH1D(Form("wrcorr_%s",mtag),Form("wrc %s",mtag),600,0.1,2.5);
    mHistos[Form("brCorr_%s",ftag)] = new TH1D(Form("brcorr_%s",mtag),Form("brc %s",mtag),600,0.1,2.5);
    mHistos[Form("wqCorr_%s",ftag)] = new TH1D(Form("wqcorr_%s",mtag),Form("wqc %s",mtag),600,0.1,2.5);
    mHistos[Form("bhCorr_%s",ftag)] = new TH1D(Form("bhcorr_%s",mtag),Form("bhc %s",mtag),600,0.1,2.5);
    mHistos[Form("blCorr_%s",ftag)] = new TH1D(Form("blcorr_%s",mtag),Form("blc %s",mtag),600,0.1,2.5);
    mHistos[Form("btnCorr_%s",ftag)] = new TH1D(Form("btncorr_%s",mtag),Form("btnc %s",mtag),600,0.1,2.5);
    mHistos[Form("btCorr_%s",ftag)] = new TH1D(Form("btcorr_%s",mtag),Form("btc %s",mtag),600,0.1,2.5);
    mHistos[Form("bwCorr_%s",ftag)] = new TH1D(Form("bwcorr_%s",mtag),Form("bwc %s",mtag),600,0.1,2.5);
    mHistos[Form("bmCorr_%s",ftag)] = new TH1D(Form("bmcorr_%s",mtag),Form("bmc %s",mtag),600,0.1,2.5);
    mHistos[Form("bnCorr_%s",ftag)] = new TH1D(Form("bncorr_%s",mtag),Form("bnc %s",mtag),600,0.1,2.5);
    mHistos[Form("bmnCorr_%s",ftag)] = new TH1D(Form("bmncorr_%s",mtag),Form("bmnc %s",mtag),600,0.1,2.5);

    int binC0 = 300;
    double hiBin0 = 1.1;
    int binC = 600;
    double loBin = 0.10;
    double hiBin = 3.50;
    int binC2 = 250;
    double loBin2 = 0.7;
    double hiBin2 = 1.2;
    mHistos[Form("bNuCorr_%s",ftag)] = new TH1D(Form("bnucorr_%s",mtag),Form("bnu c %s",mtag),binC0,loBin,hiBin0);
    mHistos[Form("lNuCorr_%s",ftag)] = new TH1D(Form("lnucorr_%s",mtag),Form("lnu c %s",mtag),binC0,loBin,hiBin0);
    mHistos[Form("bMuCorr_%s",ftag)] = new TH1D(Form("bmucorr_%s",mtag),Form("bmu c %s",mtag),binC0,loBin,hiBin0);
    mHistos[Form("lMuCorr_%s",ftag)] = new TH1D(Form("lmucorr_%s",mtag),Form("lmu c %s",mtag),binC0,loBin,hiBin0);
    mHistos[Form("bMuNuCorr_%s",ftag)] = new TH1D(Form("bmunucorr_%s",mtag),Form("bmunu c %s",mtag),binC0,loBin,hiBin0);
    mHistos[Form("lMuNuCorr_%s",ftag)] = new TH1D(Form("lmunucorr_%s",mtag),Form("lmunu c %s",mtag),binC0,loBin,hiBin0);

    for (auto &era : mEras) {
        const char* etag = era.c_str();
        mProfs[Form("bCorrPt_%s_%s",etag,ftag)]    = new TProfile(Form("bcorrpt_%s_%s",  etag,mtag),Form("b cpt %s %s",  etag,mtag),50,20,220);
        mProfs[Form("gCorrPt_%s_%s",etag,ftag)]    = new TProfile(Form("gcorrpt_%s_%s",  etag,mtag),Form("g cpt %s %s",  etag,mtag),50,20,220);
        mProfs[Form("lCorrPt_%s_%s",etag,ftag)]    = new TProfile(Form("lcorrpt_%s_%s",  etag,mtag),Form("l cpt %s %s",  etag,mtag),50,20,220);
        mHistos[Form("lCorr_%s_%s",etag,ftag)]     = new TH1D(    Form("lcorr_%s_%s",    etag,mtag),Form("l c %s %s",    etag,mtag),binC2,loBin2,hiBin2);
        mHistos[Form("bCorr_%s_%s",etag,ftag)]     = new TH1D(    Form("bcorr_%s_%s",    etag,mtag),Form("b c %s %s",    etag,mtag),binC2,loBin2,hiBin2);
        mHistos[Form("gCorr_%s_%s",etag,ftag)]     = new TH1D(    Form("gcorr_%s_%s",    etag,mtag),Form("g c %s %s",    etag,mtag),binC2,loBin2,hiBin2);
        mHistos[Form("tfCorr_%s_%s",etag,ftag)]    = new TH1D(    Form("tfcorr_%s_%s",   etag,mtag),Form("tf c %s %s",   etag,mtag),binC,loBin,hiBin);
        mHistos[Form("tfbCorr_%s_%s",etag,ftag)]   = new TH1D(    Form("tfbcorr_%s_%s",  etag,mtag),Form("tf bc %s %s",  etag,mtag),binC,loBin,hiBin);
        mHistos[Form("tfbnuCorr_%s_%s",etag,ftag)] = new TH1D(    Form("tfbnucorr_%s_%s",etag,mtag),Form("tf bnuc %s %s",etag,mtag),binC,loBin,hiBin);
        mHistos[Form("tfKJES_%s_%s",etag,ftag)]    = new TH1D(    Form("tfkjes_%s_%s",   etag,mtag),Form("tf kjes %s %s",etag,mtag),binC,loBin,hiBin);

        if (mDoPartonLvl) {
            mHistos[Form("WMassHdrPtn_%s_%s",etag,ftag)] = new TH1D(Form("wmass_hadrptn_%s_%s",etag,mtag),Form("wmass hp %s %s",etag,mtag),wbins,wmin,wmax);
            mHistos[Form("TMassHdrPtn_%s_%s",etag,ftag)] = new TH1D(Form("tmass_hadrptn_%s_%s",etag,mtag),Form("tmass hp %s %s",etag,mtag),tbins,tmin,tmax);
        }

        for (auto &kjes : mKJESRange) {
            mHistos[Form("TMassHdrCorr_%s_%s_%.3f",etag,ftag,kjes)] = new TH1D(Form("tmass_hadr_corr_%s_%s_%.3f",mtag,etag,kjes),Form("tmass hc %s %s %.3f",mtag,etag,kjes),tbins,tmin,tmax);
            mHistos[Form("TMassLptCorr_%s_%s_%.3f",etag,ftag,kjes)] = new TH1D(Form("tmass_lept_corr_%s_%s_%.3f",mtag,etag,kjes),Form("tmass lc %s %s %.3f",mtag,etag,kjes),tbins,tmin,tmax);
            mHistos[Form("WMassHdrCorr_%s_%s_%.3f",etag,ftag,kjes)] = new TH1D(Form("wmass_hadr_corr_%s_%s_%.3f",mtag,etag,kjes),Form("wmass hc %s %s %.3f",mtag,etag,kjes),wbins,wmin,wmax);
            mHistos[Form("BPtCorr_%s_%s_%.3f"     ,etag,ftag,kjes)] = new TH1D(Form("bpt_corr_%s_%s_%.3f"       ,mtag,etag,kjes),Form("bpt c %s %s %.3f"   ,mtag,etag,kjes),bbins,bmin,bmax);

            if (mDoSemiCorr) {
                mHistos[Form("TMassHdrCorrW_%s_%s_%.3f",etag,ftag,kjes)] = new TH1D(Form("tmass_hadr_corrw_%s_%s_%.3f",mtag,etag,kjes),Form("tmass hcw %s %s %.3f",mtag,etag,kjes),tbins,tmin,tmax);
                mHistos[Form("TMassHdrCorrB_%s_%s_%.3f",etag,ftag,kjes)] = new TH1D(Form("tmass_hadr_corrb_%s_%s_%.3f",mtag,etag,kjes),Form("tmass hcb %s %s %.3f",mtag,etag,kjes),tbins,tmin,tmax);
            }

            if (mDoNuSim) {
                mHistos[Form("TMassLptRecoCorr_%s_%s_%.3f",etag,ftag,kjes)] = new TH1D(Form("tmass_leptreco_corr_%s_%s_%.3f",mtag,etag,kjes),Form("tmass lrc %s %s %.3f",mtag,etag,kjes),tbins,tmin,tmax);
            }

            if (mDoNuJets) {
                mHistos[Form("TMassHdrNuCorr_%s_%s_%.3f" ,etag,ftag,kjes)] = new TH1D(Form("tmass_hadrnu_corr_%s_%s_%.3f" ,mtag,etag,kjes),Form("tmass hnuc %s %s %.3f" ,mtag,etag,kjes),tbins,tmin,tmax);
                mHistos[Form("TMassHdrNuCorrW_%s_%s_%.3f",etag,ftag,kjes)] = new TH1D(Form("tmass_hadrnu_corrw_%s_%s_%.3f",mtag,etag,kjes),Form("tmass hnucw %s %s %.3f",mtag,etag,kjes),tbins,tmin,tmax);
                mHistos[Form("TMassHdrNuCorrB_%s_%s_%.3f",etag,ftag,kjes)] = new TH1D(Form("tmass_hadrnu_corrb_%s_%s_%.3f",mtag,etag,kjes),Form("tmass hnucb %s %s %.3f",mtag,etag,kjes),tbins,tmin,tmax);
                mHistos[Form("TMassLptNuCorr_%s_%s_%.3f" ,etag,ftag,kjes)] = new TH1D(Form("tmass_leptnu_corr_%s_%s_%.3f" ,mtag,etag,kjes),Form("tmass lnuc %s %s %.3f" ,mtag,etag,kjes),tbins,tmin,tmax);
                mHistos[Form("WMassHdrNuCorr_%s_%s_%.3f" ,etag,ftag,kjes)] = new TH1D(Form("wmass_hadrnu_corr_%s_%s_%.3f" ,mtag,etag,kjes),Form("wmass hnuc %s %s %.3f" ,mtag,etag,kjes),wbins,wmin,wmax);
                mHistos[Form("BPtNuCorr_%s_%s_%.3f"      ,etag,ftag,kjes)] = new TH1D(Form("bptnu_corr_%s_%s_%.3f"        ,mtag,etag,kjes),Form("bptnu c %s %s %.3f"    ,mtag,etag,kjes),bbins,bmin,bmax);
            }
        }
    }

    mHistos[Form("wFracture_%s"  ,ftag)] = new TH1D(Form("wfracture_%s"  ,mtag),Form("w fracture %s"  ,mtag),10,0,10);
    mHistos[Form("bFracture_%s"  ,ftag)] = new TH1D(Form("bfracture_%s"  ,mtag),Form("b fracture %s"  ,mtag),10,0,10);
    mHistos[Form("bbFracture_%s" ,ftag)] = new TH1D(Form("bbfracture_%s" ,mtag),Form("bb fracture %s" ,mtag),10,0,10);
    mHistos[Form("totFracture_%s",ftag)] = new TH1D(Form("totfracture_%s",mtag),Form("tot fracture %s",mtag),10,0,10);
}

TopJets::~TopJets() {
    if (!mChain) return;
    delete mChain->GetCurrentFile();
}

Int_t TopJets::GetEntry(Long64_t entry) {
    if (!mChain) return 0;
    return mChain->GetEntry(entry);
}

Long64_t TopJets::LoadTree(Long64_t entry) {
    if (!mChain) return -5;
    Long64_t centry = mChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (mChain->GetTreeNumber() != mCurrent) {
        mCurrent = mChain->GetTreeNumber();
    }
    return centry;
}

void TopJets::Init(TTree *tree) {
    mIsolation = 0;
    mPJet = 0;
    mPPtn = 0;
    mPOwn = 0;
    mPPDG = 0;
    mPTag = 0;
    mPPt = 0;
    mPEta = 0;
    mPPhi = 0;
    mPE = 0;
    mPDR = 0;
    mJPt = 0;
    mJEta = 0;
    mJPhi = 0;
    mJE = 0;
    mJCst = 0;
    mJPTD = 0;
    mJSm2 = 0;
    // Set branch addresses and branch pointers
    if (!tree) return;
    mChain = tree;
    mCurrent = -1;
    mChain->SetMakeClass(1);

    mChain->SetBranchAddress("info",             &mInfo);
    mChain->SetBranchAddress("bnucount",         &mBNuCount);
    mChain->SetBranchAddress("nub",              &mNuB);
    mChain->SetBranchAddress("nuc",              &mNuC);
    mChain->SetBranchAddress("nulept",           &mNuLept);
    mChain->SetBranchAddress("nuother",          &mNuOther);
    mChain->SetBranchAddress("weight",           &mWeight);
    mChain->SetBranchAddress("pthat",            &mPtHat);
    mChain->SetBranchAddress("isolation",        &mIsolation);
    mChain->SetBranchAddress("prtn_jet",         &mPJet);
    mChain->SetBranchAddress("prtn_ptn",         &mPPtn);
    mChain->SetBranchAddress("prtn_own",         &mPOwn);
    mChain->SetBranchAddress("prtn_pdgid",       &mPPDG);
    mChain->SetBranchAddress("prtn_tag",         &mPTag);
    mChain->SetBranchAddress("prtn_pt",          &mPPt);
    mChain->SetBranchAddress("prtn_eta",         &mPEta);
    mChain->SetBranchAddress("prtn_phi",         &mPPhi);
    mChain->SetBranchAddress("prtn_e",           &mPE);
    mChain->SetBranchAddress("prtn_dr",          &mPDR);
    mChain->SetBranchAddress("jet_pt",           &mJPt);
    mChain->SetBranchAddress("jet_eta",          &mJEta);
    mChain->SetBranchAddress("jet_phi",          &mJPhi);
    mChain->SetBranchAddress("jet_e",            &mJE);
    mChain->SetBranchAddress("jet_constituents", &mJCst);
    mChain->SetBranchAddress("jet_ptd",          &mJPTD);
    mChain->SetBranchAddress("jet_sigma2",       &mJSm2);
    mChain->SetBranchAddress("met",              &mMet);
}

bool TopJets::SubText(string s1, string s2) {
    return s1.find(s2) != std::string::npos;
}

template<class T>
inline void TopJets::Saver(T* h) {
    auto type = typeid(T).name();
    vector<double> errVals;
    double errOff = 0;
    if (SubText(string(h->GetName()),string("cuts"))) {
        h->SetMinimum(0.);
        int count = 1;
        auto it = mInsertions.begin();
        double baseline = (mFTag=="El" ? mErrorListEl[*it] : mErrorListMu[*it]);
        h->GetXaxis()->SetBinLabel(count,it->c_str());
        h->SetBinContent(count,baseline);
        errVals.push_back(baseline);
        errOff = 0.02*baseline;
        ++it;
        for (; it != mInsertions.end(); ++it) {
            if (mFTag=="El") {
                if (mErrorListEl.count(*it)==0) continue;
            } else {
                if (mErrorListMu.count(*it)==0) continue;
            }
            ++count;
            h->GetXaxis()->SetBinLabel(count,it->c_str());
            baseline -= (mFTag=="El" ? mErrorListEl[*it] : mErrorListMu[*it]);
            errVals.push_back(baseline);
            h->SetBinContent(count,baseline);
        }
    }
}

#endif // #ifdef TopJets_cxx
