#ifdef DEBUG
#undef DEBUG
#endif
//#define DEBUG

#ifndef TopPlots_h
#define TopPlots_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TMath.h>
#include <TGraph.h>
#include <TLine.h>
#include <TKey.h>
#include <TText.h>
#include <TLatex.h>

// Header file for the classes stored in the TTree if any.
#include <TH3.h>
#include <TH2.h>
#include <TF1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>

#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <iomanip>
#include <fstream>
#include <cstdlib>

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Math/IFunction.h"

#include "tdrstyle_mod15.C"

using std::endl;
using std::cout;
using std::vector;
using std::cerr;
using std::map;
using std::to_string;

struct EraGroup {
    double a;
    double b1;
    double b2;
    double b34;
};

class TopPlots {
public :
    TopPlots(string loc = ".", string type = "El", string fname="pjets_pythia8_amcnlo.root", double mtop = 172.5) {
        mMTop = mtop;
        mTMin = 120;
        mTMax = 200;
        mWMin = 50;
        mWMax = 100;
        mSkipFits = false;
        mSaveLoc = loc + string("/") + type + string("/pdf");
        system(Form("mkdir -p %s",mSaveLoc.c_str()));

        mFile = new TFile(Form("%s/%s",loc.c_str(),fname.c_str()),"READ");

        mKJESTypes = {"1.000","1.005", "1.010","1.015","1.020"};
        mMassTypes = {"fit","ave","itg","max","med","ch2","ndf","fer"};
        mHistTypes = {"tmass_hadr_corr_",
//                       "tmass_hadr_corrw_"  ,"tmass_hadr_corrb_"  ,
                      "tmass_lept_corr_",
                      "tmass_leptreco_corr_",
                      "wmass_hadr_corr_"};
        mSnglTypes = {"tmass_hadr_",
                      "tmass_lept_",
                      "tmass_leptreco_",
//                       "wmass_hadrbos_","wmass_hadrptn_",
                      "wmass_hadr_",
//                       "wmass_leptbos_","wmass_leptg_",
                      "wmass_lept_"};
//         mHistTypes = {"tmass_hadr_corr_"  ,"tmass_hadr_corrw_"  ,"tmass_hadr_corrb_"  ,
//                       "tmass_hadrnu_corr_","tmass_hadrnu_corrw_","tmass_hadrnu_corrb_",
//                       "tmass_lept_corr_" ,"tmass_leptnu_corr_",
//                       "wmass_hadr_corr_"  ,"wmass_hadrnu_corr_"};
//         mSnglTypes = {"tmass_hadr_","tmass_hadrnu_",
//                       "tmass_lept_","tmass_leptnu_","tmass_leptg_","tmass_leptnug_",
//                       "wmass_hadr_bos_","wmass_hadr_ptn_","wmass_hadr_","wmass_hadrnu_",
//                       "wmass_lept_bos_","wmass_leptg_","wmass_lept_"};
        for (auto &htype : mHistTypes){
            for (auto &mtype : mMassTypes) {
                for (auto &ktype : mKJESTypes) {
                    mMasses[htype][mtype][ktype] = EraGroup{0.0,0.0,0.0,0.0};
                }
            }
        }

        /* 1: Linear fit. */
        mF1  = new TF1("f1","[0]+[1]*x",0,0.45);
        /* 2: 1/linear fit. */
        mF2  = new TF1("f2","[0]/(1+[1]*x)",0,0.45);
        /* 3: Gaussian fit. */
        mF3  = new TF1("f3","[2]*TMath::Gaus(x,[0],[1])/TMath::Gaus([0],[0],[1])",mWMin,mWMax);
        /* 4: Sqrt of a 2nd degree polynomial fit. */
        mF4  = new TF1("f4","sqrt([0]*[0]*x*x+[1]*[1]*x+[2]*[2])",0,500);
        /* 5: Something mimicking a Breit-Wigner -fit. */
        mF5  = new TF1("f5","sqrt([0]*[0]/(x*x)+[1]*[1])",0,500);

        /* 6: Voigtian de-stabilised by an error function. */
        mF6  = new TF1("f6","[3]*TMath::Voigt(x-[0],[1],[2])*(1-[4]*TMath::Erf((x-[0])/[5]))/(TMath::Voigt(0,[1],[2]))",mWMin,mWMax);
        /* 7: Two-sided Gaussian. */
        mF7  = new TF1("f7","x>[0] ? [3]*TMath::Gaus(x,[0],[1])/TMath::Gaus([0],[0],[1]) : [3]*TMath::Gaus(x,[0],[2])/TMath::Gaus([0],[0],[2])",mWMin,mWMax);
        /* 8: Combination of an exponential and a delta function, convoluted with a Gaussian resolution. */
        mF8  = new TF1("f8","[4]*((1-[0])*TMath::Gaus(x,[1],[2]) + [0]*([3]/2)*TMath::Exp([3]*(x-[1]+[3]*[2]*[2]/2))*(1-TMath::Erf((x-[1]+[3]*[2]*[2])/(1.4142136*[2]))))/(1+[0]*([3]/2-1))",mTMin,mTMax);
        /* 9: Voigtian de-stabilised by an error function and an exponential tail. */
        mF9  = new TF1("f9","[3]*TMath::Voigt(x-[0],[1],[2])*(1-[4]*TMath::Erf((x-[0])/[5])*TMath::Exp([6]*min(x-[0],0.0)/[5]))/(TMath::Voigt(0,[1],[2])*(1+[4])*TMath::Exp([6]/[5]))",mTMin,mTMax);
        /* 10: Built-in crystal ball. */
        mF10 = new TF1("f10","[4]*ROOT::Math::crystalball_function(x,[0],[1],[2],[3])",mWMin,mWMax);
        /* 11: The generic Voigtian Crystal Ball. */
        mF11 = new TF1("f11","(x-[0]>-[4]) ? [3]*TMath::Voigt(x-[0],[1],[2])/TMath::Voigt(0,[1],[2]) : pow([6]-x+[0]-[4],-[5])*pow([6],[5])*[3]*TMath::Voigt(-[4],[1],[2])/TMath::Voigt(0,[1],[2])",mWMin,mWMax);
        /* 12: The generic Voigtian Crystal Ball, destabilized by an error function. */
        mF12 = new TF1("f12","(x-[0]>-[4]) ? [3]*TMath::Voigt(x-[0],[1],[2])*(1-[7]*TMath::Erf((x-[0])/[8]))/TMath::Voigt(0,[1],[2]) : pow([6]-x+[0]-[4],-[5])*pow([6],[5])*[3]*TMath::Voigt(-[4],[1],[2])*(1-[7]*TMath::Erf(-[4]/[8]))/TMath::Voigt(0,[1],[2])",mWMin,mWMax);
        /* 13: A two-sided Gaussian Crystal Ball. */
        mF13 = new TF1("f13","(x>[0]-[1]*[3]) ? ((x<[0]+[2]*[3]) ? TMath::Gaus(x,[0],[3])*[4]/TMath::Gaus([0],[0],[3]) : pow([6]/[2]-[2]+(x-[0])/[3],-[6])*pow([6]/[2],[6])*TMath::Gaus([0]+[2]*[3],[0],[3])*[4]/TMath::Gaus([0],[0],[3])) : pow([5]/[1]-[1]-(x-[0])/[3],-[5])*pow([5]/[1],[5])*TMath::Gaus([0]-[1]*[3],[0],[3])*[4]/TMath::Gaus([0],[0],[3])",mWMin,mWMax);
        /* 14: A two-sided Voigtian Crystal Ball. */
        mF14 = new TF1("f14","(x-[0]>-[4]) ? ((x-[0]<[7]) ? [3]*TMath::Voigt(x-[0],[1],[2])/TMath::Voigt(0,[1],[2]) :  pow([9]+x-[0]-[7],-[8])*pow([9],[8])*[3]*TMath::Voigt([7],[1],[2])/TMath::Voigt(0,[1],[2])) : pow([6]-x+[0]-[4],-[5])*pow([6],[5])*[3]*TMath::Voigt(-[4],[1],[2])/TMath::Voigt(0,[1],[2])",mWMin,mWMax);
        /* 15: A two-sied Voigtian Crystal Ball with an exponential extension on the left. */
        mF15 = new TF1("f15","(x-[0]>-[4]) ? ((x-[0]<[7]) ? [3]*TMath::Voigt(x-[0],[1],[2])/TMath::Voigt(0,[1],[2]) :  pow([9]+x-[0]-[7],-[8])*pow([9],[8])*[3]*TMath::Voigt([7],[1],[2])/TMath::Voigt(0,[1],[2])) : ((x-[0]>-[4]-[10]) ? pow([6]-x+[0]-[4],-[5])*pow([6],[5])*[3]*TMath::Voigt(-[4],[1],[2])/TMath::Voigt(0,[1],[2]) : TMath::Exp([11]*(x-[0]+[4]+[10]))*pow([6]+[10],-[5])*pow([6],[5])*[3]*TMath::Voigt(-[4],[1],[2])/TMath::Voigt(0,[1],[2]))",mWMin,mWMax);
        /* 16: A two-sied Voigtian Crystal Ball with a power law extension on the left. */
        mF16 = new TF1("f16","(x-[0]>-[4]) ? ((x-[0]<[7]) ? [3]*TMath::Voigt(x-[0],[1],[2])/TMath::Voigt(0,[1],[2]) : pow([9]+x-[0]-[7],-[8])*pow([9],[8])*[3]*TMath::Voigt([7],[1],[2])/TMath::Voigt(0,[1],[2])) : ((x-[0]>-[4]-[10]) ? pow([6]-x+[0]-[4],-[5])*pow([6],[5])*[3]*TMath::Voigt(-[4],[1],[2])/TMath::Voigt(0,[1],[2]) : pow([12]-x+[0]-[4]-[10],-[11])*pow([12],[11])*pow([6]+[10],-[5])*pow([6],[5])*[3]*TMath::Voigt(-[4],[1],[2])/TMath::Voigt(0,[1],[2]))",mWMin,mWMax);
        /* 16: A two-sied Voigtian Crystal Ball with a Gaussian extension on the left. */
        mF17 = new TF1("f17","(x-[0]>-[4]) ? ((x-[0]<[7]) ? [3]*TMath::Voigt(x-[0],[1],[2])/TMath::Voigt(0,[1],[2]) : pow([9]+x-[0]-[7],-[8])*pow([9],[8])*[3]*TMath::Voigt([7],[1],[2])/TMath::Voigt(0,[1],[2])) : ((x-[0]>-[4]-[10]) ? pow([6]-x+[0]-[4],-[5])*pow([6],[5])*[3]*TMath::Voigt(-[4],[1],[2])/TMath::Voigt(0,[1],[2]) : (TMath::Gaus(x,[10]+[11],[12])/TMath::Gaus([10],[10]+[11],[12]))*pow([6]+[10],-[5])*pow([6],[5])*[3]*TMath::Voigt(-[4],[1],[2])/TMath::Voigt(0,[1],[2]))",mWMin,mWMax);
    }
    ~TopPlots() {}

    bool             SubText(string s1, string s2) { return s1.find(s2) != std::string::npos; }
    template<class T>
    void             Drawer(T *h, bool stats = false, bool logY = true);
    void             Process() {
        if (!mFile or mFile->IsZombie()) return;

        // Automatically go through the list of keys (directories)
        TList *keys = gDirectory->GetListOfKeys();
        TListIter itkey(keys);
        TObject *obj;
        TKey *key;

        while ( (key = dynamic_cast<TKey*>(itkey.Next())) ) {
            obj = key->ReadObj(); assert(obj);
            string name = obj->GetName();
            if (SubText(name,"_bos_") or SubText(name,"_ptn_")) continue;
            if (SubText(name,"_corr") or SnglIdx(name)==-1) {
                if (HistIdx(name)==-1) continue;
            }
            cout << obj->GetName() << endl;

            if (obj->InheritsFrom("TProfile")) {
                TProfile *handle = dynamic_cast<TProfile*>(obj);
                Drawer(handle);
            } else if (obj->InheritsFrom("TH3")) {
                TH3D *handle = dynamic_cast<TH3D*>(obj);
                Drawer(handle);
            } else if (obj->InheritsFrom("TH2")) {
                TH2D *handle = dynamic_cast<TH2D*>(obj);
                Drawer(handle);
            } else if (obj->InheritsFrom("TH1")) {
                TH1D *handle = dynamic_cast<TH1D*>(obj);
                Drawer(handle);
            }
        }
    }

    int HistIdx(string name) {
        unsigned idx = 0;
        for (auto &hname : mHistTypes) {
            if (SubText(name,hname)) break;
            ++idx;
        }
        if (idx >= mHistTypes.size()) return -1;
        return idx;
    }

    int SnglIdx(string name) {
        unsigned idx = 0;
        for (auto &hname : mSnglTypes) {
            if (SubText(name,hname)) break;
            ++idx;
        }
        if (idx >= mSnglTypes.size()) return -1;
        return idx;
    }

    int KJESIdx(string name) {
        unsigned idx = 0;
        for (auto &kname : mKJESTypes) {
            if (SubText(name,kname)) break;
            ++idx;
        }
        if (idx >= mKJESTypes.size()) return -1;
        return idx;
    }

    template<class T>
    double ItgMass(T *h,double lolim, double uplim) {
        int lobin = h->FindBin(lolim);
        int hibin = h->FindBin(uplim);

        double cumulator = 0;
        double masscumulator = 0;
        for (int bin = lobin; bin <= hibin; ++bin) {
            double bcont = h->GetBinContent(bin);
            double bcent = h->GetBinCenter(bin);

            cumulator += bcont;
            masscumulator += bcont*bcent;
        }

        return (cumulator>0) ? masscumulator/cumulator : -1;
    }

    int NumKJES()   { return mKJESTypes.size(); }
    int NumMasses() { return mMassTypes.size(); }
    int NumHistos() { return mHistTypes.size(); }
    int NumSngls()  { return mSnglTypes.size(); }
    double MassVal() { return mMTop; }
    string KJESName(int kidx) { return mKJESTypes[kidx]; }
    string HistName(int hidx) { return mHistTypes[hidx]; }
    string MassName(int midx) { return mMassTypes[midx]; }
    string SnglName(int sidx) { return mSnglTypes[sidx]; }
    void MassInfo(int hidx, int midx, int kidx, std::ofstream &s) {
        auto &hname = mHistTypes[hidx];
        auto &mname = mMassTypes[midx];
        auto &kname = mKJESTypes[kidx];
        s << std::setprecision(8) << std::fixed
                                  << mMasses[hname][mname][kname].a << " "
                                  << mMasses[hname][mname][kname].b1 << " "
                                  << mMasses[hname][mname][kname].b2 << " "
                                  << mMasses[hname][mname][kname].b34 << " ";
    }
    void SnglInfo(int sidx, int midx, std::ofstream &s) {
        auto &sname = mSnglTypes[sidx];
        auto &mname = mMassTypes[midx];
        s << std::setprecision(8) << std::fixed << mSnglMasses[sname][mname] << " ";
    }
private:
    string                                  mSaveLoc;
    vector<string>                          mEras;
    double                                  mMTop;
    double                                  mWMin;
    double                                  mWMax;
    double                                  mTMin;
    double                                  mTMax;

    TF1*                                    mF1;
    TF1*                                    mF2;
    TF1*                                    mF3;
    TF1*                                    mF4;
    TF1*                                    mF5;
    TF1*                                    mF6;
    TF1*                                    mF7;
    TF1*                                    mF8;
    TF1*                                    mF9;
    TF1*                                    mF10;
    TF1*                                    mF11;
    TF1*                                    mF12;
    TF1*                                    mF13;
    TF1*                                    mF14;
    TF1*                                    mF15;
    TF1*                                    mF16;
    TF1*                                    mF17;
    TF1*                                    mF18;
    TF1*                                    mF19;
    TF1*                                    mF20;

    TFile*                                  mFile;
    bool                                    mSkipFits;

    vector<string>                               mKJESTypes;
    vector<string>                               mMassTypes;
    vector<string>                               mHistTypes;
    vector<string>                               mSnglTypes;
    map<string,map<string,map<string,EraGroup>>> mMasses;
    map<string,map<string,double>>               mSnglMasses;
};

#endif

/* Example fits! */

//// f3 (t) :
// mF3  = new TF1("f3","[2]*TMath::Gaus(x,[0],[1])",mWMin,mWMax);
//     mF3->SetRange(mTMin,mTMax);
//     mF3->SetParameters(maxM-0.5,2,maxEv);
//     mF3->SetParLimits(0,170,180);
//     mF3->SetParLimits(1,0.2,2);
//     mF3->SetParLimits(2,0.997*maxEv,1.003*maxEv);
//     h->Fit("f3",fitopt);
//     mF3->Draw("CSAME");
//     if (clog) {
//         clog->cd();
//         mF3->Draw("CSAME");
//         c->cd();
//     }
//     tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %d",mF3->GetParameter(0),mF3->GetParameter(1),mF3->GetParameter(2),mF3->GetChisquare(),mF3->GetNDF()));


//// f6 (W) :
// mF6->SetRange(mWMin,mWMax);
// mF6->SetParameters(80.4,1,2.05,maxEv,0.01,1);
// mF6->SetParLimits(0,70,90);
// mF6->SetParLimits(1,0.001,10);
// mF6->SetParLimits(2,2.0,5);
// mF6->SetParLimits(3,0.95*maxEv,1.05*maxEv);
// mF6->SetParLimits(4,0,0.5);
// mF6->SetParLimits(5,0.01,10);
// h->Fit("f6",fitopt);
// mF6->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF6->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF6->GetParameter(0),mF6->GetParameter(1),mF6->GetParameter(2),mF6->GetParameter(3),mF6->GetParameter(4),mF6->GetParameter(5)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %d",mF6->GetChisquare(),mF6->GetNDF()));

//// f6 (t) :
// mF6  = new TF1("f6","[3]*TMath::Voigt(x-[0],[1],[2])*(1-[4]*TMath::Erf((x-[0])/[5]))/(TMath::Voigt(0,[1],[2]))",wmin,wmax);
// mF6->SetRange(120,220);
// mF6->SetParameters(168,1,1,maxEv,0.1,1);
// mF6->SetParLimits(0,150,180);
// mF6->SetParLimits(1,0.5,40);
// mF6->SetParLimits(2,0.5,40);
// mF6->SetParLimits(3,0.9*maxEv,1.3*maxEv);
// mF6->SetParLimits(4,0.05,1);
// mF6->SetParLimits(5,0.01,10);
// h->Fit("f6","QRNM");
// mF6->Draw("SAME");
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF6->GetParameter(0),mF6->GetParameter(1),mF6->GetParameter(2),mF6->GetParameter(3)/maxEv,mF6->GetParameter(4),mF6->GetParameter(5)));

//// f7 (W) :
// mF7->SetParameters(80.4,4,4,maxEv);
// mF7->SetParLimits(0,70,90);
// mF7->SetParLimits(1,1,15);
// mF7->SetParLimits(2,1,15);
// mF7->SetParLimits(3,0.99*maxEv,1.01*maxEv);
// h->Fit("f7","QRN");
// mF7->Draw("SAME");
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f",mF7->GetParameter(0),mF7->GetParameter(1),mF7->GetParameter(2),mF7->GetParameter(3)));

//// f7 (t) :
// mF7  = new TF1("f7","x>[0] ? [3]*TMath::Gaus(x,[0],[1]) : [3]*TMath::Gaus(x,[0],[2])",wmin,wmax);
// mF7->SetParameters(80.4,4,4,maxEv);
// mF7->SetParLimits(0,70,90);
// mF7->SetParLimits(1,1,15);
// mF7->SetParLimits(2,1,15);
// mF7->SetParLimits(3,0.99*maxEv,1.01*maxEv);
// h->Fit("f7","QRN");
// mF7->Draw("SAME");
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f",mF7->GetParameter(0),mF7->GetParameter(1),mF7->GetParameter(2),mF7->GetParameter(3)));

//// f8 (t) :
// mF8  = new TF1("f8","[4]*((1-[0])*TMath::Gaus(x,[1],[2]) + [0]*([3]/2)*TMath::Exp([3]*(x-[1]+[3]*[2]*[2]/2))*(1-TMath::Erf((x-[1]+[3]*[2]*[2])/(1.4142136*[2]))))/(1+[0]*([3]/2-1))",tmin,tmax);
// mF8->SetParameters(0.1,168,1,1,maxEv);
// mF8->SetParLimits(0,0.01,1);
// mF8->SetParLimits(1,150,190);
// mF8->SetParLimits(2,1,20);
// mF8->SetParLimits(3,1,100);
// mF8->SetParLimits(4,0.95*maxEv,1.05*maxEv);
// h->Fit("f8","QRNM");
// mF8->Draw("SAME");
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f",mF8->GetParameter(0),mF8->GetParameter(1),mF8->GetParameter(2),mF8->GetParameter(3),mF8->GetParameter(4)));

//// f9 (t) :
// mF9  = new TF1("f9","[3]*TMath::Voigt(x-[0],[1],[2])*(1-[4]*TMath::Erf((x-[0])/[5])*TMath::Exp([6]*min(x-[0],0.0)/[5]))/(TMath::Voigt(0,[1],[2])*(1+[4])*TMath::Exp([6]/[5]))",tmin,tmax);
// mF9->SetParameters(168,1,1,maxEv,0.2,1,1);
// mF9->SetParLimits(0,150,180);
// mF9->SetParLimits(1,0.5,40);
// mF9->SetParLimits(2,0.5,40);
// mF9->SetParLimits(3,0.9*maxEv,1.3*maxEv);
// mF9->SetParLimits(4,0.1,1);
// mF9->SetParLimits(5,0.1,30);
// mF9->SetParLimits(6,0.5,2);
// h->Fit("f9","QRNM");
// mF9->Draw("SAME");
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f %.3f",mF9->GetParameter(0),mF9->GetParameter(1),mF9->GetParameter(2),mF9->GetParameter(3),mF9->GetParameter(4),mF9->GetParameter(5),mF9->GetParameter(6)));

//// f10 (t):
// mF10 = new TF1("f10","[4]*ROOT::Math::crystalball_function(x,[0],[1],[2],[3])",wmin,wmax);
// mF10->SetRange(100,220);
// mF10->SetParameters(1,0.1,1,168,maxEv);
// mF10->SetParLimits(0,0.1,100);
// mF10->SetParLimits(1,0,40);
// mF10->SetParLimits(2,0.5,40);
// mF10->SetParLimits(3,167,180);
// mF10->SetParLimits(4,0.99*maxEv,1.01*maxEv);
// h->Fit("f10","QRNM");
// mF10->Draw("SAME");
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f",mF10->GetParameter(0),mF10->GetParameter(1),mF10->GetParameter(2),mF10->GetParameter(3),mF10->GetParameter(4)));


//// f11 (W):
// mF11->SetRange(mWMin,mWMax);
// mF11->SetParameters(80.4,3,7,maxEv,5,10,80);
// mF11->SetParLimits(0,70,90);
// mF11->SetParLimits(1,0.2,10);
// mF11->SetParLimits(2,2,10);
// mF11->SetParLimits(3,0.997*maxEv,1.003*maxEv);
// mF11->SetParLimits(4,1,50);
// mF11->SetParLimits(5,0.1,100);
// mF11->SetParLimits(6,0.1,1000);
// h->Fit("f11",fitopt);
// mF11->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF11->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f %.3f",mF11->GetParameter(0),mF11->GetParameter(1),mF11->GetParameter(2),mF11->GetParameter(3),mF11->GetParameter(4),mF11->GetParameter(5),mF11->GetParameter(6)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %d",mF11->GetChisquare(),mF11->GetNDF()));

//// f11 (thadrnu) :
// mF11 = new TF1("f11","(x-[0]>-[4]) ? [3]*TMath::Voigt(x-[0],[1],[2])/TMath::Voigt(0,[1],[2]) : pow([6]+x-[0]+[4],-[5])*pow([6],[5])*[3]*TMath::Voigt(-[4],[1],[2])/TMath::Voigt(0,[1],[2])",wmin,wmax);
// mF11->SetRange(mTMin,mTMax);
// mF11->SetParameters(maxM,3,7,maxEv,5,10,80);
// mF11->SetParLimits(0,166,180);
// mF11->SetParLimits(1,1,10);
// mF11->SetParLimits(2,1,10);
// mF11->SetParLimits(3,0.997*maxEv,1.003*maxEv);
// mF11->SetParLimits(4,1,50);
// mF11->SetParLimits(5,0.1,100);
// mF11->SetParLimits(6,0.1,1000);
// h->Fit("f11",fitopt);
// mF11->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF11->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF11->GetParameter(0),mF11->GetParameter(1),mF11->GetParameter(2),mF11->GetParameter(3)/maxEv,mF11->GetParameter(4),mF11->GetParameter(5)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %.3f %d",mF11->GetParameter(6),mF11->GetChisquare(),mF11->GetNDF()));

//// f11 (thadr)
// mF11->SetRange(mTMin,mTMax);
// mF11->SetParameters(maxM,2,2,maxEv,5,10,80);
// mF11->SetParLimits(0,166,180);
// mF11->SetParLimits(1,2,10);
// mF11->SetParLimits(2,2,10);
// mF11->SetParLimits(3,0.997*maxEv,1.003*maxEv);
// mF11->SetParLimits(4,1,50);
// mF11->SetParLimits(5,0.1,100);
// mF11->SetParLimits(6,0.1,1000);
// h->Fit("f11",fitopt);
// mF11->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF11->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF11->GetParameter(0),mF11->GetParameter(1),mF11->GetParameter(2),mF11->GetParameter(3)/maxEv,mF11->GetParameter(4),mF11->GetParameter(5)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %.3f %d",mF11->GetParameter(6),mF11->GetChisquare(),mF11->GetNDF()));

//// f12 (W):
// mF12 = new TF1("f12","(x-[0]>-[4]) ? [3]*TMath::Voigt(x-[0],[1],[2])*(1-[7]*TMath::Erf((x-[0])/[8]))/TMath::Voigt(0,[1],[2]) : pow([6]-x+[0]-[4],-[5])*pow([6],[5])*[3]*TMath::Voigt(-[4],[1],[2])*(1-[7]*TMath::Erf(-[4]/[8]))/TMath::Voigt(0,[1],[2])",wmin,wmax);
// mF12->SetRange(mTMin,mTMax);
// mF12->SetParameters(maxM,0.6,17.6,0.91*maxEv,5,10,80,0.7,10);
// mF12->SetParLimits(0,160,180);
// mF12->SetParLimits(1,0.1,20);
// mF12->SetParLimits(2,2,40);
// mF12->SetParLimits(3,0.9*maxEv,1.05*maxEv);
// mF12->SetParLimits(4,1,50);
// mF12->SetParLimits(5,0.1,100);
// mF12->SetParLimits(6,0.1,1000);
// mF12->SetParLimits(7,0.05,1);
// mF12->SetParLimits(8,0.01,40);
// h->Fit("f11","LQRN");
// mF12->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF12->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF12->GetParameter(0),mF12->GetParameter(1),mF12->GetParameter(2),mF12->GetParameter(3)/maxEv,mF12->GetParameter(4),mF12->GetParameter(5)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %.3f %.3f %.3f %d",mF12->GetParameter(6),mF12->GetParameter(7),mF12->GetParameter(8),mF12->GetChisquare(),mF12->GetNDF()));

//// f12 (t):
// mF12->SetRange(mTMin,mTMax);
// mF12->SetParameters(maxM,0.6,7,0.97*maxEv,5,10,80,0.7,10);
// mF12->SetParLimits(0,160,180);
// mF12->SetParLimits(1,0.1,20);
// mF12->SetParLimits(2,2,40);
// mF12->SetParLimits(3,0.9*maxEv,1.05*maxEv);
// mF12->SetParLimits(4,1,50);
// mF12->SetParLimits(5,0.1,100);
// mF12->SetParLimits(6,0.1,1000);
// mF12->SetParLimits(7,0.05,1);
// mF12->SetParLimits(8,0.01,40);
// h->Fit("f11",fitopt);
// mF12->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF12->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF12->GetParameter(0),mF12->GetParameter(1),mF12->GetParameter(2),mF12->GetParameter(3)/maxEv,mF12->GetParameter(4),mF12->GetParameter(5)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %.3f %.3f %.3f %d",mF12->GetParameter(6),mF12->GetParameter(7),mF12->GetParameter(8),mF12->GetChisquare(),mF12->GetNDF()));

//// f13 (thadr):
// mF13 = new TF1("f13","((x-[0])/[3]>-[1]) ? (((x-[0])/[3]<[2]) ? TMath::Gaus(x,[0],[3])*[4]/TMath::Gaus(0,[0],[3]) : TMath::Pow([6]/[2]-[2]-(x-[0])/[3],-[6])*TMath::Pow([6]/[2],[6])*TMath::Exp(-[2]*[2]/2)*[4]/TMath::Gaus(0,[0],[3])) : TMath::Pow([6]/[1]-[1]+(x-[0])/[3],-[5])*TMath::Pow([5]/[1],[5])*TMath::Exp(-[1]*[1]/2)*[4]/TMath::Gaus(0,[0],[3])",mWMin,mWMax);
// mF13->SetRange(mTMin,mTMax);
// mF13->SetParameters(maxM,2,2,2,maxEv,1,1);
// mF13->SetParLimits(0,166,180);
// mF13->SetParLimits(1,0.2,20);
// mF13->SetParLimits(2,0.2,20);
// mF13->SetParLimits(3,0.5,20);
// mF13->SetParLimits(4,0.997*maxEv,1.003*maxEv);
// mF13->SetParLimits(5,0.2,20);
// mF13->SetParLimits(6,0.2,20);
// h->Fit("f13","ILQRNM");
// mF13->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF13->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF13->GetParameter(0),mF13->GetParameter(1),mF13->GetParameter(2),mF13->GetParameter(3)/maxEv,mF13->GetParameter(4),mF13->GetParameter(5)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %.3f %d",mF13->GetParameter(6),mF13->GetChisquare(),mF13->GetNDF()));

//// f13 (tlept):
// mF13->SetRange(mTMin,mTMax);
// mF13->SetParameters(maxM,2,2,2,maxEv,1,1);
// mF13->SetParLimits(0,166,180);
// mF13->SetParLimits(1,0.2,20);
// mF13->SetParLimits(2,0.2,20);
// mF13->SetParLimits(3,0.5,20);
// mF13->SetParLimits(4,0.997*maxEv,1.003*maxEv);
// mF13->SetParLimits(5,0.2,20);
// mF13->SetParLimits(6,0.2,20);
// h->Fit("f13","ILQRNM");
// mF13->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF13->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF13->GetParameter(0),mF13->GetParameter(1),mF13->GetParameter(2),mF13->GetParameter(3)/maxEv,mF13->GetParameter(4),mF13->GetParameter(5)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %.3f %d",mF13->GetParameter(6),mF13->GetChisquare(),mF13->GetNDF()));

//// f14 (thadr):
// mF13->SetRange(mTMin,mTMax);
// mF13->SetParameters(maxM,2,2,2,maxEv,1,1);
// mF13->SetParLimits(0,166,180);
// mF13->SetParLimits(1,0.2,20);
// mF13->SetParLimits(2,0.2,20);
// mF13->SetParLimits(3,0.5,20);
// mF13->SetParLimits(4,0.997*maxEv,1.003*maxEv);
// mF13->SetParLimits(5,0.2,20);
// mF13->SetParLimits(6,0.2,20);
// h->Fit("f13","ILQRNM");
// mF13->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF13->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF13->GetParameter(0),mF13->GetParameter(1),mF13->GetParameter(2),mF13->GetParameter(3)/maxEv,mF13->GetParameter(4),mF13->GetParameter(5)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %.3f %d",mF13->GetParameter(6),mF13->GetChisquare(),mF13->GetNDF()));
// cout << mF13->GetProb() << endl;

//// f14 (tlept):
// mF14->SetRange(mTMin,mTMax);
// mF14->SetParameters(maxM-0.5,2,2.19,maxEv,3.797,3.566,17.582,3.8,3.6,17.6);
// mF14->SetParLimits(0,160,180);
// mF14->SetParLimits(1,0.2,10);
// mF14->SetParLimits(2,0.2,10);
// mF14->SetParLimits(3,0.997*maxEv,1.003*maxEv);
// mF14->SetParLimits(4,1,50);
// mF14->SetParLimits(5,0.1,100);
// mF14->SetParLimits(6,0.1,1000);
// mF14->SetParLimits(7,1,50);
// mF14->SetParLimits(8,0.1,100);
// mF14->SetParLimits(9,0.1,1000);
// h->Fit("f14",fitopt);
// mF14->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF14->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF14->GetParameter(0),mF14->GetParameter(1),mF14->GetParameter(2),mF14->GetParameter(3)/maxEv,mF14->GetParameter(4),mF14->GetParameter(5)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %.3f %.3f %.3f %.3f %d",mF14->GetParameter(6),mF14->GetParameter(7),mF14->GetParameter(8),mF14->GetParameter(9),mF14->GetChisquare(),mF14->GetNDF()));

//// f16:
// mF16->SetRange(mTMin,mTMax);
// mF16->SetParameters(maxM-0.5,2,2.19,maxEv,3.797,3.566,17.582,3.8,3.6,17.6,30);
// mF16->SetParameter(11,4.2);
// mF16->SetParameter(12,28);
// mF16->SetParLimits(0,160,180);
// mF16->SetParLimits(1,0.2,10);
// mF16->SetParLimits(2,0.2,10);
// mF16->SetParLimits(3,0.997*maxEv,1.003*maxEv);
// mF16->SetParLimits(4,1,50);
// mF16->SetParLimits(5,0.1,100);
// mF16->SetParLimits(6,0.1,1000);
// mF16->SetParLimits(7,1,50);
// mF16->SetParLimits(8,0.1,100);
// mF16->SetParLimits(9,0.1,1000);
// mF16->SetParLimits(10,20,50);
// mF16->SetParLimits(11,0.1,100);
// mF16->SetParLimits(12,0.1,1000);
// h->Fit("f16",fitopt);
// mF16->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF16->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF16->GetParameter(0),mF16->GetParameter(1),mF16->GetParameter(2),mF16->GetParameter(3)/maxEv,mF16->GetParameter(4),mF16->GetParameter(5)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF16->GetParameter(6),mF16->GetParameter(7),mF16->GetParameter(8),mF16->GetParameter(9),mF16->GetParameter(10),mF16->GetParameter(11)));
// tt.DrawLatex(maxM,maxEv*0.55,Form("%.3F %.3f %d",mF16->GetParameter(12),mF16->GetChisquare(),mF16->GetNDF()));


//// f17:
// mF17->SetRange(mTMin,mTMax);
// mF17->SetParameters(maxM-0.5,2,2.19,maxEv,3.797,3.566,17.582,3.8,3.6,17.6,30);
// mF17->SetParameter(11,1);
// mF17->SetParameter(12,2);
// mF17->SetParLimits(0,160,180);
// mF17->SetParLimits(1,0.2,10);
// mF17->SetParLimits(2,0.2,10);
// mF17->SetParLimits(3,0.997*maxEv,1.003*maxEv);
// mF17->SetParLimits(4,1,50);
// mF17->SetParLimits(5,0.1,100);
// mF17->SetParLimits(6,0.1,1000);
// mF17->SetParLimits(7,1,50);
// mF17->SetParLimits(8,0.1,100);
// mF17->SetParLimits(9,0.1,1000);
// mF17->SetParLimits(10,20,50);
// mF17->SetParLimits(11,0.1,10);
// mF17->SetParLimits(12,0.1,100);
// h->Fit("f17",fitopt);
// mF17->Draw("CSAME");
// if (clog) {
//     clog->cd();
//     mF17->Draw("CSAME");
//     c->cd();
// }
// tt.DrawLatex(maxM,maxEv*0.75,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF17->GetParameter(0),mF17->GetParameter(1),mF17->GetParameter(2),mF17->GetParameter(3)/maxEv,mF17->GetParameter(4),mF17->GetParameter(5)));
// tt.DrawLatex(maxM,maxEv*0.65,Form("%.3f %.3f %.3f %.3f %.3f %.3f",mF17->GetParameter(6),mF17->GetParameter(7),mF17->GetParameter(8),mF17->GetParameter(9),mF17->GetParameter(10),mF17->GetParameter(11)));
// tt.DrawLatex(maxM,maxEv*0.55,Form("%.3F %.3f %d",mF17->GetParameter(12),mF17->GetChisquare(),mF17->GetNDF()));
