#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TMath.h>
#include <TGraph.h>
#include <TLine.h>
#include <TText.h>
#include <TLatex.h>

// Header file for the classes stored in the TTree if any.
#include <TH3.h>
#include <TH2.h>
#include <TF1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>

#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <iomanip>
#include <fstream>

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Math/IFunction.h"

#include "tdrstyle_mod15.C"

using std::endl;
using std::cout;
using std::string;
using std::pair;
using std::vector;
using std::cerr;
using std::map;
using std::to_string;

#include <TMatrixD.h>
#include <TDecompSVD.h>

bool SubText(string s1, string s2) { return s1.find(s2) != std::string::npos; }

struct MTriplet {
    double v1725;
    double v1737;
    double v1750;
};

struct MEras {
    MTriplet a;
    MTriplet b1;
    MTriplet b2;
    MTriplet b34;
};

vector<double> TripletToV(MTriplet &m) { return vector<double>{m.v1725,m.v1737,m.v1750}; }

// fit = a + b*gen
template <int DIM, int DEG=1>
class Fitter1D {
public:
    Fitter1D(vector<double> expv = vector<double>(DIM*DEG,1), vector<double> genv = vector<double>(DIM,1), vector<double> experr = vector<double>(DIM*DEG,1)) :
        mInverted(2,DIM*DEG),mCoeffs(2,1),mExp(DIM*DEG,1),mErrs(DIM*DEG,1),mVals(DIM*DEG,2),mErrInverter(2,2) {
        mInitialized = false;
        if (genv.size()!=DIM or expv.size()!=DIM*DEG or experr.size()!=DIM*DEG) return;

        for (unsigned idx = 0; idx < DIM; ++idx) {
            for (unsigned deg = 0; deg < DEG; ++deg) {
                int totidx = DEG*idx+deg;
                mExp (totidx,0)  = expv [totidx];
                mErrs(totidx,0) = experr[totidx];
                mVals(totidx,0) = 1;
                mVals(totidx,1) = genv[idx];
            }
        }

        mChi2 = 0.0;
        //mTVal = 1.0; // N = Infinity
        mTVal = 1.04; // N=15

        SetPars();
    }

    double ExpVal(double genv) { return (mInitialized ? genv*mCoeffs(1,0) + mCoeffs(0,0) : genv); }
    double GenVal(double fitv) { return (mInitialized ? (fitv - mCoeffs(0,0))/mCoeffs(1,0) : fitv); }

    void SetPars() {
        TDecompSVD inversion(mVals);
        TMatrixD tmpInv(DIM*DEG,DIM*DEG);
        if (inversion.Invert(tmpInv)) {
            for (unsigned row = 0; row < 2; ++row) {
                for (unsigned col = 0; col < DIM*DEG; ++col) {
                    mInverted(row,col) = tmpInv(row,col);
                }
            }
            mCoeffs = mInverted * mExp;
            mInitialized = true;

            const TMatrixD tmpV = inversion.GetV();
            const TMatrixD tmpVt(TMatrixD::kTransposed,tmpV);
            TVectorD tmpS = inversion.GetSig();
            TMatrixD tmpDm2(2,2);
            for (int i = 0; i < 2; ++i) {
                for (int j = 0; j < 2; ++j)
                    tmpDm2(i,j) = 0;
            }
            tmpDm2(0,0) = pow(tmpS(0),-2);
            tmpDm2(1,1) = pow(tmpS(1),-2);
            mErrInverter = tmpV*tmpDm2*tmpVt;

            // Calculating the combined error
            mChi2 = 0;
            mChi2Scaled = 0;
            for (unsigned idx = 0; idx < DIM*DEG; ++idx) {
                double diff2 = pow(ExpVal(mVals(idx,1))-mExp(idx,0),2);
                mChi2 += diff2;
                double err = mErrs(idx,0);
                if (err==0) err = 1;
                mChi2Scaled += diff2/pow(err,2);
            }
        }
    }

    double GetChi2() { return mChi2; }
    double GetChi2Scaled() { return mChi2Scaled; }
    // DIM values, 2 parameters
    double GetNDoF() { return DIM*DEG-2.0; }
    double GetChi2PerNDoF() { return GetChi2()/GetNDoF(); }
    double GetQ0() { return mCoeffs(0,0); }
    double GetQ1() { return mCoeffs(1,0); }
    double GetKHatErr(double KHat) {
        TMatrixD vec(2,1);
        vec(0,0) = 1;
        vec(1,0) = KHat;
        const TMatrixD vecT(TMatrixD::kTransposed,vec);
        TMatrixD res = vecT*mErrInverter*vec;
        return mTVal*sqrt(GetChi2PerNDoF()*res(0,0));
    }

    void Print() {
        cout << "Fit = " << mCoeffs(0,0) << " + " << mCoeffs(1,0) << "*Gen, chi2/ndof = " << GetChi2() << "/" << GetNDoF() << " = " << GetChi2PerNDoF() << endl;
    }

private:
    TMatrixD mExp;
    TMatrixD mErrs;
    TMatrixD mVals;
    TMatrixD mInverted;
    TMatrixD mCoeffs;
    TMatrixD mErrInverter;

    double mTVal;
    double mChi2;
    double mChi2Scaled;
    bool mInitialized;
};

// fit = a + b*gen1 + c*gen2
template <int DIMX, int DIMY>
class Fitter2D {
public:
    Fitter2D(vector<double> expv = vector<double>(DIMX*DIMY,1), vector<double> genvx = vector<double>(DIMX,1),
             vector<double> genvy = vector<double>(DIMY,1), vector<double> experr = vector<double>(DIMX*DIMY,1)) :
    mInverted(3,DIMX*DIMY),mCoeffs(3,1),mExp(DIMX*DIMY,1),mErrs(DIMX*DIMY,1),mVals(DIMX*DIMY,3),mErrInverter(3,3) {
        mInitialized = false;
        if (genvx.size()!=DIMX or genvy.size()!=DIMY or expv.size()!=DIMX*DIMY or experr.size()!=DIMX*DIMY) return;

        for (unsigned xidx = 0; xidx < DIMX; ++xidx) {
            for (unsigned yidx = 0; yidx < DIMY; ++yidx) {
                int idx = xidx*DIMY+yidx;
                mExp(idx,0) = expv[idx];
                mErrs(idx,0) = experr[idx];
                mVals(idx,0) = 1;
                mVals(idx,1) = genvx[xidx];
                mVals(idx,2) = genvy[yidx];
            }
        }
        mChi2 = 0.0;
        mMLKJES = 1.0;
        //mTVal = 1.0; // N = Infinity
        mTVal = 1.04344; // N = 15

        SetPars();
    }

    void SetMLKJES(double KJES) { mMLKJES = KJES; }
    double GetMLKJES() { return mMLKJES; }

    double ExpVal(double genvx, double genvy) { return mInitialized ? genvy*mCoeffs(2,0) + genvx*mCoeffs(1,0) + mCoeffs(0,0) : 0.0; }
    double GenVal(double genvx, double expv) { return mInitialized ? (expv - genvx*mCoeffs(1,0) - mCoeffs(0,0))/mCoeffs(2,0) : 0.0; }
    double MLExpVal(double genvy) { return ExpVal(mMLKJES,genvy); }
    double MLGenVal(double expv ) { return GenVal(mMLKJES,expv ); }

    void SetPars() {
        TDecompSVD inversion(mVals);
        TMatrixD tmpInv(DIMX*DIMY,DIMX*DIMY);
        if (inversion.Invert(tmpInv)) {
            for (unsigned row = 0; row < 3; ++row) {
                for (unsigned col = 0; col < DIMX*DIMY; ++col) {
                    mInverted(row,col) = tmpInv(row,col);
                }
            }
            mCoeffs = mInverted * mExp;
            mInitialized = true;

            const TMatrixD tmpV = inversion.GetV();
            const TMatrixD tmpVt(TMatrixD::kTransposed,tmpV);
            TVectorD tmpS = inversion.GetSig();
            TMatrixD tmpDm2(3,3);
            for (int i = 0; i < 3; ++i) {
                for (int j = 0; j < 3; ++j)
                    tmpDm2(i,j) = 0;
            }
            tmpDm2(0,0) = pow(tmpS(0),-2);
            tmpDm2(1,1) = pow(tmpS(1),-2);
            tmpDm2(2,2) = pow(tmpS(2),-2);
            mErrInverter = tmpV*tmpDm2*tmpVt;

            // Calculating the combined error
            mChi2 = 0;
            mChi2Scaled = 0;
            for (unsigned idx = 0; idx < DIMX*DIMY; ++idx) {
                double diff2 = pow(ExpVal(mVals(idx,1),mVals(idx,2))-mExp(idx,0),2);
                mChi2 += diff2;
                double err = mErrs(idx,0);
                if (err==0) err = 1;
                mChi2Scaled += diff2/pow(err,2);
            }
        }
    }

    double GetChi2() { return mChi2; }
    // DIMX*DIMY values, 3 parameters
    double GetNDoF() { return DIMX*DIMY-3.0; }
    double GetChi2PerNDoF() { return GetChi2()/GetNDoF(); }
    double GetKCoeff() { return mCoeffs(1,0)/mCoeffs(2,0); }
    double GetP0() { return mCoeffs(0,0); }
    double GetP1() { return mCoeffs(2,0); }
    double GetP2() { return mCoeffs(1,0); }

    double GetMHatErr(double M) {
        TMatrixD vec(3,1);
        vec(0,0) = 1;
        vec(1,0) = mMLKJES;
        vec(2,0) = M;
        const TMatrixD vecT(TMatrixD::kTransposed,vec);
        TMatrixD res = vecT*mErrInverter*vec;
        return mTVal*sqrt(GetChi2PerNDoF()*res(0,0));
    }

    void Print() {
        cout << "Fit = " << mCoeffs(0,0) << " + " << mCoeffs(1,0) << "*GenX + " << mCoeffs(2,0) << "*GenY, chi2/ndof = " << GetChi2() << "/" << GetNDoF() << " = " << GetChi2PerNDoF() <<endl;
    }

    void PrintShifted(double q0, double q1) {
        cout << "Fit = " << mCoeffs(0,0)-mCoeffs(1,0)*q0/q1 << " + " << mCoeffs(1,0)/q1 << "*GenX + " << mCoeffs(2,0) << "*GenY, chi2 = " << GetChi2() <<endl;
    }

private:
    TMatrixD mExp;
    TMatrixD mErrs;
    TMatrixD mVals;
    TMatrixD mInverted;
    TMatrixD mCoeffs;
    TMatrixD mErrInverter;

    double mMLKJES;
    double mChi2Scaled;
    double mChi2;
    double mTVal;
    bool mInitialized;
};

class SnglParser {
private:
    bool             mDoNu;
    int              mWHadr;
    int              mWHadrNu;

    vector<string>   mNames;

    vector<unsigned> mTIdx;
    vector<unsigned> mWIdx;
    vector<MTriplet> mMFit;
    vector<MTriplet> mMAve;
    vector<MTriplet> mMItg;
    vector<MTriplet> mMMax;
    vector<MTriplet> mMMed;
    vector<MTriplet> mChi2;
    vector<MTriplet> mNDF;
    vector<MTriplet> mFErr;

    vector<Fitter1D<3>> mTFit;
    vector<Fitter1D<3>> mTAve;
    vector<Fitter1D<3>> mTItg;
    vector<Fitter1D<3>> mTMax;
    vector<Fitter1D<3>> mTMed;

    vector<pair<double,double>> mWFit;
    vector<pair<double,double>> mWAve;
    vector<pair<double,double>> mWItg;
    vector<pair<double,double>> mWMax;
    vector<pair<double,double>> mWMed;

public:
    SnglParser(string dir = ".") {
        mDoNu = false; /* Deducted from the files. */
        if (!Parse(dir)) cerr << "Something went wrong in the parsing process!" << endl;
        if (!WProc())    cerr << "Something went wrong in the W processing routine!" << endl;
        if (!TProc())    cerr << "Something went wrong in the top processing routine!" << endl;
    }

    bool DoNu() { return mDoNu; }

    int HistIdx(string name) {
        for (unsigned idx = 0; idx < mNames.size(); ++idx) {
            if (mNames[idx]==name) return idx;
        }
        return -1;
    }

    int TIdx(string name) {
        for (unsigned tidx = 0; tidx < mTIdx.size(); ++tidx) {
            auto &idx = mTIdx[tidx];
            if (mNames[idx]==name) return tidx;
        }
        return -1;
    }

    int WIdx(string name) {
        for (unsigned widx = 0; widx < mWIdx.size(); ++widx) {
            auto &idx = mWIdx[widx];
            if (mNames[idx]==name) return widx;
        }
        return -1;
    }

    void PrintNames() {
        cout << "Single histos names:" << endl;
        for (auto &nm : mNames) cout << " " << nm << endl;
    }

    /* W peak position. */
    inline double WPeak(vector<pair<double,double>> &W, bool nu) {
        int idx = nu ? mWHadrNu : mWHadr;
        return W[idx].first;
    }
    inline double WSigma(vector<pair<double,double>> &W, bool nu) {
        int idx = nu ? mWHadrNu : mWHadr;
        return W[idx].second;
    }
    /* Generic functions for "experimental KJES", called by all estimators. */
    inline double KJESExp   (vector<pair<double,double>> &W, bool nu) { return WPeak (W,nu)/80.4; }
    inline double KJESExpErr(vector<pair<double,double>> &W, bool nu) { return WSigma(W,nu)/80.4; }
    /* Specialized W Peak functions. */
    double WPeakFit     (bool nu = false) { return WPeak(mWFit, nu); }
    double WPeakAve     (bool nu = false) { return WPeak(mWAve, nu); }
    double WPeakItg     (bool nu = false) { return WPeak(mWItg, nu); }
    double WPeakMax     (bool nu = false) { return WPeak(mWMax, nu); }
    double WPeakMed     (bool nu = false) { return WPeak(mWMed, nu); }
    /* Specialized W peak error functions. */
    double WSigmaFit    (bool nu = false) { return WSigma(mWFit, nu); }
    double WSigmaAve    (bool nu = false) { return WSigma(mWAve, nu); }
    double WSigmaItg    (bool nu = false) { return WSigma(mWItg, nu); }
    double WSigmaMax    (bool nu = false) { return WSigma(mWMax, nu); }
    double WSigmaMed    (bool nu = false) { return WSigma(mWMed, nu); }
    /* Specialized residual KJES functions. */
    double KJESExpFit   (bool nu = false) { return KJESExp(mWFit, nu); }
    double KJESExpAve   (bool nu = false) { return KJESExp(mWAve, nu); }
    double KJESExpItg   (bool nu = false) { return KJESExp(mWItg, nu); }
    double KJESExpMax   (bool nu = false) { return KJESExp(mWMax, nu); }
    double KJESExpMed   (bool nu = false) { return KJESExp(mWMed, nu); }
    /* Specialized residual KJES error functions. */
    double KJESExpErrFit(bool nu = false) { return KJESExpErr(mWFit, nu); }
    double KJESExpErrAve(bool nu = false) { return KJESExpErr(mWAve, nu); }
    double KJESExpErrItg(bool nu = false) { return KJESExpErr(mWItg, nu); }
    double KJESExpErrMax(bool nu = false) { return KJESExpErr(mWMax, nu); }
    double KJESExpErrMed(bool nu = false) { return KJESExpErr(mWMed, nu); }

    unsigned NumberHists() { return mNames.size(); }
    string   GetName(unsigned hidx) { return (hidx>mNames.size() ? "" : mNames[hidx]); }

private:
    bool Parse(string dir) {
        std::fstream fs(Form("%s/snginfo.txt",dir.c_str()));
        if (!fs.is_open()) return false;

        unsigned nHists = 0;
        unsigned nInfos = 0;
        unsigned tmpI;
        double tmpD, tmpD1, tmpD2, tmpD3;
        string tmpS;
        fs >> nHists >> tmpS >> tmpS >> tmpS >> nInfos >> tmpS >> tmpS >> tmpS;

        /* Loop over histograms. */
        for (unsigned nH = 0; nH < nHists; ++nH) {
            fs >> tmpI >> tmpS;
            if (tmpI != nH) return false;
            mNames.push_back(tmpS);
            if (SubText(tmpS,"tmass_")) {
                mTIdx.push_back(tmpI); /* Histogram name. */
            } else if (SubText(tmpS,"wmass_")) {
                mWIdx.push_back(tmpI); /* Histogram name. */
            }
            if (SubText(tmpS,"nu_")) mDoNu = true;
            /* Loop over mass types. */
            for (unsigned nI = 0; nI < nInfos; ++nI) {
                fs >> tmpS; /* Mass variable name */
                /* Loop over the kjes values. */

                /* Loop over the three gen masses. */
                fs >> tmpD >> tmpD1;
                if (tmpD!=172.5) return false;
                fs >> tmpD >> tmpD2;
                if (tmpD!=173.7) return false;
                fs >> tmpD >> tmpD3;
                if (tmpD!=175.0) return false;

                if      (tmpS == "fit") mMFit.push_back(MTriplet{tmpD1,tmpD2,tmpD3});
                else if (tmpS == "ave") mMAve.push_back(MTriplet{tmpD1,tmpD2,tmpD3});
                else if (tmpS == "itg") mMItg.push_back(MTriplet{tmpD1,tmpD2,tmpD3});
                else if (tmpS == "max") mMMax.push_back(MTriplet{tmpD1,tmpD2,tmpD3});
                else if (tmpS == "med") mMMed.push_back(MTriplet{tmpD1,tmpD2,tmpD3});
                else if (tmpS == "ch2") mChi2.push_back(MTriplet{tmpD1,tmpD2,tmpD3});
                else if (tmpS == "ndf") mNDF .push_back(MTriplet{tmpD1,tmpD2,tmpD3});
                else if (tmpS == "fer") mFErr.push_back(MTriplet{tmpD1,tmpD2,tmpD3});
            }
        }
        fs.close();
        if (mTIdx .size()+mWIdx.size()!=nHists) return false;
        if (mNames.size()             !=nHists) return false;
        if (mMFit .size()             !=nHists) return false;
        if (mMAve .size()             !=nHists) return false;
        if (mMItg .size()             !=nHists) return false;
        if (mMMax .size()             !=nHists) return false;
        if (mMMed .size()             !=nHists) return false;
        if (mChi2 .size()             !=nHists) return false;
        if (mNDF  .size()             !=nHists) return false;
        if (mFErr .size()             !=nHists) return false;

        cerr << "Parsed successfully " << nHists << " histograms with " << nInfos << " pieces of information." << endl;

        mWHadr   = WIdx("wmass_hadr_");
        mWHadrNu = mDoNu ? WIdx("wmass_hadrnu_") : 0;
        if (mWHadrNu<0 or mWHadrNu<0) {
            cerr << "W histogram not found!" << endl;
            return false;
        }

        return true;
    }

    pair<double,double> AveSigma(MTriplet ms) {
        double ave = (ms.v1725+ms.v1737+ms.v1750)/3.0;
        double s2  = (pow(ms.v1725-ave,2)+pow(ms.v1737-ave,2)+pow(ms.v1750-ave,2))/(3.0*2.0);
        return std::make_pair(ave,TMath::Sqrt(s2));
    }

    bool WProc() {
        for (unsigned widx = 0; widx < mWIdx.size(); ++widx) {
            auto &idx = mWIdx[widx];
            mWFit.emplace_back(AveSigma(mMFit[idx]));
            mWAve.emplace_back(AveSigma(mMAve[idx]));
            mWItg.emplace_back(AveSigma(mMItg[idx]));
            mWMax.emplace_back(AveSigma(mMMax[idx]));
            mWMed.emplace_back(AveSigma(mMMed[idx]));
        }

        if (mWFit.size()!=mWIdx.size()) return false;
        if (mWAve.size()!=mWIdx.size()) return false;
        if (mWItg.size()!=mWIdx.size()) return false;
        if (mWMax.size()!=mWIdx.size()) return false;
        if (mWMed.size()!=mWIdx.size()) return false;

        cerr << "Inspected successfully " << mWIdx.size() << " W mass histograms." << endl;
        return true;
    }

    bool TProc() {
        for (unsigned tidx = 0; tidx < mTIdx.size(); ++tidx) {
            auto &idx = mTIdx[tidx];

            mTFit.emplace_back(TripletToV(mMFit[idx]));
            mTAve.emplace_back(TripletToV(mMAve[idx]));
            mTItg.emplace_back(TripletToV(mMItg[idx]));
            mTMax.emplace_back(TripletToV(mMMax[idx]));
            mTMed.emplace_back(TripletToV(mMMed[idx]));
        }

        cerr << "Inspected successfully " << mTIdx.size() << " top mass histograms." << endl;
        return true;
    }
};

class ErasParser {
private:
    bool             mUseD0KJES;
    int              mD0Shift;
    bool             mDoNu;

    vector<string>   mNames;
    vector<double>   mKJES;
    unsigned         mRefKJES;

    int              mWHadr;
    int              mWHadrNu;

    vector<unsigned> mTIdx;
    vector<unsigned> mWIdx;
    vector<MEras>    mMFit;
    vector<MEras>    mMAve;
    vector<MEras>    mMItg;
    vector<MEras>    mMMax;
    vector<MEras>    mMMed;
    vector<MEras>    mChi2;
    vector<MEras>    mNDF;
    vector<MEras>    mFErr;

    vector<Fitter1D<5,3>> mKJESFit2;
    vector<Fitter1D<5,3>> mKJESAve2;
    vector<Fitter1D<5,3>> mKJESItg2;
    vector<Fitter1D<5,3>> mKJESMax2;
    vector<Fitter1D<5,3>> mKJESMed2;
    vector<Fitter1D<5,3>> mKJESNuFit2;
    vector<Fitter1D<5,3>> mKJESNuAve2;
    vector<Fitter1D<5,3>> mKJESNuItg2;
    vector<Fitter1D<5,3>> mKJESNuMax2;
    vector<Fitter1D<5,3>> mKJESNuMed2;
    vector<Fitter1D<5>> mKJESTest;
    vector<Fitter1D<5>> mKJESFit;
    vector<Fitter1D<5>> mKJESAve;
    vector<Fitter1D<5>> mKJESItg;
    vector<Fitter1D<5>> mKJESMax;
    vector<Fitter1D<5>> mKJESMed;
    vector<Fitter1D<5>> mKJESNuFit;
    vector<Fitter1D<5>> mKJESNuAve;
    vector<Fitter1D<5>> mKJESNuItg;
    vector<Fitter1D<5>> mKJESNuMax;
    vector<Fitter1D<5>> mKJESNuMed;

    vector<Fitter2D<5,3>> mTKJESFit;
    vector<Fitter2D<5,3>> mTKJESAve;
    vector<Fitter2D<5,3>> mTKJESItg;
    vector<Fitter2D<5,3>> mTKJESMax;
    vector<Fitter2D<5,3>> mTKJESMed;

    vector<double> mWFit2;
    vector<double> mWAve2;
    vector<double> mWItg2;
    vector<double> mWMax2;
    vector<double> mWMed2;
    vector<pair<double,double>> mWFit;
    vector<pair<double,double>> mWAve;
    vector<pair<double,double>> mWItg;
    vector<pair<double,double>> mWMax;
    vector<pair<double,double>> mWMed;

    vector<double> mEraD0Err;
    vector<double> mEraD0ErrEl;
    vector<double> mEraD0ErrMu;
    vector<double> mEraD0Mt;
    vector<double> mEraD0MtEl;
    vector<double> mEraD0MtMu;
    vector<double> mEraD0KJES;
    vector<double> mEraD0KJESEl;
    vector<double> mEraD0KJESMu;
    vector<double> mEraD0KErr;
    vector<double> mEraD0KErrEl;
    vector<double> mEraD0KErrMu;

    vector<double> mKJESHat;
    vector<double> mKJESHatNu;
    vector<double> mPlainKJES;
    vector<double> mPlainKJESNu;

public:
    ErasParser(string dir = ".", int shiftmt = 0, bool used0kjes = false) {
        mDoNu = false; /* Deducted from the files. */
        mD0Shift   = shiftmt;
        mUseD0KJES = used0kjes;
        if (mD0Shift < 0 or mD0Shift > 3) {
            mD0Shift = 0;
            cerr << "Erroneous value of mass shift type." << endl;
        }

        if (!Parse(dir)) cerr << "Something went wrong in the parsing process!" << endl;
        if (!WProc())    cerr << "Something went wrong in the W processing routine!" << endl;

        mEraD0Mt      = {175.29 ,174.165,173.95 ,175.761 };
        mEraD0MtEl    = {177.65 ,174.54 ,173.44 ,176.61  };
        mEraD0MtMu    = {172.67 ,173.76 ,174.46 ,174.79  };
        mEraD0Err     = {1.88603,1.6431 ,1.06773,0.840032};
        mEraD0ErrEl   = {2.60   ,2.28   ,1.51   ,1.15    };
        mEraD0ErrMu   = {2.74   ,2.37   ,1.51   ,1.23    };
        mEraD0KJES    = {0.993,1.027,1.033,1.026};
        mEraD0KJESEl  = {1.000,1.016,1.034,1.027};
        mEraD0KJESMu  = {0.986,1.038,1.032,1.025};
        mEraD0KErr    = {0.016,0.013,0.008,0.006};
        mEraD0KErrEl  = {0.023,0.019,0.012,0.008};
        mEraD0KErrMu  = {0.022,0.019,0.012,0.010};
    }

    void FitAll(SnglParser &sng, bool nokjes = false) {
        if (!Fit()) cerr << "Something went wrong in the top fitting process!" << endl;

        mKJESHat.clear();
        mKJESHatNu.clear();
        mPlainKJES.clear();
        mPlainKJESNu.clear();
        for (unsigned eidx = 0; eidx < 4; ++eidx) {
            if (nokjes or mUseD0KJES) {
                double tmpKJES = nokjes ? 1.0 : (mD0Shift==1 ? mEraD0KJES[eidx] : (mD0Shift==2 ? mEraD0KJESEl[eidx] : mEraD0KJESMu[eidx]));
                for (unsigned i = 0; i < 5; ++i) mKJESHat.push_back(1.0);
                mPlainKJES.push_back(tmpKJES);
                mPlainKJES.push_back(tmpKJES);
                mPlainKJES.push_back(tmpKJES);
                mPlainKJES.push_back(tmpKJES);
                mPlainKJES.push_back(tmpKJES);
                if (mDoNu) {
                    for (unsigned i = 0; i < 5; ++i) mKJESHatNu.push_back(1.0);
                    mPlainKJESNu.push_back(tmpKJES);
                    mPlainKJESNu.push_back(tmpKJES);
                    mPlainKJESNu.push_back(tmpKJES);
                    mPlainKJESNu.push_back(tmpKJES);
                    mPlainKJESNu.push_back(tmpKJES);
                }
            } else {
                mKJESHat.push_back(sng.KJESExpFit());
                mPlainKJES.push_back(mKJESFit[eidx].GenVal(mKJESHat.back()));
                mKJESHat.push_back(sng.KJESExpAve());
                mPlainKJES.push_back(mKJESAve[eidx].GenVal(mKJESHat.back()));
                mKJESHat.push_back(sng.KJESExpItg());
                mPlainKJES.push_back(mKJESItg[eidx].GenVal(mKJESHat.back()));
                mKJESHat.push_back(sng.KJESExpMax());
                mPlainKJES.push_back(mKJESMax[eidx].GenVal(mKJESHat.back()));
                mKJESHat.push_back(sng.KJESExpMed());
                mPlainKJES.push_back(mKJESMed[eidx].GenVal(mKJESHat.back()));
                if (mDoNu) {
                    mKJESHatNu.push_back(sng.KJESExpFit(true));
                    mPlainKJESNu.push_back(mKJESNuFit[eidx].GenVal(mKJESHatNu.back()));
                    mKJESHatNu.push_back(sng.KJESExpAve(true));
                    mPlainKJESNu.push_back(mKJESNuAve[eidx].GenVal(mKJESHatNu.back()));
                    mKJESHatNu.push_back(sng.KJESExpItg(true));
                    mPlainKJESNu.push_back(mKJESNuItg[eidx].GenVal(mKJESHatNu.back()));
                    mKJESHatNu.push_back(sng.KJESExpMax(true));
                    mPlainKJESNu.push_back(mKJESNuMax[eidx].GenVal(mKJESHatNu.back()));
                    mKJESHatNu.push_back(sng.KJESExpMed(true));
                    mPlainKJESNu.push_back(mKJESNuMed[eidx].GenVal(mKJESHatNu.back()));
                }
            }
        }

        for (unsigned tidx = 0; tidx < mTIdx.size(); ++tidx) {
            bool nu = mDoNu and SubText(mNames[mTIdx[tidx]],"nu_");
            if (nu) cout << "Using neutrino mode for histrogram " << mNames[mTIdx[tidx]] << endl;
            for (unsigned eidx = 0; eidx < 4; ++eidx) {
                int totidx = 4*tidx+eidx;
                mTKJESFit[totidx].SetMLKJES(nu ? mPlainKJESNu[5*eidx  ] : mPlainKJES[5*eidx  ]);
                mTKJESAve[totidx].SetMLKJES(nu ? mPlainKJESNu[5*eidx+1] : mPlainKJES[5*eidx+1]);
                mTKJESItg[totidx].SetMLKJES(nu ? mPlainKJESNu[5*eidx+2] : mPlainKJES[5*eidx+2]);
                mTKJESMax[totidx].SetMLKJES(nu ? mPlainKJESNu[5*eidx+3] : mPlainKJES[5*eidx+3]);
                mTKJESMed[totidx].SetMLKJES(nu ? mPlainKJESNu[5*eidx+4] : mPlainKJES[5*eidx+4]);
            }
        }

        for (unsigned tidx = 0; tidx < mTIdx.size(); ++tidx) {
            auto &idx = mTIdx[tidx];
            cout << mNames[idx] << ": " << endl;
            for (unsigned eidx = 0; eidx < 4; ++eidx) {
                mTKJESAve[4*tidx+eidx].Print();
            }
            cout << endl;
        }

        for (unsigned eidx = 0; eidx < 4; ++eidx) {
            mKJESAve2[mWHadr*4+eidx].Print();
        }
        cout << endl;
    }

    double GetKJES(int eidx = 0, int mode = 1, bool nu = false) {
        if (eidx<0 or mode<0 or mode>4) { cerr << "Invalid KJES settings!" << endl; return 1.0; }
        unsigned kjesidx = 5*eidx+mode;
        if ((nu and kjesidx>=mPlainKJESNu.size()) or (!nu and kjesidx>=mPlainKJES.size())) return 1.0;
        return nu ? mPlainKJESNu[kjesidx] : mPlainKJES[kjesidx];
    }

    double GetKCoeff(int tidx ,int eidx = 0, int mode = 1) {
        unsigned totidx = 4*tidx+eidx;
        if (tidx<0 or eidx<0 or totidx>=mTKJESAve.size()) { cerr << "Invalid k coeff settings!" << endl; return 0.0; }

        if      (mode==0) return mTKJESFit[totidx].GetKCoeff();
        else if (mode==1) return mTKJESAve[totidx].GetKCoeff();
        else if (mode==2) return mTKJESItg[totidx].GetKCoeff();
        else if (mode==3) return mTKJESMax[totidx].GetKCoeff();
        else if (mode==4) return mTKJESMed[totidx].GetKCoeff();

        return 0.0;
    }

    int HistIdx(string name) {
        for (unsigned idx = 0; idx < mNames.size(); ++idx) {
            if (mNames[idx]==name) return idx;
        }
        return -1;
    }

    int TIdx(string name) {
        for (unsigned tidx = 0; tidx < mTIdx.size(); ++tidx) {
            auto &idx = mTIdx[tidx];
            if (mNames[idx]==name) return tidx;
        }
        return -1;
    }

    int WIdx(string name) {
        for (unsigned widx = 0; widx < mWIdx.size(); ++widx) {
            auto &idx = mWIdx[widx];
            if (mNames[idx]==name) return widx;
        }
        return -1;
    }

    void PrintNames() {
        cout << "Era histos names:" << endl;
        for (auto &nm : mNames) cout << " " << nm << endl;
    }

    inline double MV1750(MEras &m, int eidx=0) {
        if      (eidx==0) return m.a.v1750;
        else if (eidx==1) return m.b1.v1750;
        else if (eidx==2) return m.b2.v1750;
        else if (eidx==3) return m.b34.v1750;
        return 0;
    }

    /* pos: IIa location, eid: era index */
    inline double ExpMt(int pos, unsigned tidx, int mode=0, int eidx=0, int kidx = -1) {
        if (mD0Shift==0) {
            int idx = mTIdx[tidx]*mKJES.size()+kidx;
            if      (mode==0) return MV1750(mMFit[idx]);
            else if (mode==1) return MV1750(mMAve[idx]);
            else if (mode==2) return MV1750(mMItg[idx]);
            else if (mode==3) return MV1750(mMMax[idx]);
            else if (mode==4) return MV1750(mMMed[idx]);
        } else {
            double refval = 175;
            if      (mD0Shift==1) refval = mEraD0Mt  [eidx];
            else if (mD0Shift==2) refval = mEraD0MtEl[eidx];
            else if (mD0Shift==3) refval = mEraD0MtMu[eidx];
            else cerr << "Lacking D0 Shift mode!" << endl;

            if      (mode==0) return mTKJESFit[pos+eidx].MLExpVal(refval);
            else if (mode==1) return mTKJESAve[pos+eidx].MLExpVal(refval);
            else if (mode==2) return mTKJESItg[pos+eidx].MLExpVal(refval);
            else if (mode==3) return mTKJESMax[pos+eidx].MLExpVal(refval);
            else if (mode==4) return mTKJESMed[pos+eidx].MLExpVal(refval);
            else { cerr << "Erroneous mode!" << endl; }
        }
        return -4;
    }
    inline int GetMtPos(unsigned tidx) {
        if (tidx > mTIdx.size()) return -1;

        return 4*tidx;
    }
    inline double GetMT_IIxFromIIx(double mtexp, int pos, int mode=0, int eidx=0) {
        if      (mode==0) return mTKJESFit[pos+eidx].MLGenVal(mtexp);
        else if (mode==1) return mTKJESAve[pos+eidx].MLGenVal(mtexp);
        else if (mode==2) return mTKJESItg[pos+eidx].MLGenVal(mtexp);
        else if (mode==3) return mTKJESMax[pos+eidx].MLGenVal(mtexp);
        else if (mode==4) return mTKJESMed[pos+eidx].MLGenVal(mtexp);
        else cerr << "Unallowed mode!" << endl;

        return 0;
    }
    double GetErrCoeff(int tidx, int mode, int eidx) {
        if      (mode==0) return 1.0/mTKJESFit[4*tidx+eidx].GetP1();
        else if (mode==1) return 1.0/mTKJESAve[4*tidx+eidx].GetP1();
        else if (mode==2) return 1.0/mTKJESItg[4*tidx+eidx].GetP1();
        else if (mode==3) return 1.0/mTKJESMax[4*tidx+eidx].GetP1();
                          return 1.0/mTKJESMed[4*tidx+eidx].GetP1();
    }
    double GetKHatErr(int tidx, int mode, int eidx) {
        if      (mode==0) return mKJESFit2[mWHadr*4+eidx].GetKHatErr(mKJESFit2[mWHadr*4+eidx].GenVal(mKJESHat[5*eidx+mode]));
        else if (mode==1) return mKJESAve2[mWHadr*4+eidx].GetKHatErr(mKJESAve2[mWHadr*4+eidx].GenVal(mKJESHat[5*eidx+mode]));
        else if (mode==2) return mKJESItg2[mWHadr*4+eidx].GetKHatErr(mKJESItg2[mWHadr*4+eidx].GenVal(mKJESHat[5*eidx+mode]));
        else if (mode==3) return mKJESMax2[mWHadr*4+eidx].GetKHatErr(mKJESMax2[mWHadr*4+eidx].GenVal(mKJESHat[5*eidx+mode]));
                          return mKJESMed2[mWHadr*4+eidx].GetKHatErr(mKJESMed2[mWHadr*4+eidx].GenVal(mKJESHat[5*eidx+mode]));
    }
    // Same as the above with two additional scales
    double GetMtHatKHatErr(int tidx, int mode, int eidx) {
        if      (mode==0) return mKJESFit2[mWHadr*4+eidx].GetKHatErr(mKJESFit2[mWHadr*4+eidx].GenVal(mKJESHat[5*eidx+mode]))*mTKJESFit[4*tidx+eidx].GetP2()/mKJESFit[eidx].GetQ1();
        else if (mode==1) return mKJESAve2[mWHadr*4+eidx].GetKHatErr(mKJESAve2[mWHadr*4+eidx].GenVal(mKJESHat[5*eidx+mode]))*mTKJESAve[4*tidx+eidx].GetP2()/mKJESAve[eidx].GetQ1();
        else if (mode==2) return mKJESItg2[mWHadr*4+eidx].GetKHatErr(mKJESItg2[mWHadr*4+eidx].GenVal(mKJESHat[5*eidx+mode]))*mTKJESItg[4*tidx+eidx].GetP2()/mKJESItg[eidx].GetQ1();
        else if (mode==3) return mKJESMax2[mWHadr*4+eidx].GetKHatErr(mKJESMax2[mWHadr*4+eidx].GenVal(mKJESHat[5*eidx+mode]))*mTKJESMax[4*tidx+eidx].GetP2()/mKJESMax[eidx].GetQ1();
                          return mKJESMed2[mWHadr*4+eidx].GetKHatErr(mKJESMed2[mWHadr*4+eidx].GenVal(mKJESHat[5*eidx+mode]))*mTKJESMed[4*tidx+eidx].GetP2()/mKJESMed[eidx].GetQ1();
    }
    double GetMtHatErr(int tidx, int mode, int eidx, double mhat) {
        if      (mode==0) return mTKJESFit[4*tidx+eidx].GetMHatErr(mTKJESFit[4*tidx+eidx].MLGenVal(mhat));
        else if (mode==1) return mTKJESAve[4*tidx+eidx].GetMHatErr(mTKJESAve[4*tidx+eidx].MLGenVal(mhat));
        else if (mode==2) return mTKJESItg[4*tidx+eidx].GetMHatErr(mTKJESItg[4*tidx+eidx].MLGenVal(mhat));
        else if (mode==3) return mTKJESMax[4*tidx+eidx].GetMHatErr(mTKJESMax[4*tidx+eidx].MLGenVal(mhat));
                          return mTKJESMed[4*tidx+eidx].GetMHatErr(mTKJESMed[4*tidx+eidx].MLGenVal(mhat));
    }
    double GetMtIIxFromMtExp(double mtexp, unsigned tidx, int mode=0, int eidx=0) {
        int pos = GetMtPos(tidx);
        return pos==-1 ? 0 : GetMT_IIxFromIIx(mtexp, pos, mode, eidx);
    }
    inline double GetMT_IIaFromIIx(unsigned tidx, int mode=0, int eidx=0) {
        int pos = GetMtPos(tidx);
        return pos==-1 ? 0 : GetMT_IIxFromIIx(ExpMt(pos,tidx,mode,eidx),pos,mode,0);
    }
    double GetMT_IIaFromIIa(unsigned tidx, int mode=0)   { return GetMT_IIaFromIIx(tidx,mode,0); }
    double GetMT_IIaFromIIb1(unsigned tidx, int mode=0)  { return GetMT_IIaFromIIx(tidx,mode,1); }
    double GetMT_IIaFromIIb2(unsigned tidx, int mode=0)  { return GetMT_IIaFromIIx(tidx,mode,2); }
    double GetMT_IIaFromIIb34(unsigned tidx, int mode=0) { return GetMT_IIaFromIIx(tidx,mode,3); }
    double GetMT_Ave(unsigned tidx, int mode=0) {
        if (mD0Shift<1 or mD0Shift>3) return 1;
        vector<double> masses{GetMT_IIaFromIIa(tidx,mode),GetMT_IIaFromIIb1(tidx,mode),
                              GetMT_IIaFromIIb2(tidx,mode),GetMT_IIaFromIIb34(tidx,mode)};

        double divisor = 0;
        if      (mD0Shift==1) for (auto &e : mEraD0Err)   divisor += pow(e,-2);
        else if (mD0Shift==2) for (auto &e : mEraD0ErrEl) divisor += pow(e,-2);
        else if (mD0Shift==3) for (auto &e : mEraD0ErrMu) divisor += pow(e,-2);
        double upstairs = 0;
        for (unsigned eidx=0; eidx<4; ++eidx) {
            if      (mD0Shift==1) upstairs += pow(mEraD0Err  [eidx],-2)*masses[eidx];
            else if (mD0Shift==2) upstairs += pow(mEraD0ErrEl[eidx],-2)*masses[eidx];
            else if (mD0Shift==3) upstairs += pow(mEraD0ErrMu[eidx],-2)*masses[eidx];
        }
        return upstairs/divisor;
    }

    MEras GetChi2(unsigned tidx, int kidx = -1) {
        if (kidx==-1) kidx = mRefKJES;
        auto &idx = mTIdx[tidx];
        int totidx = idx*mKJES.size()+kidx;
        return mChi2[totidx];
    }

    MEras GetNDF(unsigned tidx, int kidx = -1) {
        if (kidx==-1) kidx = mRefKJES;
        auto &idx = mTIdx[tidx];
        int totidx = idx*mKJES.size()+kidx;
        return mNDF[totidx];
    }

    MEras GetFErr(unsigned tidx, int kidx = -1) {
        if (kidx==-1) kidx = mRefKJES;
        auto &idx = mTIdx[tidx];
        int totidx = idx*mKJES.size()+kidx;
        return mFErr[totidx];
    }

    inline double WPeak(vector<double> &W, bool nu, unsigned eidx, int kidx, int midx) {
        if (kidx==-1) kidx = mRefKJES;
        int idx = (((nu ? mWHadrNu : mWHadr)*mKJES.size()+kidx)*4+eidx)*3+midx;
        //cout << mRefKJES << " " << mWHadr << " " << mWHadrNu << " " << idx << " " << W[idx].first << endl;
        return W[idx];
    }
    inline double WPeak(vector<pair<double,double>> &W, bool nu, unsigned eidx, int kidx) {
        if (kidx==-1) kidx = mRefKJES;
        int idx = ((nu ? mWHadrNu : mWHadr)*mKJES.size()+kidx)*4+eidx;
        //cout << mRefKJES << " " << mWHadr << " " << mWHadrNu << " " << idx << " " << W[idx].first << endl;
        return W[idx].first;
    }
    inline double WSigma(vector<pair<double,double>> &W, bool nu, unsigned eidx, unsigned kidx) {
        if (kidx==-1) kidx = mRefKJES;
        int idx = ((nu ? mWHadrNu : mWHadr)*mKJES.size()+kidx)*4+eidx;
        return W[idx].second;
    }
    inline double KJESExp(vector<double> &W, bool nu, unsigned eidx, int kidx, int midx) {
        return WPeak(W,nu,eidx,kidx,midx)/80.4;
    }
    inline double KJESExp(vector<pair<double,double>> &W, bool nu, unsigned eidx, int kidx) {
        return WPeak(W,nu,eidx,kidx)/80.4;
    }
    inline double KJESExpErr(vector<pair<double,double>> &W, bool nu, unsigned eidx, int kidx) {
        return WSigma(W,nu,eidx,kidx)/80.4;
    }
    double WPeakFit(bool nu = false, unsigned eidx = 0, int kidx = -1) { return WPeak(mWFit,nu,eidx,kidx); }
    double WPeakAve(bool nu = false, unsigned eidx = 0, int kidx = -1) { return WPeak(mWAve,nu,eidx,kidx); }
    double WPeakItg(bool nu = false, unsigned eidx = 0, int kidx = -1) { return WPeak(mWItg,nu,eidx,kidx); }
    double WPeakMax(bool nu = false, unsigned eidx = 0, int kidx = -1) { return WPeak(mWMax,nu,eidx,kidx); }
    double WPeakMed(bool nu = false, unsigned eidx = 0, int kidx = -1) { return WPeak(mWMed,nu,eidx,kidx); }
    double WSigmaFit(bool nu = false, unsigned eidx = 0, int kidx = -1) { return WSigma(mWFit,nu,eidx,kidx); }
    double WSigmaAve(bool nu = false, unsigned eidx = 0, int kidx = -1) { return WSigma(mWAve,nu,eidx,kidx); }
    double WSigmaItg(bool nu = false, unsigned eidx = 0, int kidx = -1) { return WSigma(mWItg,nu,eidx,kidx); }
    double WSigmaMax(bool nu = false, unsigned eidx = 0, int kidx = -1) { return WSigma(mWMax,nu,eidx,kidx); }
    double WSigmaMed(bool nu = false, unsigned eidx = 0, int kidx = -1) { return WSigma(mWMed,nu,eidx,kidx); }
    double KJESExpFit(bool nu, unsigned eidx, int kidx, int midx) { return KJESExp(mWFit2,nu,eidx,kidx,midx); }
    double KJESExpAve(bool nu, unsigned eidx, int kidx, int midx) { return KJESExp(mWAve2,nu,eidx,kidx,midx); }
    double KJESExpItg(bool nu, unsigned eidx, int kidx, int midx) { return KJESExp(mWItg2,nu,eidx,kidx,midx); }
    double KJESExpMax(bool nu, unsigned eidx, int kidx, int midx) { return KJESExp(mWMax2,nu,eidx,kidx,midx); }
    double KJESExpMed(bool nu, unsigned eidx, int kidx, int midx) { return KJESExp(mWMed2,nu,eidx,kidx,midx); }
    double KJESExpFit(bool nu = false, unsigned eidx = 0, int kidx = -1) { return KJESExp(mWFit,nu,eidx,kidx); }
    double KJESExpAve(bool nu = false, unsigned eidx = 0, int kidx = -1) { return KJESExp(mWAve,nu,eidx,kidx); }
    double KJESExpItg(bool nu = false, unsigned eidx = 0, int kidx = -1) { return KJESExp(mWItg,nu,eidx,kidx); }
    double KJESExpMax(bool nu = false, unsigned eidx = 0, int kidx = -1) { return KJESExp(mWMax,nu,eidx,kidx); }
    double KJESExpMed(bool nu = false, unsigned eidx = 0, int kidx = -1) { return KJESExp(mWMed,nu,eidx,kidx); }
    double KJESExpErrFit(bool nu = false, unsigned eidx = 0, int kidx = -1) { return KJESExpErr(mWFit,nu,eidx,kidx); }
    double KJESExpErrAve(bool nu = false, unsigned eidx = 0, int kidx = -1) { return KJESExpErr(mWAve,nu,eidx,kidx); }
    double KJESExpErrItg(bool nu = false, unsigned eidx = 0, int kidx = -1) { return KJESExpErr(mWItg,nu,eidx,kidx); }
    double KJESExpErrMax(bool nu = false, unsigned eidx = 0, int kidx = -1) { return KJESExpErr(mWMax,nu,eidx,kidx); }
    double KJESExpErrMed(bool nu = false, unsigned eidx = 0, int kidx = -1) { return KJESExpErr(mWMed,nu,eidx,kidx); }

    string GetName(unsigned hidx) { return (hidx>mNames.size() ? "" : mNames[hidx]); }
    unsigned NumberHists() { return mNames.size(); }

private:
    bool Parse(string dir) {
        std::fstream fs(Form("%s/erainfo.txt",dir.c_str()));
        if (!fs.is_open()) return false;
        unsigned nHists = 0;
        unsigned nInfos = 0;
        unsigned nKJESs = 0;
        int    tmpI;
        double tmpD, tmpD1, tmpD2, tmpD3, tmpD4;
        MTriplet tmpT1, tmpT2, tmpT3, tmpT4;
        string tmpS;
        bool initKJES = false;
        mRefKJES = 1000;
        fs >> nHists >> tmpS >> tmpS >> nInfos >> tmpS >> tmpS >> tmpS >> tmpS >> nKJESs >> tmpS >> tmpS;
        /* Loop over histograms. */
        for (unsigned nH = 0; nH < nHists; ++nH) {
            fs >> tmpI >> tmpS;
            if (tmpI != nH) return false;
            mNames.push_back(tmpS); /* Histogram name. */
            if (SubText(tmpS,"tmass_")) {
                mTIdx.push_back(tmpI); /* Histogram name. */
            } else if (SubText(tmpS,"wmass_")) {
                mWIdx.push_back(tmpI); /* Histogram name. */
            }
            if (SubText(tmpS,"nu_")) mDoNu = true;
            /* Loop over mass types. */
            for (unsigned nI = 0; nI < nInfos; ++nI) {
                fs >> tmpS; /* Mass variable name */
                /* Loop over the kjes values. */
                for (unsigned kI = 0; kI < nKJESs; ++kI) {
                    fs >> tmpD; /* Current kjes */
                    if (!initKJES) {
                        if (tmpD==1.0) mRefKJES = mKJES.size();
                        mKJES.push_back(tmpD);
                    } else if (kI>=mKJES.size() or mKJES[kI]!=tmpD) return false;

                    /* Loop over the three gen masses. */
                    fs >> tmpD >> tmpD1 >> tmpD2 >> tmpD3 >> tmpD4; /* Vals (four eras). */
                    if (tmpD!=172.5) return false;
                    tmpT1.v1725 = tmpD1;
                    tmpT2.v1725 = tmpD2;
                    tmpT3.v1725 = tmpD3;
                    tmpT4.v1725 = tmpD4;
                    fs >> tmpD >> tmpD1 >> tmpD2 >> tmpD3 >> tmpD4; /* Vals (four eras). */
                    if (tmpD!=173.7) return false;
                    tmpT1.v1737 = tmpD1;
                    tmpT2.v1737 = tmpD2;
                    tmpT3.v1737 = tmpD3;
                    tmpT4.v1737 = tmpD4;
                    fs >> tmpD >> tmpD1 >> tmpD2 >> tmpD3 >> tmpD4; /* Vals (four eras). */
                    if (tmpD!=175.0) return false;
                    tmpT1.v1750 = tmpD1;
                    tmpT2.v1750 = tmpD2;
                    tmpT3.v1750 = tmpD3;
                    tmpT4.v1750 = tmpD4;

                    if      (tmpS == "fit") mMFit.push_back(MEras{tmpT1,tmpT2,tmpT3,tmpT4});
                    else if (tmpS == "ave") mMAve.push_back(MEras{tmpT1,tmpT2,tmpT3,tmpT4});
                    else if (tmpS == "itg") mMItg.push_back(MEras{tmpT1,tmpT2,tmpT3,tmpT4});
                    else if (tmpS == "max") mMMax.push_back(MEras{tmpT1,tmpT2,tmpT3,tmpT4});
                    else if (tmpS == "med") mMMed.push_back(MEras{tmpT1,tmpT2,tmpT3,tmpT4});
                    else if (tmpS == "ch2") mChi2.push_back(MEras{tmpT1,tmpT2,tmpT3,tmpT4});
                    else if (tmpS == "ndf") mNDF .push_back(MEras{tmpT1,tmpT2,tmpT3,tmpT4});
                    else if (tmpS == "fer") mFErr.push_back(MEras{tmpT1,tmpT2,tmpT3,tmpT4});
                }
                if (!initKJES) {
                    if (nKJESs!=mKJES.size() or mRefKJES==1000) return false;
                    initKJES = true;
                }
            }
        }
        fs.close();
        if (mTIdx.size()+mWIdx.size()!=nHists) return false;
        if (mNames.size()!=nHists) return false;
        if (mMFit .size()!=nHists*nKJESs) return false;
        if (mMAve .size()!=nHists*nKJESs) return false;
        if (mMItg .size()!=nHists*nKJESs) return false;
        if (mMMax .size()!=nHists*nKJESs) return false;
        if (mMMed .size()!=nHists*nKJESs) return false;
        if (mChi2 .size()!=nHists*nKJESs) return false;
        if (mNDF  .size()!=nHists*nKJESs) return false;
        if (mFErr .size()!=nHists*nKJESs) return false;

        cerr << "Parsed successfully " << nHists << " histograms with " << nInfos << " pieces of information." << endl;

        mWHadr   = WIdx("wmass_hadr_corr_");
        mWHadrNu = mDoNu ? WIdx("wmass_hadrnu_corr_") : mWHadr;
        if (mWHadrNu<0 or mWHadrNu<0) {
            cerr << "W histogram not found!" << endl;
            return false;
        }

        return true;
    }

    pair<double,double> AveSigma(MTriplet ms) {
        double ave = (ms.v1725+ms.v1737+ms.v1750)/3.0;
        double s2  = (pow(ms.v1725-ave,2)+pow(ms.v1737-ave,2)+pow(ms.v1750-ave,2))/(3.0*2.0);
        return std::make_pair(ave,TMath::Sqrt(s2));
    }

    inline void AddW(vector<pair<double,double>> &W, MEras &me) {
        W.emplace_back(AveSigma(me.a  ));
        W.emplace_back(AveSigma(me.b1 ));
        W.emplace_back(AveSigma(me.b2 ));
        W.emplace_back(AveSigma(me.b34));
    }

    inline void AddW2Mass(vector<double> &W2, MTriplet ms) {
        W2.emplace_back(ms.v1725);
        W2.emplace_back(ms.v1737);
        W2.emplace_back(ms.v1750);
    }

    inline void AddW2(vector<double> &W2, MEras &mes) {
        AddW2Mass(W2, mes.a  );
        AddW2Mass(W2, mes.b1 );
        AddW2Mass(W2, mes.b2 );
        AddW2Mass(W2, mes.b34);
    }

    bool WProc() {
        for (unsigned widx = 0; widx < mWIdx.size(); ++widx) {
            auto &idx = mWIdx[widx];
            for (unsigned kidx = 0; kidx < mKJES.size(); ++kidx) {
                int totidx = idx*mKJES.size()+kidx;
                AddW(mWFit,mMFit[totidx]);
                AddW(mWAve,mMAve[totidx]);
                AddW(mWItg,mMItg[totidx]);
                AddW(mWMax,mMMax[totidx]);
                AddW(mWMed,mMMed[totidx]);
                AddW2(mWFit2,mMFit[totidx]);
                AddW2(mWAve2,mMAve[totidx]);
                AddW2(mWItg2,mMItg[totidx]);
                AddW2(mWMax2,mMMax[totidx]);
                AddW2(mWMed2,mMMed[totidx]);
            }
        }

        if (mWFit.size() != 4*mWIdx.size()*mKJES.size()) return false;
        if (mWAve.size() != 4*mWIdx.size()*mKJES.size()) return false;
        if (mWItg.size() != 4*mWIdx.size()*mKJES.size()) return false;
        if (mWMax.size() != 4*mWIdx.size()*mKJES.size()) return false;
        if (mWMed.size() != 4*mWIdx.size()*mKJES.size()) return false;
        if (mWFit2.size() != 3*4*mWIdx.size()*mKJES.size()) return false;
        if (mWAve2.size() != 3*4*mWIdx.size()*mKJES.size()) return false;
        if (mWItg2.size() != 3*4*mWIdx.size()*mKJES.size()) return false;
        if (mWMax2.size() != 3*4*mWIdx.size()*mKJES.size()) return false;
        if (mWMed2.size() != 3*4*mWIdx.size()*mKJES.size()) return false;

        cerr << "Inspected successfully 4x" << mWIdx.size() << "x" << mKJES.size() << " W mass histograms." << endl;
        return true;
    }

    inline void Add2DFit(vector<Fitter2D<5,3>> &mtv,vector<double> &va, vector<double> &vb1, vector<double> &vb2, vector<double> &vb34, vector<double> &mgen) {
        if (va.size()!=15 or vb1.size()!=15 or vb2.size()!=15 or vb34.size()!=15) {
            cerr << "Problems in fitting!" << endl;
        }
        mtv.emplace_back(va  ,mKJES,mgen);
        mtv.emplace_back(vb1 ,mKJES,mgen);
        mtv.emplace_back(vb2 ,mKJES,mgen);
        mtv.emplace_back(vb34,mKJES,mgen);
    }

    inline void AppendMass(vector<double> &v, MTriplet &m, bool scale = false) {
        v.push_back(m.v1725/(scale ? 80.4 : 1.0));
        v.push_back(m.v1737/(scale ? 80.4 : 1.0));
        v.push_back(m.v1750/(scale ? 80.4 : 1.0));
    }

    inline void AppendEras(vector<double> &va, vector<double> &vb1, vector<double> &vb2, vector<double> &vb34, MEras &m, bool scale = false) {
        AppendMass(va  ,m.a  , scale);
        AppendMass(vb1 ,m.b1 , scale);
        AppendMass(vb2 ,m.b2 , scale);
        AppendMass(vb34,m.b34, scale);
    }

    void FitKJES(vector<Fitter1D<5>> &kfit, vector<Fitter1D<5,3>> &kfit2, int mode, bool nu = false) {
        for (unsigned eidx = 0; eidx < 4; ++eidx) {
            vector<double> karray, kerrs, karray2;
            for (unsigned kidx = 0; kidx < mKJES.size(); ++kidx) {
                if (mode==0) {
                    karray.push_back(KJESExpFit   (nu,eidx,kidx));
                    kerrs .push_back(KJESExpErrFit(nu,eidx,kidx));
                    for (int midx = 0; midx < 3; ++midx) karray2.push_back(KJESExpFit(nu,eidx,kidx,midx));
                } else if (mode==1) {
                    karray.push_back(KJESExpAve   (nu,eidx,kidx));
                    kerrs .push_back(KJESExpErrAve(nu,eidx,kidx));
                    for (int midx = 0; midx < 3; ++midx) karray2.push_back(KJESExpAve(nu,eidx,kidx,midx));
                } else if (mode==2) {
                    karray.push_back(KJESExpItg   (nu,eidx,kidx));
                    kerrs .push_back(KJESExpErrItg(nu,eidx,kidx));
                    for (int midx = 0; midx < 3; ++midx) karray2.push_back(KJESExpItg(nu,eidx,kidx,midx));
                } else if (mode==3) {
                    karray.push_back(KJESExpMax   (nu,eidx,kidx));
                    kerrs .push_back(KJESExpErrMax(nu,eidx,kidx));
                    for (int midx = 0; midx < 3; ++midx) karray2.push_back(KJESExpMax(nu,eidx,kidx,midx));
                } else if (mode==4) {
                    karray.push_back(KJESExpMed   (nu,eidx,kidx));
                    kerrs .push_back(KJESExpErrMed(nu,eidx,kidx));
                    for (int midx = 0; midx < 3; ++midx) karray2.push_back(KJESExpMed(nu,eidx,kidx,midx));
                }
            }
            if (karray.size()!=mKJES.size() or kerrs.size()!=mKJES.size() or karray2.size()!=3*mKJES.size()) cerr << "Something's wrong in KJES!" << endl;
            kfit.emplace_back(karray,mKJES,kerrs);
            kfit2.emplace_back(karray2,mKJES);
        }
    }

    bool Fit() {
        vector<double> mgen{172.5,173.7,175.0};
        for (unsigned tidx = 0; tidx < mTIdx.size(); ++tidx) {
            auto &idx = mTIdx[tidx];

            for (unsigned mode = 0; mode < 5; ++mode) {
                vector<double> va,vb1,vb2,vb34;
                for (unsigned kidx = 0; kidx < mKJES.size(); ++kidx) {
                    int totidx = idx*mKJES.size()+kidx;
                    if      (mode==0) AppendEras(va,vb1,vb2,vb34,mMFit[totidx]);
                    else if (mode==1) AppendEras(va,vb1,vb2,vb34,mMAve[totidx]);
                    else if (mode==2) AppendEras(va,vb1,vb2,vb34,mMItg[totidx]);
                    else if (mode==3) AppendEras(va,vb1,vb2,vb34,mMMax[totidx]);
                    else if (mode==4) AppendEras(va,vb1,vb2,vb34,mMMed[totidx]);
                }
                if      (mode==0) Add2DFit(mTKJESFit,va,vb1,vb2,vb34,mgen);
                else if (mode==1) Add2DFit(mTKJESAve,va,vb1,vb2,vb34,mgen);
                else if (mode==2) Add2DFit(mTKJESItg,va,vb1,vb2,vb34,mgen);
                else if (mode==3) Add2DFit(mTKJESMax,va,vb1,vb2,vb34,mgen);
                else if (mode==4) Add2DFit(mTKJESMed,va,vb1,vb2,vb34,mgen);
                else cerr << "Unknown mode!" << endl;
            }
            cout << mNames[idx] << endl;
        }

        if (mTKJESFit.size() != 4*mTIdx.size()) return false;
        if (mTKJESAve.size() != 4*mTIdx.size()) return false;
        if (mTKJESItg.size() != 4*mTIdx.size()) return false;
        if (mTKJESMax.size() != 4*mTIdx.size()) return false;
        if (mTKJESMed.size() != 4*mTIdx.size()) return false;

        cerr << "Fitted successfully 4x" << mTIdx.size() << " top mass histogram sets." << endl;

        /* Making 1D fits for the W resonances. */
        FitKJES(mKJESFit,mKJESFit2,0);
        FitKJES(mKJESAve,mKJESAve2,1);
        FitKJES(mKJESItg,mKJESItg2,2);
        FitKJES(mKJESMax,mKJESMax2,3);
        FitKJES(mKJESMed,mKJESMed2,4);
        if (mDoNu) {
            FitKJES(mKJESNuFit,mKJESNuFit2,0,true);
            FitKJES(mKJESNuAve,mKJESNuAve2,1,true);
            FitKJES(mKJESNuItg,mKJESNuItg2,2,true);
            FitKJES(mKJESNuMax,mKJESNuMax2,3,true);
            FitKJES(mKJESNuMed,mKJESNuMed2,4,true);
        }

        return true;
    }
};
