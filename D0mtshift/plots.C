#include "TopPlots.h"

R__LOAD_LIBRARY(TopPlots_C)
//R__LOAD_LIBRARY(TopPlots.C+)

void helper(string baseloc, string tag) {
    string loc = baseloc + "/" + tag;
    vector<string> masstags = {"1725","1737","1750"};
    vector<double> masses   = {172.5,173.7,175};

    vector<TopPlots> store;
    string tname = "Pythia6Jets";
    for (unsigned i = 0; i < masses.size(); ++i) {
        auto &mtag = masstags[i];
        auto &mass = masses[i];
        string fname = (tag=="." ? Form("results_%s.root",mtag.c_str()) : Form("results_%s_%s.root",mtag.c_str(),tag.c_str()));
        store.emplace_back(baseloc,tag,fname,mass);
        store.back().Process();
    }

    int noHist = store.back().NumHistos();
    int noSngl = store.back().NumSngls();
    int noMass = store.back().NumMasses();
    int noKJES = store.back().NumKJES();

    std::ofstream erafile(Form("%s/erainfo.txt",loc.c_str()),std::ofstream::out);
    erafile << std::setprecision(8) << std::fixed;
    erafile << noHist << " era histograms, " << noMass << " pieces of information and " << noKJES << " JES values." << endl;
    for (int hidx = 0; hidx < noHist; ++hidx) {
        erafile << hidx << " " << store.back().HistName(hidx) << endl;
        for (int midx = 0; midx < noMass; ++midx) {
            erafile << store.back().MassName(midx) << endl;
            for (int kidx = 0; kidx < noKJES; ++kidx) {
                erafile << store.back().KJESName(kidx) << endl;
                for (auto &handle : store) {
                    erafile << std::setprecision(1) << std::fixed << handle.MassVal() << " ";
                    handle.MassInfo(hidx,midx,kidx,erafile);
                    erafile << endl;
                }
            }
        }
    }
    erafile.close();
    std::ofstream sngfile(Form("%s/snginfo.txt",loc.c_str()),std::ofstream::out);
    sngfile << std::setprecision(8) << std::fixed;
    sngfile << noSngl << " singular histograms and " << noMass << " pieces of information!" << endl;
    for (int sidx = 0; sidx < noSngl; ++sidx) {
        sngfile << sidx << " " << store.back().SnglName(sidx) << endl;
        for (int midx = 0; midx < noMass; ++midx) {
            sngfile << store.back().MassName(midx) << endl;
            for (auto &handle : store) {
                sngfile << std::setprecision(1) << std::fixed << handle.MassVal() << " ";
                handle.SnglInfo(sidx,midx,sngfile);
                sngfile << endl;
            }
        }
    }
    sngfile.close();
}

void plots() {
    string loc = "non2v1";

    //helper(loc,"El");
    helper(loc,"Mu");
    //helper(loc,".");
}
