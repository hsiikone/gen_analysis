void Visual() {
  TH1D *base = new TH1D("h1","",250,0.,2.5);
  TH1D *bose = new TH1D("h2","",250,0.,2.5);
  TH1D *bise = new TH1D("h3","",500,0.,5.0);

  TRandom rando;
  const double SF = 1.1;
  const double sigma = 2.5;
  const double sigma2 = sigma * sqrt(SF*SF - 1);
  for (int i = 0; i < 1000000; ++i) {
    double dPt = 4*sigma;
    // while (std::abs(dPt) > 3*sigma)
      dPt = rando.Gaus(0, sigma);
    double dPt2 = 4*sigma2;
    // while (std::abs(dPt2) > 3*sigma2)
      dPt2 = rando.Gaus(0, sigma2);

    base->Fill(TMath::Exp(pow(dPt/sigma,2)*(1 - pow(SF,-2))/2.)/SF);
    bose->Fill(TMath::Exp(pow(sigma,-2)*(2*dPt*dPt2/SF - pow(dPt2/SF,2))/2.)/SF);
    // bise->Fill(TMath::Exp(pow(sigma,-2)*dPt*dPt2/SF)/SF);
    // bise->Fill(TMath::Exp(-pow(dPt/(SF*sigma),2)/2. + pow(dPt2/(sigma/SF),2)/2.)/SF);
    bise->Fill(TMath::Exp(pow(dPt/(sigma),2)*(pow(SF,2)-1)/2. - dPt*dPt2*pow(sigma,-2)*(SF-1./SF) + pow(dPt2/sigma,2)*(1-pow(SF,-2))/2.)/SF);
  }

  TCanvas *c1 = new TCanvas("c", "c", 800, 600);
  c1->cd();
  base->Draw();
  c1->Print("base.pdf");
  bose->Draw();
  c1->Print("var.pdf");
  bise->Draw();
  c1->Print("varvar.pdf");
}