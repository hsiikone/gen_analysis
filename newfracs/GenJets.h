//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Apr  4 11:30:33 2022 by ROOT version 6.22/02
// from TTree GenJets/GenJets
// found on file: Jets.root
//////////////////////////////////////////////////////////

#ifndef GenJets_h
#define GenJets_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

#include <string>
#include <iostream>

// Header file for the classes stored in the TTree if any.

class GenJets {
public :
  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

  // Declaration of leaf types
  Float_t         weight;
  Float_t         Pt;
  Float_t         Eta;
  Float_t         Phi;
  Float_t         E;
  Int_t           JIdx;
  Int_t           Status;
  Float_t         EFracs[19];

  GenJets(std::string fName = "");
  virtual ~GenJets();
  virtual Int_t    GetEntry(Long64_t entry);
  virtual Long64_t LoadTree(Long64_t entry);
  virtual void     Init(TTree *tree);
  virtual void     Loop();
};

#endif

#ifdef GenJets_cxx
GenJets::GenJets(std::string fName) : fChain(0) 
{
  TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(fName.c_str());
  if (!f || !f->IsOpen()) {
    f = new TFile(fName.c_str());
  }
  TDirectory * dir = (TDirectory*)f->Get(Form("%s:/jets", fName.c_str()));
  TTree *tree = nullptr;
  dir->GetObject("GenJets",tree);
  Init(tree);
}

GenJets::~GenJets()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t GenJets::GetEntry(Long64_t entry)
{
// Read contents of entry.
  if (!fChain) return 0;
  return fChain->GetEntry(entry);
}
Long64_t GenJets::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (fChain->GetTreeNumber() != fCurrent) {
    fCurrent = fChain->GetTreeNumber();
  }
  return centry;
}

void GenJets::Init(TTree *tree)
{
  // The Init() function is called when the selector needs to initialize
  // a new tree or chain. Typically here the branch addresses and branch
  // pointers of the tree will be set.
  // It is normally not necessary to make changes to the generated
  // code, but the routine can be extended by the user if needed.
  // Init() will be called many times when running on PROOF
  // (once per file to be processed).

  // Set branch addresses and branch pointers
  if (!tree) return;
  fChain = tree;
  fCurrent = -1;
  fChain->SetMakeClass(1);

  fChain->SetBranchAddress("weight", &weight);
  fChain->SetBranchAddress("Pt", &Pt);
  fChain->SetBranchAddress("Eta", &Eta);
  fChain->SetBranchAddress("Phi", &Phi);
  fChain->SetBranchAddress("E", &E);
  fChain->SetBranchAddress("JIdx", &JIdx);
  fChain->SetBranchAddress("Status", &Status);
  fChain->SetBranchAddress("EFracs", EFracs);
}

#endif // #ifdef GenJets_cxx
