//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon May 25 14:17:28 2020 by ROOT version 6.20/04
// from TTree ProcessedTree/ProcessedTree
// found on file: MCHadrons.root
//////////////////////////////////////////////////////////

#ifndef Hadrons_h
#define Hadrons_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class Hadrons {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Float_t         Pt;
   Float_t         Eta;
   Float_t         Phi;
   Float_t         E;
   Float_t         Ef_ECALRaw;
   Float_t         Ef_ECAL;
   Float_t         Ef_HCALRaw;
   Float_t         Ef_HCAL;

   // List of branches
   TBranch        *b_Pt;   //!
   TBranch        *b_Eta;   //!
   TBranch        *b_Phi;   //!
   TBranch        *b_E;   //!
   TBranch        *b_Ef_ECALRaw;   //!
   TBranch        *b_Ef_ECAL;   //!
   TBranch        *b_Ef_HCALRaw;   //!
   TBranch        *b_Ef_HCAL;   //!

   Hadrons(TTree *tree=0);
   virtual ~Hadrons();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef Hadrons_cxx
Hadrons::Hadrons(TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("MCHadrons.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("EHadrons.root");
         //f = new TFile("CHadrons.root");
         //f = new TFile("MCHadrons.root");
      }
      TDirectory * dir = (TDirectory*)f->Get("EHadrons.root:/ak4");
      //TDirectory * dir = (TDirectory*)f->Get("CHadrons.root:/ak4");
      //TDirectory * dir = (TDirectory*)f->Get("MCHadrons.root:/ak4");
      dir->GetObject("ProcessedTree",tree);

   }
   Init(tree);
}

Hadrons::~Hadrons()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Hadrons::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Hadrons::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void Hadrons::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("Pt", &Pt, &b_Pt);
   fChain->SetBranchAddress("Eta", &Eta, &b_Eta);
   fChain->SetBranchAddress("Phi", &Phi, &b_Phi);
   fChain->SetBranchAddress("E", &E, &b_E);
   fChain->SetBranchAddress("Ef_ECALRaw", &Ef_ECALRaw, &b_Ef_ECALRaw);
   fChain->SetBranchAddress("Ef_ECAL", &Ef_ECAL, &b_Ef_ECAL);
   fChain->SetBranchAddress("Ef_HCALRaw", &Ef_HCALRaw, &b_Ef_HCALRaw);
   fChain->SetBranchAddress("Ef_HCAL", &Ef_HCAL, &b_Ef_HCAL);
   Notify();
}

Bool_t Hadrons::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void Hadrons::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Hadrons::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef Hadrons_cxx
