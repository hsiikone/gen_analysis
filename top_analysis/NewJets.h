//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Fri Sep  1 11:06:22 2017 by ROOT version 6.08/06
// from TTree NewJets/Pythia8 particle data.
// found on file: pjets_pythia8_powheg_ttbar.root
//////////////////////////////////////////////////////////

#ifdef DEBUG
#undef DEBUG
#endif
//#define DEBUG

#ifndef NewJets_h
#define NewJets_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TH2.h>
#include <TProfile.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>
#include <TMath.h>
#include <TGraph.h>
#include <TLine.h>
#include <TText.h>

// Header file for the classes stored in the TTree if any.
#include <TH2.h>
#include <TF1.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <TLorentzVector.h>

#include <string>
#include <algorithm>
#include <iostream>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <iomanip>

#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#include "Math/IFunction.h"

#include "tdrstyle_mod15.C"

using std::endl;
using std::cout;
using std::vector;
using std::cerr;
using std::map;
using std::to_string;

struct Tag {
    int flav;
    int lvl;
    float ratio;
    float dr;
    int idx;
    int fIdx;
};

struct Dtr {
    int jetI;
    int id;
    Tag tag;
};

class Neutrino {
public:
    Neutrino() : mInit(false), mMW(80.4) {}
    Neutrino(const TLorentzVector &l, const TLorentzVector &m, const TLorentzVector &n);

    int Quality1() const { return mInit ? mQual1 : -2; }
    int Quality2() const { return mInit ? mQual2 : -2; }
    const TLorentzVector &Nu1() const { return mNu1; }
    const TLorentzVector &Nu2() const { return mNu2; }
    double PtR1() const { return mNu1.Pt()/mL.Pt(); }
    double PtR2() const { return mNu2.Pt()/mL.Pt(); }
    double PR1() const { return mNu1.P()/mL.P(); }
    double PR2() const { return mNu2.P()/mL.P(); }
    double Base() const { return mBase; }
    double Det() const { return mPosDet ? mDet : -mDet; }
private:
    TLorentzVector mL;
    TLorentzVector mM;
    TLorentzVector mNu;
    TLorentzVector mNu1;
    TLorentzVector mNu2;

    int mQual1;
    int mQual2;

    bool mInit;
    bool mPosDet;

    const double mMW;
    double mBase;
    double mDet;
};

class KinFit {
public:
    KinFit() : mInit(false), mPrint(false), mLikeli0(0.0), mLikeli(0.0), mMW(80.4), mGW(2.0), mGT(1.35) {}
    KinFit(const TLorentzVector &bHadr, const TLorentzVector &bLept, const TLorentzVector &j1, const TLorentzVector &j2, const TLorentzVector &lept, const TLorentzVector &neutr);
    bool Fit(bool print = false, int mode = 0);
    double GetLogLikelihood() { return mLikeli; }
    double GetPreLogLikelihood() {return mLikeli0; }
    double GetTotLogLikelihood() { return mLikeli1; }
    double GetChi2() { return mLikeli2; }
    double GetNeutrino() { return mNu.Pz(); }
    double GetNuDiff() { return mNuf.Pz()-mNu.Pz(); }

    double NegLikeli(const double *xx);
    double NegLikeliLagr(const double *xx);
    double mDet;

    // Pre- and after fit values
    TLorentzVector mBH;
    TLorentzVector mBHf;
    TLorentzVector mBL;
    TLorentzVector mBLf;
    TLorentzVector mJ1;
    TLorentzVector mJ1f;
    TLorentzVector mJ2;
    TLorentzVector mJ2f;
    TLorentzVector mL;
    TLorentzVector mNu;
    TLorentzVector mNuf;

    TLorentzVector mWHf;
    TLorentzVector mWLf;
    TLorentzVector mTHf;
    TLorentzVector mTLf;
    TLorentzVector mTot;
    TLorentzVector mTotf;
private:
    double EvalLikeli(double mwh, double mwl, double mth, double mtl);

    bool mInit;
    bool mPrint;
    double mLikeli;
    double mLikeli0;
    double mLikeli1;
    double mLikeli2;
    const double mMW;
    const double mGW;
    const double mGT;

    double mBHM;
    double mBLM;
    double mJ1M;
    double mJ2M;

    double mKtS;
    double mPhiS;
    double mBHPtS;
    double mBHEtaS;
    double mBHPhiS;
    double mBLPtS;
    double mBLEtaS;
    double mBLPhiS;
    double mJ1PtS;
    double mJ1EtaS;
    double mJ1PhiS;
    double mJ2PtS;
    double mJ2EtaS;
    double mJ2PhiS;

    double mBase;

    TLorentzVector mDNu;
    TLorentzVector mDW1;
    TLorentzVector mDW2;
};

class NewJets {
public :
    static constexpr float    LeptPt = 30;
    static constexpr float    LeptEta = 2.1;
    static constexpr float    JetPt = 30;
    static constexpr float    MinPt = 15;
    static constexpr float    JetEta = 2.4;
    static constexpr float    MaxEta = 5.0;
    unsigned mABins;
    unsigned mPtBins;
    vector<Float_t> mARange;
    vector<Double_t> mPtRange;

    vector<pair<string,float>> mATitles;
    vector<pair<string,float>> mMwTitles;
    vector<pair<string,float>> mMtTitles;

    TTree          *fChain;
    Int_t           fCurrent;

    // Declaration of leaf types
    Char_t          info;
    Char_t          bnucount;
    Char_t          nub;
    Char_t          nuc;
    Char_t          nulept;
    Char_t          nuother;
    Float_t         weight;
    Float_t         pthat;
    vector<float>   *isolation;
    vector<unsigned char> *prtcl_jet;
    vector<int>     *prtcl_pdgid;
    vector<float>   *prtcl_pt;
    vector<float>   *prtcl_eta;
    vector<float>   *prtcl_phi;
    vector<float>   *prtcl_e;
    vector<char>    *prtn_jet;
    vector<int>     *prtn_ptn;
    vector<int>     *prtn_own;
    vector<int>     *prtn_pdgid;
    vector<char>    *prtn_tag;
    vector<float>   *prtn_pt;
    vector<float>   *prtn_eta;
    vector<float>   *prtn_phi;
    vector<float>   *prtn_e;
    vector<float>   *prtn_dr;
    vector<float>   *jet_pt;
    vector<float>   *jet_eta;
    vector<float>   *jet_phi;
    vector<float>   *jet_e;
    vector<int>     *jet_ptn;
    vector<int>     *jet_constituents;
    vector<float>   *jet_ptd;
    vector<float>   *jet_sigma2;
    Float_t         met;
    int             mWSign;
    int             mNoWFollow;
    int             mRealLept;

    NewJets(string fname="pjets_pythia8_amcnlo.root", string tname="NewJets", TTree *tree=0);
    virtual ~NewJets();
    virtual Int_t    GetEntry(Long64_t entry);
    virtual Long64_t LoadTree(Long64_t entry);
    virtual void     Init(TTree *tree);
    virtual void     Loop(Long64_t = -1, bool drawMode=true);

    template<typename Out>
    void split(const std::string &s, char delim, Out result);

    std::vector<std::string> split(const std::string &s, char delim);
    bool checker(float frac, float met);

    void AddMessage(string msg,double wgt);
    bool Test(string msg,bool test);
    bool Problem(string msg,bool test);
    template<class T>
    void Drawer(T *h, bool stats = true, bool logY = false);
    void Filler(float alpha, float pt, float ratio, vector<std::pair<string,float>> &titles, float num = 0, float lowlim = 0.0,  float hilim = 10000.0);

    vector<int> OptimalMass(const TLorentzVector &lepton, const TLorentzVector &MET, const TLorentzVector &neutrinos, const vector<int> &truth, bool jets4, bool jetsG, unsigned jetlim, double &nuz);

    void TagTree(int id, int lvl, int fIdx, int info = 0);
    void Descend(int id,  int lvl,  int fIdx,  int info = 0);
    void Eliminate();
    void DElim(int id);
    void MElim(int id);
    void LeftoverTree(int id, int lvl, int fIdx, int info = 0);
    vector<Dtr> mCandidates;

    vector<int>              mBParts;
    vector<int>              mWParts;
    vector<int>              mGammas;
    map<int,Tag>             mTmpTags;
    vector<int>              mLeaders;
    map<int, vector<int> >   mFollowers;
    vector<int>              mWFollowers;
    map<int,int>             mTagName;
    map<string,double>       mErrorList;
    vector<string>           mInsertions;
    vector<std::pair<TLorentzVector,bool> >   mJets;
    vector<int>              mBJets;
    map<string, TH1D*>       mHistos;
    map<string, TH2D*>       m2DHistos;
    map<string, TProfile*>   mProfs;
    map<string, double>      mFitLim;

    TF1* f1;
    TF1* f2;
    TF1* f3;
    TF1* f4;
    TF1* f5;

    Long64_t mJentry;
};
#endif

#ifdef NewJets_cxx
NewJets::NewJets(string fname, string tname, TTree *tree) : fChain(0)
{
    if (tree == 0) {
        TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject(fname.c_str());
        if (!f || !f->IsOpen()) {
            f = new TFile(fname.c_str());
        }
        f->GetObject(tname.c_str(),tree);
    }
    Init(tree);
    mARange = {0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4};
    mABins = mARange.size()-1;
    Float_t* aRange = &mARange[0];
    mPtRange =
    //{18, 21, 24,·
    {28, 32, 37, 43, 49,
     56, 64, 74, 84,
     97, 114, 133, 153, 174, 196, 220, 245, 272, 300, 330, 362, 395, 430, 468,
     507, 548, 592, 638, 686, 737, 790, 846, 905, 967,
     1032, 1101, 1172, 1248, 1327, 1410, 1497, 1588, 1684, 1784, 1890, 2000,
     2116, 2238, 2366, 2500, 2640, 2787, 2941, 3103, 3273, 3450,
     3637, 3832, 4037};
     mPtBins = mPtRange.size()-1;
     Double_t* PtRange = &mPtRange[0];

     // Pt range and bins for alpha studies
    Int_t ptBins = 40;
    Double_t ptRange[] = {40,60,80,100,120,140,160,180,200,220,240,260,280,300,320,340,360,380,400,420,440,460,480,500,520,540,560,580,600,620,640,660,680,700,720,740,760,780,800,820,840};
    Int_t ptAlts = 8;
    Int_t ptBlts = 5;
    Double_t ptRangeA[] = {30,36,43,51,60,70,85,110,150};
    Double_t ptRangeB[] = {10,20,30,42,56,72};

    // Ordering of the stuff, important to keep in the correct order!
    mInsertions.push_back("All");
    mInsertions.push_back("Fake L");
    mInsertions.push_back("Mult L");

    mInsertions.push_back("L kin");
    mInsertions.push_back("L isol");
    mInsertions.push_back("3J Pt");
    mInsertions.push_back("4J Pt");
    mInsertions.push_back("5J Pt");
    mInsertions.push_back("5J No");
    mInsertions.push_back("J Eta");
    mInsertions.push_back("B 2hP");
    mInsertions.push_back("B 2hM");
    mInsertions.push_back("B ftag");
    mInsertions.push_back("B In4");
    mInsertions.push_back("No match");

    mInsertions.push_back("B pmiss");
    mInsertions.push_back("B ttag");
    mInsertions.push_back("J Fail");
    mInsertions.push_back("MET");
    mInsertions.push_back("PTR");

    m2DHistos["BLept"] = new TH2D("blepton","blepton", mPtBins, PtRange,500,-5,5);
    m2DHistos["BHadr"] = new TH2D("bhadron","bhadron", mPtBins, PtRange,500,-5,5);
    m2DHistos["WJet"] = new TH2D("wjets","wjets", mPtBins, PtRange,500,-5,5);

    mHistos["PtHat"] = new TH1D("pthat","pthat",200,0,500);
    mHistos["Isol"] = new TH1D("isol","isol",500,0,10);
    mHistos["NoJets"] = new TH1D("jets_no","jetcount",20,0,20);
    mHistos["JetC"] = new TH1D("jetcount","goodjetcount",15,0,15);
    mHistos["fBtags"] = new TH1D("fbtags","btags",5,0,5);

    mHistos["Gammas"] = new TH1D("gammas","gammas",10,0,10);
    mHistos["HMet"] = new TH1D("truemet","truemet",200,0,500);
    mHistos["BNeutr"] = new TH1D("bneutris","bneutris",10,0,10);
    mHistos["ONeutr"] = new TH1D("othneutris","othneutris",10,0,10);
    mHistos["BMetf"] = new TH1D("bneutrimetfrac","bneutrimetfrac",40,0,1);
    mHistos["OMetf"] = new TH1D("othneutrimetfrac","othneutrimetfrac",40,0,1);
    mHistos["Metf"] = new TH1D("metleftoverfrac","metleftoverfrac",40,0,1);
    mHistos["MetAdd"] = new TH1D("metadditional","metadditional",50,0,100);
    mHistos["MetLeft"] = new TH1D("metinterest","metinterest",50,0,500);
    mHistos["MetLept"] = new TH1D("metperlept","metperlept",50,0,2);
    mHistos["CMetLept"] = new TH1D("neutriperlept","neutriperlept",50,0,2);

    mHistos["fAss"] = new TH1D("fass","parton0 ass no",15,0,15);
    mHistos["nfAss"] = new TH1D("nfass","parton1 ass no",15,0,15);
    mHistos["fDR"] = new TH1D("fdr","parton0 dist",100,0,2.5);
    mHistos["nfDR"] = new TH1D("nfdr","parton1 dist",100,0,2.5);
    mHistos["bDR"] = new TH1D("bdr","bj dist",100,0,2.5);
    mHistos["wDR"] = new TH1D("wdr","wj dist",100,0,2.5);
    mHistos["fPtR"] = new TH1D("fptr","parton0 ratio",50,-1,1);
    mHistos["nfPtR"] = new TH1D("nfptr","parton1 ratio",50,-1,1);
    mHistos["bPtR"] = new TH1D("bptr","bparton ratio",50,-1,1);

    unsigned tbins = 500;
    double tmin = 100;
    double tmax = 250;
    mHistos["ExpTMass"] = new TH1D("tmass_ejets","tmass e",tbins,tmin,tmax);
    mHistos["ExpTMassGoodW"] = new TH1D("tmass_ejets_goodw","tmass egoodw",tbins,tmin,tmax);
    mHistos["ExpTMassBadW"] = new TH1D("tmass_ejets_badw","tmass ebadw",tbins,tmin,tmax);
    mHistos["ExpTMassFine"] = new TH1D("tmass_ejets_fine","tmass efine",tbins,tmin,tmax);
    mHistos["ExpTMassModerate"] = new TH1D("tmass_ejets_moderate","tmass emoderate",tbins,tmin,tmax);
    mHistos["ExpTMassFjals"] = new TH1D("tmass_ejets_rough","tmass erough",tbins,tmin,tmax);
    mHistos["ExpTMassWggBg"] = new TH1D("tmass_ejets_wggbg","tmass ewggbg",tbins,tmin,tmax);
    mHistos["ExpTMassWggBb"] = new TH1D("tmass_ejets_wggbb","tmass ewggbb",tbins,tmin,tmax);
    mHistos["ExpTMassWgbBg"] = new TH1D("tmass_ejets_wgbbg","tmass ewgbbg",tbins,tmin,tmax);
    mHistos["ExpTMassWgbBb"] = new TH1D("tmass_ejets_wgbbb","tmass ewgbbb",tbins,tmin,tmax);
    mHistos["ExpTMassWbbBg"] = new TH1D("tmass_ejets_wbbbg","tmass ewbbbg",tbins,tmin,tmax);
    mHistos["ExpTMassWbbBb"] = new TH1D("tmass_ejets_wbbbb","tmass ewbbbb",tbins,tmin,tmax);
    mHistos["TMassMet"] = new TH1D("tmass_met","tmass met",tbins,tmin,tmax);
    mHistos["TMassMNet"] = new TH1D("tmass_metneutri","tmass metneutri",tbins,tmin,tmax);
    mHistos["TMassNet"] = new TH1D("tmass_neutri","tmass neutri",tbins,tmin,tmax);
    mHistos["TMassParton"] = new TH1D("tmass_parton","tmass parton",tbins,tmin,tmax);
    mHistos["TMassRecoJ"] = new TH1D("tmass_recoj","tmass reco j",tbins,tmin,tmax);
    mHistos["TMassRecoNuJ"] = new TH1D("tmass_reconuj","tmass reco nuj",tbins,tmin,tmax);
    mHistos["TMassRecoNuLept"] = new TH1D("tmass_reconul","tmass reco nul",tbins,tmin,tmax);
    mHistos["TMassRecoNuGLept"] = new TH1D("tmass_reconugl","tmass reconugl",tbins,tmin,tmax);
    mHistos["TMassRecoMean"] = new TH1D("tmass_recomean","tmass reco mean",tbins,tmin,tmax);
    mHistos["TMassRecoDiff"] = new TH1D("tmass_recodiff","tmass recodiff",tbins,-100,100);
    mHistos["TMassRecoNuGDiff"] = new TH1D("tmass_reconugdiff","tmass reconugdiff",tbins,-100,100);
    mHistos["T3JMass"] = new TH1D("tmass_3j","tmass 3j",tbins,tmin,tmax);
    mHistos["T3JFake"] = new TH1D("tmass_3jfake","tmass 3j fake",tbins,tmin,tmax);
    mHistos["T2JMass"] = new TH1D("tmass_2j","tmass 2j",tbins,tmin,tmax);

    mHistos["TMassNetGood"] = new TH1D("tmass_neutri_gd","tmass neutri good",tbins,tmin,tmax);
    mHistos["TMassRecoJGood"] = new TH1D("tmass_recoj_gd","tmass recoj good",tbins,tmin,tmax);

    mHistos["mt05"] = new TH1D("tmass_05","tmass 05",tbins,tmin,tmax);
    mHistos["mt10"] = new TH1D("tmass_10","tmass 10",tbins,tmin,tmax);
    mHistos["mt15"] = new TH1D("tmass_15","tmass 15",tbins,tmin,tmax);
    mHistos["mt20"] = new TH1D("tmass_20","tmass 20",tbins,tmin,tmax);
    mHistos["mt25"] = new TH1D("tmass_25","tmass 25",tbins,tmin,tmax);
    mHistos["mt30"] = new TH1D("tmass_30","tmass 30",tbins,tmin,tmax);
    mHistos["mt35"] = new TH1D("tmass_35","tmass 35",tbins,tmin,tmax);
    mHistos["mt40"] = new TH1D("tmass_40","tmass 40",tbins,tmin,tmax);

    unsigned wbins = 500;
    double wmin = 0;
    double wmax = 150;
    mHistos["mw05"] = new TH1D("wmass_05","wmass 05",wbins,wmin,wmax);
    mHistos["mw10"] = new TH1D("wmass_10","wmass 10",wbins,wmin,wmax);
    mHistos["mw15"] = new TH1D("wmass_15","wmass 15",wbins,wmin,wmax);
    mHistos["mw20"] = new TH1D("wmass_20","wmass 20",wbins,wmin,wmax);
    mHistos["mw25"] = new TH1D("wmass_25","wmass 25",wbins,wmin,wmax);
    mHistos["mw30"] = new TH1D("wmass_30","wmass 30",wbins,wmin,wmax);
    mHistos["mw35"] = new TH1D("wmass_35","wmass 35",wbins,wmin,wmax);
    mHistos["mw40"] = new TH1D("wmass_40","wmass 40",wbins,wmin,wmax);

    mHistos["metx"] = new TH1D("metx","met x diff",100,-40,40);
    mHistos["mety"] = new TH1D("mety","met y diff",100,-40,40);

    mHistos["ptmevslep_tru"] = new TH1D("pt_mvsl_tru","pt met vs. lep cp",500,0,10);
    mHistos["pmevslep_tru"] = new TH1D("p_mvsl_tru","p met vs. lep cp",500,0,10);
    mHistos["ptmevslep_tautru"] = new TH1D("pt_mvsl_tautru","pt met vs. lep cp",500,0,10);
    mHistos["pmevslep_tautru"] = new TH1D("p_mvsl_tautru","p met vs. lep cp",500,0,10);
    mHistos["ptmevslep_cp"] = new TH1D("pt_mvsl_cp","pt met vs. lep cp",500,0,10);
    mHistos["pmevslep_cp"] = new TH1D("p_mvsl_cp","p met vs. lep cp",500,0,10);
    mHistos["ptlepvsme_cp"] = new TH1D("pt_lvsm_cp","pt lep vs. met cp",500,0,10);
    mHistos["plepvsme_cp"] = new TH1D("p_lvsm_cp","p lep vs. met cp",500,0,10);
    mHistos["ptmevslep_cpb"] = new TH1D("pt_mvsl_cpb","pt met vs. lep cp bad",500,0,10);
    mHistos["pmevslep_cpb"] = new TH1D("p_mvsl_cpb","p met vs. lep cp bad",500,0,10);
    mHistos["ptmevslep_wp"] = new TH1D("pt_mvsl_wp","pt met vs. lep wp",500,0,10);
    mHistos["pmevslep_wp"] = new TH1D("p_mvsl_wp","p met vs. lep wp",500,0,10);
    mHistos["ptmevslep_det"] = new TH1D("pt_mvsl_det","pt met vs. lep det",500,0,10);
    mHistos["pmevslep_det"] = new TH1D("p_mvsl_det","p met vs. lep det",500,0,10);
    mHistos["ptmevslep_tau"] = new TH1D("pt_mvsl_tau","pt met vs. lep tau",500,0,10);
    mHistos["pmevslep_tau"] = new TH1D("p_mvsl_tau","p met vs. lep tau",500,0,10);
    mHistos["pt_bnuvsb"] = new TH1D("pt_bnuvsb","b vs bnu",100,0.95,2.45);
    mHistos["pvspt_bnuvsb"] = new TH1D("pvspt_bnuvsb","b vs bnu",100,0.95,2.45);
    mHistos["pt_bnuvsmet"] = new TH1D("pt_bnuvsmet","bnu vs met",100,0,2.5);
    mHistos["pt_onuvsmet"] = new TH1D("pt_onuvsmet","onu vs met",100,0,2.5);

    mHistos["categories"] = new TH1D("categories","categories",16,0,16);
    mHistos["conv_0w"] = new TH1D("conv_0w","conw_0w",4,0,4);
    mHistos["conv_1w"] = new TH1D("conv_1w","conw_1w",4,0,4);
    mHistos["conv_2w"] = new TH1D("conv_2w","conw_2w",4,0,4);
    mHistos["conv_un"] = new TH1D("conv_un","conw_un",5,0,5);

    mHistos["lrat_0w_cb"] = new TH1D("lrat_0w_cb","lrat 0w",50,0,5);
    mHistos["lrat_1w_cb"] = new TH1D("lrat_1w_cb","lrat 1w",50,0,5);
    mHistos["lrat_1w_cw"] = new TH1D("lrat_1w_cw","lrat 1w",50,0,5);
    mHistos["lrat_2w_cb"] = new TH1D("lrat_2w_cb","lrat 2w",50,0,5);
    mHistos["lrat_2w_cw1"] = new TH1D("lrat_2w_cw1","lrat 2w",50,0,5);
    mHistos["lrat_2w_cw2"] = new TH1D("lrat_2w_cw2","lrat 2w",50,0,5);

    mHistos["pgof_tru_c"] = new TH1D("pgof_tru_c","pgof tru",46,0.1,1.02);
    mHistos["pgof_eff_c"] = new TH1D("pgof_eff_c","pgof eff",46,0.1,1.02);
    mHistos["pgof_tru_b"] = new TH1D("pgof_tru_b","pgof tru",46,0.1,1.02);
    mHistos["pgof_eff_b"] = new TH1D("pgof_eff_b","pgof eff",46,0.1,1.02);
    mHistos["pgof_tru_w"] = new TH1D("pgof_tru_w","pgof tru",46,0.1,1.02);
    mHistos["pgof_eff_w"] = new TH1D("pgof_eff_w","pgof eff",46,0.1,1.02);
    mHistos["pgof_tru_u"] = new TH1D("pgof_tru_u","pgof tru",46,0.1,1.02);
    mHistos["pgof_eff_u"] = new TH1D("pgof_eff_u","pgof eff",46,0.1,1.02);

    mHistos["chi2likeli"] = new TH1D("chi2likeli","chi2likeli",30,0,30);
    mHistos["chi2tot"] = new TH1D("chi2tot","chi2tot",30,0,30);
    mHistos["chi2red"] = new TH1D("chi2red","chi2red",30,0,30);
    mHistos["chi2nu_c"] = new TH1D("chi2nu_c","chi2nu_c",50,-50,50);
    mHistos["chi2nu_b"] = new TH1D("chi2nu_b","chi2nu_b",50,-50,50);
    mHistos["chi2nu_w"] = new TH1D("chi2nu_w","chi2nu_w",50,-50,50);

    mProfs["ptsigma_k"] = new TProfile("ptsigma_kt","pt sigma k",ptBlts,ptRangeB);
    mProfs["ptsigma_b"] = new TProfile("ptsigma_b","pt sigma b",ptAlts,ptRangeA);
    mProfs["etasigma_b"] = new TProfile("etasigma_b","eta sigma b",ptAlts,ptRangeA);
    mProfs["phisigma_b"] = new TProfile("phisigma_b","phi sigma b",ptAlts,ptRangeA);
    mProfs["ptsigma_l"] = new TProfile("ptsigma_l","pt sigma l",ptAlts,ptRangeA);
    mProfs["etasigma_l"] = new TProfile("etasigma_l","eta sigma l",ptAlts,ptRangeA);
    mProfs["phisigma_l"] = new TProfile("phisigma_l","phi sigma l",ptAlts,ptRangeA);
    for (int i = 1; i<=ptAlts; ++i) {
        mHistos[Form("ptdiff_k_%d",i)] = new TH1D(Form("ptdiff_kx%d",i),Form("ptdiff kt %d",i),100,-50,50);
        mHistos[Form("ptdiff_b_%d",i)] = new TH1D(Form("ptdiff_bx%d",i),Form("ptdiff b %d",i),100,-20,20);
        mHistos[Form("etadiff_b_%d",i)] = new TH1D(Form("etadiff_bx%d",i),Form("etadiff b %d",i),100,-0.3,0.3);
        mHistos[Form("phidiff_b_%d",i)] = new TH1D(Form("phidiff_bx%d",i),Form("phidiff b %d",i),100,-0.3,0.3);
        mHistos[Form("ptdiff_l_%d",i)] = new TH1D(Form("ptdiff_lx%d",i),Form("ptdiff l %d",i),100,-20,20);
        mHistos[Form("etadiff_l_%d",i)] = new TH1D(Form("etadiff_lx%d",i),Form("etadiff l %d",i),100,-0.3,0.3);
        mHistos[Form("phidiff_l_%d",i)] = new TH1D(Form("phidiff_lx%d",i),Form("phidiff l %d",i),100,-0.3,0.3);
    }
    // The non-dependent resolutions
    mHistos["ptdiff_k"] = new TH1D("ptdiff_kt","ptdiff kt",100,-50,50);
    mHistos["ptdiff_b"] = new TH1D("ptdiff_b","ptdiff b",100,-20,20);
    mHistos["ptdiff_l"] = new TH1D("ptdiff_l","ptdiff l",100,-20,20);
    mHistos["ptdiffnu_k"] = new TH1D("ptdiffnu_kt","ptdiffnu kt",100,-50,50);
    mHistos["ptdiffnu_b"] = new TH1D("ptdiffnu_b","ptdiffnu b",100,-20,20);
    mHistos["ptdiffnu_l"] = new TH1D("ptdiffnu_l","ptdiffnu l",100,-20,20);
    mHistos["etadiff_b"] = new TH1D("etadiff_b","etadiff b",100,-0.3,0.3);
    mHistos["etadiff_l"] = new TH1D("etadiff_l","etadiff l",100,-0.3,0.3);
    mHistos["etadiffnu_b"] = new TH1D("etadiffnu_b","etadiffnu b",100,-0.3,0.3);
    mHistos["etadiffnu_l"] = new TH1D("etadiffnu_l","etadiffnu l",100,-0.3,0.3);
    mHistos["phidiff_b"] = new TH1D("phidiff_b","phidiff b",100,-0.3,0.3);
    mHistos["phidiff_l"] = new TH1D("phidiff_l","phidiff l",100,-0.3,0.3);
    mHistos["phidiffnu_b"] = new TH1D("phidiffnu_b","phidiffnu b",100,-0.3,0.3);
    mHistos["phidiffnu_l"] = new TH1D("phidiffnu_l","phidiffnu l",100,-0.3,0.3);

    mHistos["ExpWMass"] = new TH1D("wmass_ejets","wmass e",wbins,wmin,wmax);
    mHistos["ExpWMassFine"] = new TH1D("wmass_ejets_fine","wmass efine",wbins,wmin,wmax);
    mHistos["ExpWMassModerate"] = new TH1D("wmass_ejets_moderate","wmass emoderate",wbins,wmin,wmax);
    mHistos["ExpWMassFjals"] = new TH1D("wmass_ejets_rough","wmass erough",wbins,wmin,wmax);
    mHistos["ExpWMassWggBg"] = new TH1D("wmass_ejets_wggbg","wmass ewggbg",wbins,wmin,wmax);
    mHistos["ExpWMassWggBb"] = new TH1D("wmass_ejets_wggbb","wmass ewggbb",wbins,wmin,wmax);
    mHistos["ExpWMassWgbBg"] = new TH1D("wmass_ejets_wgbbg","wmass ewgbbg",wbins,wmin,wmax);
    mHistos["ExpWMassWgbBb"] = new TH1D("wmass_ejets_wgbbb","wmass ewgbbb",wbins,wmin,wmax);
    mHistos["ExpWMassWbbBg"] = new TH1D("wmass_ejets_wbbbg","wmass ewbbbg",wbins,wmin,wmax);
    mHistos["ExpWMassWbbBb"] = new TH1D("wmass_ejets_wbbbb","wmass ewbbbb",wbins,wmin,wmax);
    mHistos["MetMass"] = new TH1D("metmass","wmass met",wbins,wmin,wmax);
    mHistos["MNetMass"] = new TH1D("metneutrmass","wmass metneutr",wbins,wmin,wmax);
    mHistos["NetMass"] = new TH1D("neutrmass","wmass neutri",wbins,wmin,wmax);
    mHistos["W1Mass"] = new TH1D("w1mass","w1mass",wbins,wmin,wmax);
    mHistos["W2Mass"] = new TH1D("w2mass","w2mass",wbins,wmin,wmax);
    mHistos["W3Mass"] = new TH1D("w3mass","w3mass",wbins,wmin,wmax);
    mHistos["WPMass"] = new TH1D("wpmass","wpmass",wbins,wmin,wmax);
    mHistos["WRecoPMass"] = new TH1D("wrpmass","wrecopmass",wbins,wmin,wmax);
    mHistos["WRecoJMass"] = new TH1D("wrjmass","wrecojmass",wbins,wmin,wmax);
    mHistos["WRecoNuJMass"] = new TH1D("wrnujmass","wreconujmass",wbins,wmin,wmax);
    mHistos["W1JMass"] = new TH1D("w1jmass","w1jmass",wbins,wmin,wmax);
    mHistos["W2JMass"] = new TH1D("w2jmass","w2jmass",wbins,wmin,wmax);
    mHistos["W3JMass"] = new TH1D("w3jmass","w3jmass",wbins,wmin,wmax);
    mHistos["W3JWrong"] = new TH1D("w3jwrong","w3jwrong",wbins,wmin,wmax);
    mHistos["W3JWrongish"] = new TH1D("w3jwrongish","w3jwrongish",wbins,wmin,wmax);
    mHistos["W3JFake"] = new TH1D("w3jfake","w3jfake",wbins,wmin,wmax);

    mHistos["wFracture"] = new TH1D("wfracture","w fracture",10,0,10);
    mHistos["bFracture"] = new TH1D("bfracture","b fracture",10,0,10);
    mHistos["bbFracture"] = new TH1D("bbfracture","bb fracture",10,0,10);
    mHistos["totFracture"] = new TH1D("totfracture","tot fracture",10,0,10);
    mHistos["ttFracture"] = new TH1D("ttfracture","tt fracture",10,0,10);
    mHistos["oFracture"] = new TH1D("ofracture","other fracture",10,0,10);
    mHistos["PartonPerms"] = new TH1D("partonperms","p perms",10,0,10);

    mHistos["LeadMass"] = new TH1D("leadjmass","leadjmass",50,0,250);
    mHistos["MetVMass"] = new TH1D("vmass_met","met mass",50,0,100);
    mHistos["NetVMass"] = new TH1D("vmass_neutr","neutri mass",50,0,100);

    mHistos["PtThird"] = new TH1D("pt_third","ptNLOparton",500,0,400);
    mHistos["PtJThird"] = new TH1D("pt_jthird","ptNLOjet",500,0,400);
    mHistos["PtRealThird"] = new TH1D("pt_rthird","pt3rdparton",500,0,400);
    mHistos["PtLead"] = new TH1D("pt_lead","pt1",500,0,400);
    mHistos["PtLeads"] = new TH1D("pt_leads","pt12",500,0,400);
    mHistos["PtFours"] = new TH1D("pt_fours","pt1234",500,0,400);
    mHistos["PtTF"] = new TH1D("pt_34","pt34",500,0,400);

    mHistos["PhiBPart"] = new TH1D("phi_bpart","phi b partons", 500, 0, TMath::Pi());
    mHistos["PhiWPart"] = new TH1D("phi_wpart","phi w partons", 500, 0, TMath::Pi());
    mHistos["PhiTPart"] = new TH1D("phi_tpart","phi t partons", 500, 0, TMath::Pi());
    mHistos["PhiWTPart"] = new TH1D("phi_wtpart","phi wrong t partons", 500, 0, TMath::Pi());

    mProfs["alphaPTB"] = new TProfile("alpha_ptb","alpha ptratio b",mABins,aRange);
    mProfs["alphaMW"] = new TProfile("alpha_mw","alpha mw",mABins,aRange);
    mProfs["alphaMT"] = new TProfile("alpha_mt","alpha mt",mABins,aRange);
    mProfs["talphaPTB"] = new TProfile("talpha_ptb","true alpha ptratio b",mABins,aRange);
    mProfs["talphaMW"] = new TProfile("talpha_mw","true alpha mw",mABins,aRange);
    mProfs["talphaMT"] = new TProfile("talpha_mt","true alpha mt",mABins,aRange);
    mProfs["ttalphaPTB"] = new TProfile("ttalpha_ptb","ttrue alpha ptratio b",mABins,aRange);
    mProfs["ttalphaMW"] = new TProfile("ttalpha_mw","ttrue alpha mw",mABins,aRange);
    mProfs["ttalphaMT"] = new TProfile("ttalpha_mt","ttrue alpha mt",mABins,aRange);
    mProfs["tttalphaPTB"] = new TProfile("tttalpha_ptb","tttrue alpha ptratio b",mABins,aRange);
    mProfs["tttalphaMW"] = new TProfile("tttalpha_mw","tttrue alpha mw",mABins,aRange);
    mProfs["tttalphaMT"] = new TProfile("tttalpha_mt","tttrue alpha mt",mABins,aRange);

    mProfs["jvg"] = new TProfile("jvsgamma","jetptpergamma",mABins,aRange);
    mProfs["pvg"] = new TProfile("pvsgamma","MEpartonptpergamma",mABins,aRange);
    mProfs["tpvg"] = new TProfile("tpvsgamma","Fpartonptpergamma",mABins,aRange);
    mProfs["MEpvg"] = new TProfile("ttpvsgamma","TheMEpartonptpergamma",mABins,aRange);

    mHistos["actrl"] = new TH1D("actrl","actrl",mABins,aRange);
    mHistos["tactrl"] = new TH1D("tactrl","tactrl",mABins,aRange);
    mHistos["ttactrl"] = new TH1D("ttactrl","ttactrl",mABins,aRange);
    mHistos["tttactrl"] = new TH1D("tttactrl","tttactrl",mABins,aRange);

    mHistos["a05"]=new TH1D("weights05","",ptBins,ptRange);
    mHistos["a10"]=new TH1D("weights10","",ptBins,ptRange);
    mHistos["a15"]=new TH1D("weights15","",ptBins,ptRange);
    mHistos["a20"]=new TH1D("weights20","",ptBins,ptRange);
    mHistos["a25"]=new TH1D("weights25","",ptBins,ptRange);
    mHistos["a30"]=new TH1D("weights30","",ptBins,ptRange);
    mHistos["a35"]=new TH1D("weights35","",ptBins,ptRange);
    mHistos["a40"]=new TH1D("weights40","",ptBins,ptRange);

    mProfs["a05"]=new TProfile("prof05","",ptBins,ptRange);
    mProfs["a10"]=new TProfile("prof10","",ptBins,ptRange);
    mProfs["a15"]=new TProfile("prof15","",ptBins,ptRange);
    mProfs["a20"]=new TProfile("prof20","",ptBins,ptRange);
    mProfs["a25"]=new TProfile("prof25","",ptBins,ptRange);
    mProfs["a30"]=new TProfile("prof30","",ptBins,ptRange);
    mProfs["a35"]=new TProfile("prof35","",ptBins,ptRange);
    mProfs["a40"]=new TProfile("prof40","",ptBins,ptRange);

    mProfs["mw05"]=new TProfile("wprof05","",ptBins,ptRange);
    mProfs["mw10"]=new TProfile("wprof10","",ptBins,ptRange);
    mProfs["mw15"]=new TProfile("wprof15","",ptBins,ptRange);
    mProfs["mw20"]=new TProfile("wprof20","",ptBins,ptRange);
    mProfs["mw25"]=new TProfile("wprof25","",ptBins,ptRange);
    mProfs["mw30"]=new TProfile("wprof30","",ptBins,ptRange);
    mProfs["mw35"]=new TProfile("wprof35","",ptBins,ptRange);
    mProfs["mw40"]=new TProfile("wprof40","",ptBins,ptRange);

    mProfs["mt05"]=new TProfile("tprof05","",ptBins,ptRange);
    mProfs["mt10"]=new TProfile("tprof10","",ptBins,ptRange);
    mProfs["mt15"]=new TProfile("tprof15","",ptBins,ptRange);
    mProfs["mt20"]=new TProfile("tprof20","",ptBins,ptRange);
    mProfs["mt25"]=new TProfile("tprof25","",ptBins,ptRange);
    mProfs["mt30"]=new TProfile("tprof30","",ptBins,ptRange);
    mProfs["mt35"]=new TProfile("tprof35","",ptBins,ptRange);
    mProfs["mt40"]=new TProfile("tprof40","",ptBins,ptRange);

//     grid1 = new TH2D("g1","g1",mABins,aRange,50,0.0,2.0);
//     grid2 = new TH2D("g2","g2",mABins,aRange,50,0.0,2.0);
//     grid3 = new TH2D("g3","g3",mABins,aRange,50,0.0,2.0);

    mATitles.push_back(std::make_pair("a40",0.4));
    mATitles.push_back(std::make_pair("a35",0.35));
    mATitles.push_back(std::make_pair("a30",0.3));
    mATitles.push_back(std::make_pair("a25",0.25));
    mATitles.push_back(std::make_pair("a20",0.2));
    mATitles.push_back(std::make_pair("a15",0.15));
    mATitles.push_back(std::make_pair("a10",0.1));
    mATitles.push_back(std::make_pair("a05",0.05));

    mMtTitles.push_back(std::make_pair("mt40",0.4));
    mMtTitles.push_back(std::make_pair("mt35",0.35));
    mMtTitles.push_back(std::make_pair("mt30",0.3));
    mMtTitles.push_back(std::make_pair("mt25",0.25));
    mMtTitles.push_back(std::make_pair("mt20",0.2));
    mMtTitles.push_back(std::make_pair("mt15",0.15));
    mMtTitles.push_back(std::make_pair("mt10",0.1));
    mMtTitles.push_back(std::make_pair("mt05",0.05));

    mMwTitles.push_back(std::make_pair("mw40",0.4));
    mMwTitles.push_back(std::make_pair("mw35",0.35));
    mMwTitles.push_back(std::make_pair("mw30",0.3));
    mMwTitles.push_back(std::make_pair("mw25",0.25));
    mMwTitles.push_back(std::make_pair("mw20",0.2));
    mMwTitles.push_back(std::make_pair("mw15",0.15));
    mMwTitles.push_back(std::make_pair("mw10",0.1));
    mMwTitles.push_back(std::make_pair("mw05",0.05));
}

NewJets::~NewJets()
{
    if (!fChain) return;
    delete fChain->GetCurrentFile();
}

Int_t NewJets::GetEntry(Long64_t entry)
{
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}
Long64_t NewJets::LoadTree(Long64_t entry)
{
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
        fCurrent = fChain->GetTreeNumber();
    }
    return centry;
}

void NewJets::Init(TTree *tree)
{
    isolation = 0;
    prtcl_jet = 0;
    prtcl_pdgid = 0;
    prtcl_pt = 0;
    prtcl_eta = 0;
    prtcl_phi = 0;
    prtcl_e = 0;
    prtn_jet = 0;
    prtn_ptn = 0;
    prtn_own = 0;
    prtn_pdgid = 0;
    prtn_tag = 0;
    prtn_pt = 0;
    prtn_eta = 0;
    prtn_phi = 0;
    prtn_e = 0;
    prtn_dr = 0;
    jet_pt = 0;
    jet_eta = 0;
    jet_phi = 0;
    jet_e = 0;
    jet_ptn = 0;
    jet_constituents = 0;
    jet_ptd = 0;
    jet_sigma2 = 0;
    // Set branch addresses and branch pointers
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);

    fChain->SetBranchAddress("info", &info);
    fChain->SetBranchAddress("bnucount", &bnucount);
    fChain->SetBranchAddress("nub", &nub);
    fChain->SetBranchAddress("nuc", &nuc);
    fChain->SetBranchAddress("nulept", &nulept);
    fChain->SetBranchAddress("nuother", &nuother);
    fChain->SetBranchAddress("weight", &weight);
    fChain->SetBranchAddress("pthat", &pthat);
    fChain->SetBranchAddress("isolation", &isolation);
    fChain->SetBranchAddress("prtcl_jet", &prtcl_jet);
    fChain->SetBranchAddress("prtcl_pdgid", &prtcl_pdgid);
    fChain->SetBranchAddress("prtcl_pt", &prtcl_pt);
    fChain->SetBranchAddress("prtcl_eta", &prtcl_eta);
    fChain->SetBranchAddress("prtcl_phi", &prtcl_phi);
    fChain->SetBranchAddress("prtcl_e", &prtcl_e);
    fChain->SetBranchAddress("prtn_jet", &prtn_jet);
    fChain->SetBranchAddress("prtn_ptn", &prtn_ptn);
    fChain->SetBranchAddress("prtn_own", &prtn_own);
    fChain->SetBranchAddress("prtn_pdgid", &prtn_pdgid);
    fChain->SetBranchAddress("prtn_tag", &prtn_tag);
    fChain->SetBranchAddress("prtn_pt", &prtn_pt);
    fChain->SetBranchAddress("prtn_eta", &prtn_eta);
    fChain->SetBranchAddress("prtn_phi", &prtn_phi);
    fChain->SetBranchAddress("prtn_e", &prtn_e);
    fChain->SetBranchAddress("prtn_dr", &prtn_dr);
    fChain->SetBranchAddress("jet_pt", &jet_pt);
    fChain->SetBranchAddress("jet_eta", &jet_eta);
    fChain->SetBranchAddress("jet_phi", &jet_phi);
    fChain->SetBranchAddress("jet_e", &jet_e);
    fChain->SetBranchAddress("jet_ptn", &jet_ptn);
    fChain->SetBranchAddress("jet_constituents", &jet_constituents);
    fChain->SetBranchAddress("jet_ptd", &jet_ptd);
    fChain->SetBranchAddress("jet_sigma2", &jet_sigma2);
    fChain->SetBranchAddress("met", &met);
}

#endif // #ifdef NewJets_cxx
