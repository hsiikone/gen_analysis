//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Wed Jul 31 07:10:14 2019 by ROOT version 6.14/00
// from TTree Events/Events
// found on file: G16/750278D4-DCA4-5D45-80F2-DCEF33E279BF.root
//////////////////////////////////////////////////////////

#ifndef MPF_h
#define MPF_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TLorentzVector.h>
#include <TMatrixD.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

using std::cout;
using std::endl;
using std::string;
using std::vector;

struct Fracs {
    double chf;
    double nhf;
    double phf;
    double emf;
};

// Header file for the classes stored in the TTree if any.

class MPF {
public :
    TTree          *fChain;   //!pointer to the analyzed TTree or TChain
    Int_t           fCurrent; //!current Tree number in a TChain

    static const Int_t kMaxJets = 100;
    static const Int_t kMaxPhotons = 1000;

    double          mHtx;
    double          mHty;
    double          mHhx;
    double          mHhy;
    double          mAlpha;
    // Declaration of leaf types
    UInt_t          run;
    UInt_t          luminosityBlock;
    ULong64_t       event;
    Float_t         PuppiMET_phi;
    Float_t         PuppiMET_pt;
    Float_t         RawMET_phi;
    Float_t         RawMET_pt;
    Float_t         GenMET_phi;
    Float_t         GenMET_pt;
    Float_t         MET_phi;
    Float_t         MET_pt;
    Float_t         ChsMET_phi;
    Float_t         ChsMET_pt;
    Float_t         ChsMET_sumEt;
    UInt_t          nGenJet;
    Float_t         GenJet_eta[kMaxJets];   //[nGenJet]
    Float_t         GenJet_mass[kMaxJets];   //[nGenJet]
    Float_t         GenJet_phi[kMaxJets];   //[nGenJet]
    Float_t         GenJet_pt[kMaxJets];   //[nGenJet]
    UInt_t          nGenPart;
    Float_t         GenPart_eta[kMaxPhotons];   //[nGenPart]
    Float_t         GenPart_mass[kMaxPhotons];   //[nGenPart]
    Float_t         GenPart_phi[kMaxPhotons];   //[nGenPart]
    Float_t         GenPart_pt[kMaxPhotons];   //[nGenPart]
    Int_t           GenPart_genPartIdxMother[kMaxPhotons];   //[nGenPart]
    Int_t           GenPart_pdgId[kMaxPhotons];   //[nGenPart]
    Int_t           GenPart_status[kMaxPhotons];   //[nGenPart]
    Int_t           GenPart_statusFlags[kMaxPhotons];   //[nGenPart]
    Float_t         genWeight;

    UInt_t          nJet;
    Float_t         Jet_area[kMaxJets];   //[nJet]
    Float_t         Jet_btagCMVA[kMaxJets];   //[nJet]
    Float_t         Jet_btagCSVV2[kMaxJets];   //[nJet]
    Float_t         Jet_btagDeepB[kMaxJets];   //[nJet]
    Float_t         Jet_btagDeepC[kMaxJets];   //[nJet]
    Float_t         Jet_btagDeepFlavB[kMaxJets];   //[nJet]
    Float_t         Jet_btagDeepFlavC[kMaxJets];   //[nJet]
    Float_t         Jet_chEmEF[kMaxJets];   //[nJet]
    Float_t         Jet_chHEF[kMaxJets];   //[nJet]
    Float_t         Jet_eta[kMaxJets];   //[nJet]
    //Float_t         Jet_jercCHF[kMaxJets];   //[nJet]
    //Float_t         Jet_jercCHPUF[kMaxJets];   //[nJet]
    Float_t         Jet_mass[kMaxJets];   //[nJet]
    Float_t         Jet_muEF[kMaxJets];   //[nJet]
    //Float_t         Jet_muonSubtrFactor[kMaxJets];   //[nJet]
    Float_t         Jet_neEmEF[kMaxJets];   //[nJet]
    Float_t         Jet_neHEF[kMaxJets];   //[nJet]
    Float_t         Jet_phi[kMaxJets];   //[nJet]
    Float_t         Jet_pt[kMaxJets];   //[nJet]
    Float_t         Jet_qgl[kMaxJets];   //[nJet]
    Float_t         Jet_rawFactor[kMaxJets];   //[nJet]
    Int_t           Jet_jetId[kMaxJets];   //[nJet]
    Int_t           Jet_nConstituents[kMaxJets];   //[nJet]
    Int_t           Jet_nMuons[kMaxJets];   //[nJet]
    Int_t           Jet_puId[kMaxJets];   //[nJet]
    UInt_t          nPhoton;
    Float_t         Photon_pt[kMaxPhotons];   //[nPhoton]
    Float_t         Photon_eta[kMaxPhotons];   //[nPhoton]
    Float_t         Photon_phi[kMaxPhotons];   //[nPhoton]
    Float_t         Photon_eCorr[kMaxPhotons];   //[nPhoton]
    Float_t         Photon_energyErr[kMaxPhotons];   //[nPhoton]
    Float_t         Photon_hoe[kMaxPhotons];   //[nPhoton]
    Float_t         Photon_mvaID[kMaxPhotons];   //[nPhoton]
    Float_t         Photon_mvaID17[kMaxPhotons];   //[nPhoton]
    Float_t         Photon_pfRelIso03_all[kMaxPhotons];   //[nPhoton]
    Float_t         Photon_pfRelIso03_chg[kMaxPhotons];   //[nPhoton]
    Float_t         Photon_r9[kMaxPhotons];   //[nPhoton]
    Float_t         Photon_sieie[kMaxPhotons];   //[nPhoton]
    Int_t           Photon_cutBased[kMaxPhotons];   //[nPhoton]
    Int_t           Photon_cutBased17Bitmap[kMaxPhotons];   //[nPhoton]
    Int_t           Photon_jetIdx[kMaxPhotons];   //[nPhoton]
    Int_t           Photon_vidNestedWPBitmap[kMaxPhotons];   //[nPhoton]
    Bool_t          Photon_electronVeto[kMaxPhotons];   //[nPhoton]
    Bool_t          Photon_isScEtaEB[kMaxPhotons];   //[nPhoton]
    Bool_t          Photon_isScEtaEE[kMaxPhotons];   //[nPhoton]
    Bool_t          Photon_mvaID17_WP80[kMaxPhotons];   //[nPhoton]
    Bool_t          Photon_mvaID17_WP90[kMaxPhotons];   //[nPhoton]
    Bool_t          Photon_mvaID_WP80[kMaxPhotons];   //[nPhoton]
    Bool_t          Photon_mvaID_WP90[kMaxPhotons];   //[nPhoton]
    Bool_t          Photon_pixelSeed[kMaxPhotons];   //[nPhoton]
    //UChar_t         Photon_seedGain[kMaxPhotons];   //[nPhoton]
    //Float_t         fixedGridRhoFastjetAll;
    //Float_t         fixedGridRhoFastjetCentral;
    //Float_t         fixedGridRhoFastjetCentralCalo;
    //Float_t         fixedGridRhoFastjetCentralChargedPileUp;
    //Float_t         fixedGridRhoFastjetCentralNeutral;
    //UInt_t          nOtherPV;
    //Float_t         OtherPV_z[3];   //[nOtherPV]
    //Float_t         PV_ndof;
    //Float_t         PV_x;
    //Float_t         PV_y;
    //Float_t         PV_z;
    //Float_t         PV_chi2;
    //Float_t         PV_score;
    //Int_t           PV_npvs;
    //Int_t           PV_npvsGood;
    Bool_t          Flag_HBHENoiseFilter;
    Bool_t          Flag_HBHENoiseIsoFilter;
    Bool_t          Flag_CSCTightHaloFilter;
    Bool_t          Flag_CSCTightHaloTrkMuUnvetoFilter;
    Bool_t          Flag_CSCTightHalo2015Filter;
    Bool_t          Flag_globalTightHalo2016Filter;
    Bool_t          Flag_globalSuperTightHalo2016Filter;
    Bool_t          Flag_HcalStripHaloFilter;
    Bool_t          Flag_hcalLaserEventFilter;
    Bool_t          Flag_EcalDeadCellTriggerPrimitiveFilter;
    Bool_t          Flag_EcalDeadCellBoundaryEnergyFilter;
    Bool_t          Flag_ecalBadCalibFilter;
    Bool_t          Flag_goodVertices;
    Bool_t          Flag_eeBadScFilter;
    Bool_t          Flag_ecalLaserCorrFilter;
    Bool_t          Flag_trkPOGFilters;
    Bool_t          Flag_chargedHadronTrackResolutionFilter;
    Bool_t          Flag_muonBadTrackFilter;
    Bool_t          Flag_BadChargedCandidateFilter;
    Bool_t          Flag_BadPFMuonFilter;
    Bool_t          Flag_BadChargedCandidateSummer16Filter;
    Bool_t          Flag_BadPFMuonSummer16Filter;
    Bool_t          Flag_trkPOG_manystripclus53X;
    Bool_t          Flag_trkPOG_toomanystripclus53X;
    Bool_t          Flag_trkPOG_logErrorTooManyClusters;
    Bool_t          METFilters;
    Bool_t          HLT_Photon22;
    Bool_t          HLT_Photon30;
    Bool_t          HLT_Photon36;
    Bool_t          HLT_Photon50;
    Bool_t          HLT_Photon75;
    Bool_t          HLT_Photon90;
    Bool_t          HLT_Photon120;
    Bool_t          HLT_Photon175;
    Bool_t          HLT_Photon500;
    Bool_t          HLT_Photon600;
    Bool_t          mP22;
    Bool_t          mP30;
    Bool_t          mP36;
    Bool_t          mP50;
    Bool_t          mP75;
    Bool_t          mP90;
    Bool_t          mP120;
    Bool_t          mP165;
    vector<double>         mPThresholds;
    vector<TLorentzVector> mJets;
    vector<bool>           mJetIDs;
    vector<Fracs>          mJetEFracs;
    vector<TLorentzVector> mGenJets;
    vector<TLorentzVector> mPhotons;
    TLorentzVector         mGenPhoton;
    int                    mGenIdx;

    vector<double>            mPts;
    vector<double>            mEtas;
    vector<double>            mSteps;
    unsigned                  mNPts;
    unsigned                  mNEtas;
    unsigned                  mNSteps;
    TMatrixD                 *mSquare;
    TMatrixD                 *mColumn;
    TMatrixD                 *mSingle;
    TMatrixD                 *mTSquare;
    TMatrixD                 *mTColumn;
    vector<double>           *mTmpVec;
    vector<double>           *mTmpVecT;
    vector<double>           *mTmpVec3;
    vector<double>           *mTmpVecT3;
    vector<double>           *mTmpVecQG;
    vector<double>           *mTmpVecTQG;

    bool                      mMatrixMode;
    bool                      mIsDT;
    bool                      mDo3;
    bool                      mDoQG;

    TH1D                     *mGPtDistr;
    TH1D                     *mAlphaDistr;
    TH1D                     *mSAlphaDistr;
    TProfile                 *mAlphaSAlpha;
    TH2D                     *mAlpha2D;
    TH2D                     *mH2D;

    MPF();
    virtual ~MPF();
    virtual Int_t    GetEntry(Long64_t entry);
    virtual Long64_t LoadTree(Long64_t entry);
    virtual void     Init(TTree *tree);
    virtual void     Loop();
    virtual void     Show(Long64_t entry = -1);

    bool             AddJets();
    bool             AddGenJets();
    bool             AddPhotons();
    bool             AddGenPhoton();
    bool             FillMatrices(TLorentzVector &photon, vector<TLorentzVector> &jets, int refidx = 0);
    double EtaBinW(double eta);
    double PtBinW(double pt);
    int PhaseBin(double pt, double eta);
    double PtStep(double pt);
};

#endif

#ifdef MPF_cxx
double MPF::EtaBinW(double eta) {
    eta = fabs(eta);
    int ebin = -1;
    for (unsigned ieta = 0; ieta < mNEtas; ++ieta) {
        if (eta < mEtas[ieta+1]) {
            ebin = ieta;
            break;
        }
    }
    if (ebin==-1) return 0.0;
    if (ebin<3) return 1.0;
    if (ebin==3) return 2.0;
    if (ebin<6) return 3.0;
    if (ebin==6) return 10.0;

    return 1.0;
}

double MPF::PtBinW(double pt) {
    int ptbin = -1;
    for (unsigned ipt = 0; ipt < mNPts; ++ipt) {
        if (pt < mPts[ipt+1]) {
            ptbin = ipt;
            break;
        }
    }
    if (ptbin==-1) return 0.0;
    if (ptbin<5) return 10.0;
    if (ptbin<8) return 4.0;
    if (ptbin<13) return 2.5;

    return 1.0;
}

int MPF::PhaseBin(double pt, double eta) {
    eta = fabs(eta);
    int ebin = -1;
    int ptbin = -1;
    for (unsigned ieta = 0; ieta < mNEtas; ++ieta) {
        if (eta < mEtas[ieta+1]) {
            ebin = ieta;
            break;
        }
    }
    for (unsigned ipt = 0; ipt < mNPts; ++ipt) {
        if (pt < mPts[ipt+1]) {
            ptbin = ipt;
            break;
        }
    }
    if (ebin==-1 or ptbin==-1) return -1;

    return ebin*mNPts+ptbin;
}

double MPF::PtStep(double pt) {
    auto it = std::upper_bound(mSteps.begin(),mSteps.end(),pt);
    if (it==mSteps.begin() or it==mSteps.end()) return 0;
    return *it;
}

MPF::MPF() : fChain(0), mSingle(0)
{
    TChain *tree = new TChain("Events");
    mIsDT = true;
    mDo3 = false;
    mDoQG = false;
    mMatrixMode = true;

    vector<string> files;
    if (mIsDT) files = {"G16/032E7388-415E-E349-9AD7-7B1755B200E8.root",
                        "G16/09458702-8F7B-564C-8F52-8AB8AAD7AD8F.root",
                        "G16/0CD91378-8CA5-964A-B3B6-AC3726F89722.root",
                        "G16/1DF830EF-15D4-B045-856A-7DFFCD3111B8.root",
                        "G16/1FBD2507-77E6-844B-8DEA-B47C0E6853FC.root",
                        "G16/30A3EE31-1FC9-0443-8940-54605E457906.root",
                        "G16/3AAF3782-5314-1D46-B1E9-D6B5E5476BB8.root",
                        "G16/3E4ECD5C-4258-EA4F-B520-DBC64B8EBCF0.root",
                        "G16/477CE4FF-A715-C542-89FA-7144B4528DD7.root",
                        "G16/6E1B5330-8350-8242-85E8-E76D9957888D.root",
                        "G16/7013BB04-D43B-214B-949F-20ED9BFF67E7.root",
                        "G16/71B27803-FF66-4749-847B-82490268007C.root",
                        "G16/750278D4-DCA4-5D45-80F2-DCEF33E279BF.root",
                        "G16/761BA857-3A3C-A947-8BC1-2CF2292DE32B.root",
                        "G16/77934705-9B02-C244-BCA7-54EA6C05623D.root",
                        "G16/9656A777-E7CF-2142-8329-A705E85AB61A.root",
                        "G16/CC2C9C93-DB6E-E74E-BD3F-9E52877C7F7F.root",
                        "G16/F6301893-D47F-4449-BBD8-4F39B83B636E.root",
                        "G16/F9BBF998-8951-6C4F-A51D-F505C3CA5305.root"};
    else       files = {"MC16/0CDE3EE1-08E5-E34B-9F48-64CC7DEEF9AA.root",
                        "MC16/0E0672C7-CD3A-8B4B-8CFD-23B3FC0299AA.root",
                        "MC16/106513C9-965F-3D44-BC98-B49FD05E539E.root",
                        "MC16/1B81321A-1476-3146-A5E4-71016B5B30AC.root",
                        "MC16/1CC33D69-1F01-EC49-86A9-A2F8825CB8A6.root",
                        "MC16/36A2A324-0ECC-D84D-B75A-89B3F96E3138.root",
                        "MC16/3ED4BBB3-16E6-2F47-9997-31B3E1A2F6B6.root",
                        "MC16/40C43C10-C942-A146-AF4F-4B47ADA66E50.root",
                        "MC16/88D6A590-43EA-5E4C-93D1-B31DE533D5E9.root",
                        "MC16/898A8C7D-3A05-264E-8369-6054817F0667.root",
                        "MC16/8B08E163-B293-6C40-AD11-DFF6DABC6329.root",
                        "MC16/8D1383E8-219E-B546-8917-56C95F3FD3B6.root",
                        "MC16/989E6DE2-E2AE-5743-8D17-1F70E7A1191A.root",
                        "MC16/B6C8179E-3A2B-754C-B69A-1DC94DB8239A.root",
                        "MC16/BECEF2C4-252A-9241-B14E-3B0C90C74562.root",
                        "MC16/E674E081-70DF-6946-B2F8-346C8DE487BF.root",
                        "MC16/F2D5D7BD-FC19-A241-9E1C-910769E497F5.root"};

    for (auto &f : files) tree->AddFile(f.c_str());

    Init(tree);

    mPThresholds = {175.0,130.0,105.0,85.0,60.0,40.0};
    mPts = {15, 21, 28, 37, 49, 64, 84, 114, 153, 196, 272, 330, 395, 468, 548, 686, 846, 1032, 1248, 1588, 2500, 100000};
    mEtas = {0.0,0.5,1.0,1.5,2.0,2.5,3.5,5.0};
    //mPts = {0,50,100,200,20000};
    //mEtas = {0.0,1.3,2.5,5.0};
    mNPts = mPts.size()-1;
    mNEtas = mEtas.size()-1;
    double step = 30.0;
    while (step<10000.0) {
        mSteps.push_back(step);
        step *= 1.25;
    }
    mNSteps = mSteps.size()-1;
    int nbase = mNPts*mNEtas;
    int ntot = nbase+1;
    if (mMatrixMode) {
        mSquare = new TMatrixD(ntot,mIsDT ? 2*ntot : 4*ntot);
        mColumn = new TMatrixD(ntot,8);
        mSingle = new TMatrixD(1,5);
        mTmpVec  = new vector<double>(ntot,0.0);
        mTmpVecT = new vector<double>(ntot,0.0);
        if (mDo3) {
            int ntot3 = 4*nbase+1;
            mTSquare = new TMatrixD(ntot3,2*ntot3);
            mTColumn = new TMatrixD(ntot3,4);
            mTmpVec3  = new vector<double>(ntot3,0.0);
            mTmpVecT3 = new vector<double>(ntot3,0.0);
        } else if (mDoQG) {
            int ntotqg = 3*nbase+1;
            mTSquare = new TMatrixD(ntotqg,2*ntotqg);
            mTColumn = new TMatrixD(ntotqg,4);
            mTmpVecQG  = new vector<double>(ntotqg,0.0);
            mTmpVecTQG = new vector<double>(ntotqg,0.0);
        }
    }

    mGPtDistr = new TH1D("gpt","gpt",150,0,1500);
    mAlphaDistr = new TH1D("alpha","alpha",150,0.0,3.0);
    mSAlphaDistr = new TH1D("semi alpha","semi alpha",200,-1.0,3.0);
    mAlphaSAlpha = new TProfile("alpha vs. salpha","alpha vs. salpha",200,-1.0,3.0);
    mAlpha2D = new TH2D("alpha2","alpha2",200,-1.0,3.0,150,0.0,3.0);
    mH2D = new TH2D("halpha2","halpha2",150,0.0,3.0,150,0.0,3.0);
}

MPF::~MPF() {}

Int_t MPF::GetEntry(Long64_t entry)
{
    if (!fChain) return 0;
    return fChain->GetEntry(entry);
}
Long64_t MPF::LoadTree(Long64_t entry)
{
    if (!fChain) return -5;
    Long64_t centry = fChain->LoadTree(entry);
    if (centry < 0) return centry;
    if (fChain->GetTreeNumber() != fCurrent) {
       fCurrent = fChain->GetTreeNumber();
    }
    return centry;
}

void MPF::Init(TTree *tree)
{
    if (!tree) return;
    fChain = tree;
    fCurrent = -1;
    fChain->SetMakeClass(1);

    fChain->SetBranchAddress("run", &run);
    fChain->SetBranchAddress("luminosityBlock", &luminosityBlock);
    fChain->SetBranchAddress("event", &event);

    fChain->SetBranchAddress("PuppiMET_phi", &PuppiMET_phi);
    fChain->SetBranchAddress("PuppiMET_pt", &PuppiMET_pt);
    fChain->SetBranchAddress("RawMET_phi", &RawMET_phi);
    fChain->SetBranchAddress("RawMET_pt", &RawMET_pt);
    fChain->SetBranchAddress("MET_phi", &MET_phi);
    fChain->SetBranchAddress("MET_pt", &MET_pt);
    fChain->SetBranchAddress("ChsMET_phi", &ChsMET_phi);
    fChain->SetBranchAddress("ChsMET_pt", &ChsMET_pt);
    fChain->SetBranchAddress("ChsMET_sumEt", &ChsMET_sumEt);

    if (!mIsDT) {
        fChain->SetBranchAddress("GenMET_phi", &GenMET_phi);
        fChain->SetBranchAddress("GenMET_pt", &GenMET_pt);
        fChain->SetBranchAddress("nGenJet", &nGenJet);
        fChain->SetBranchAddress("GenJet_eta", GenJet_eta);
        fChain->SetBranchAddress("GenJet_mass", GenJet_mass);
        fChain->SetBranchAddress("GenJet_phi", GenJet_phi);
        fChain->SetBranchAddress("GenJet_pt", GenJet_pt);
        fChain->SetBranchAddress("nGenPart", &nGenPart);
        fChain->SetBranchAddress("GenPart_eta", GenPart_eta);
        fChain->SetBranchAddress("GenPart_mass", GenPart_mass);
        fChain->SetBranchAddress("GenPart_phi", GenPart_phi);
        fChain->SetBranchAddress("GenPart_pt", GenPart_pt);
        fChain->SetBranchAddress("GenPart_genPartIdxMother", GenPart_genPartIdxMother);
        fChain->SetBranchAddress("GenPart_pdgId", GenPart_pdgId);
        fChain->SetBranchAddress("GenPart_status", GenPart_status);
        fChain->SetBranchAddress("GenPart_statusFlags", GenPart_statusFlags);
        fChain->SetBranchAddress("genWeight", &genWeight);
    }

    fChain->SetBranchAddress("nJet", &nJet);
    fChain->SetBranchAddress("Jet_pt", Jet_pt);
    fChain->SetBranchAddress("Jet_eta", Jet_eta);
    fChain->SetBranchAddress("Jet_phi", Jet_phi);
    fChain->SetBranchAddress("Jet_mass", Jet_mass);
    fChain->SetBranchAddress("Jet_area", Jet_area);
    //fChain->SetBranchAddress("Jet_btagCMVA", Jet_btagCMVA);
    //fChain->SetBranchAddress("Jet_btagCSVV2", Jet_btagCSVV2);
    //fChain->SetBranchAddress("Jet_btagDeepB", Jet_btagDeepB);
    //fChain->SetBranchAddress("Jet_btagDeepC", Jet_btagDeepC);
    //fChain->SetBranchAddress("Jet_btagDeepFlavB", Jet_btagDeepFlavB);
    //fChain->SetBranchAddress("Jet_btagDeepFlavC", Jet_btagDeepFlavC);
    fChain->SetBranchAddress("Jet_chEmEF", Jet_chEmEF);
    fChain->SetBranchAddress("Jet_chHEF", Jet_chHEF);
    //fChain->SetBranchAddress("Jet_jercCHF", Jet_jercCHF);
    //fChain->SetBranchAddress("Jet_jercCHPUF", Jet_jercCHPUF);
    fChain->SetBranchAddress("Jet_muEF", Jet_muEF);
    //fChain->SetBranchAddress("Jet_muonSubtrFactor", Jet_muonSubtrFactor);
    fChain->SetBranchAddress("Jet_neEmEF", Jet_neEmEF);
    fChain->SetBranchAddress("Jet_neHEF", Jet_neHEF);
    fChain->SetBranchAddress("Jet_qgl", Jet_qgl);
    fChain->SetBranchAddress("Jet_rawFactor", Jet_rawFactor);
    fChain->SetBranchAddress("Jet_jetId", Jet_jetId);
    fChain->SetBranchAddress("Jet_nMuons", Jet_nMuons);

    //fChain->SetBranchAddress("Jet_nConstituents", Jet_nConstituents);
    //fChain->SetBranchAddress("Jet_puId", Jet_puId);

    //fChain->SetBranchAddress("fixedGridRhoFastjetAll", &fixedGridRhoFastjetAll);
    //fChain->SetBranchAddress("fixedGridRhoFastjetCentral", &fixedGridRhoFastjetCentral);
    //fChain->SetBranchAddress("fixedGridRhoFastjetCentralCalo", &fixedGridRhoFastjetCentralCalo);
    //fChain->SetBranchAddress("fixedGridRhoFastjetCentralChargedPileUp", &fixedGridRhoFastjetCentralChargedPileUp);
    //fChain->SetBranchAddress("fixedGridRhoFastjetCentralNeutral", &fixedGridRhoFastjetCentralNeutral);
    //fChain->SetBranchAddress("nOtherPV", &nOtherPV);
    //fChain->SetBranchAddress("OtherPV_z", OtherPV_z);
    //fChain->SetBranchAddress("PV_ndof", &PV_ndof);
    //fChain->SetBranchAddress("PV_x", &PV_x);
    //fChain->SetBranchAddress("PV_y", &PV_y);
    //fChain->SetBranchAddress("PV_z", &PV_z);
    //fChain->SetBranchAddress("PV_chi2", &PV_chi2);
    //fChain->SetBranchAddress("PV_score", &PV_score);
    //fChain->SetBranchAddress("PV_npvs", &PV_npvs);
    //fChain->SetBranchAddress("PV_npvsGood", &PV_npvsGood);
    fChain->SetBranchAddress("Flag_HBHENoiseFilter", &Flag_HBHENoiseFilter);
    fChain->SetBranchAddress("Flag_HBHENoiseIsoFilter", &Flag_HBHENoiseIsoFilter);
    fChain->SetBranchAddress("Flag_CSCTightHaloFilter", &Flag_CSCTightHaloFilter);
    fChain->SetBranchAddress("Flag_CSCTightHaloTrkMuUnvetoFilter", &Flag_CSCTightHaloTrkMuUnvetoFilter);
    fChain->SetBranchAddress("Flag_CSCTightHalo2015Filter", &Flag_CSCTightHalo2015Filter);
    fChain->SetBranchAddress("Flag_globalTightHalo2016Filter", &Flag_globalTightHalo2016Filter);
    fChain->SetBranchAddress("Flag_globalSuperTightHalo2016Filter", &Flag_globalSuperTightHalo2016Filter);
    fChain->SetBranchAddress("Flag_HcalStripHaloFilter", &Flag_HcalStripHaloFilter);
    fChain->SetBranchAddress("Flag_hcalLaserEventFilter", &Flag_hcalLaserEventFilter);
    fChain->SetBranchAddress("Flag_EcalDeadCellTriggerPrimitiveFilter", &Flag_EcalDeadCellTriggerPrimitiveFilter);
    fChain->SetBranchAddress("Flag_EcalDeadCellBoundaryEnergyFilter", &Flag_EcalDeadCellBoundaryEnergyFilter);
    fChain->SetBranchAddress("Flag_ecalBadCalibFilter", &Flag_ecalBadCalibFilter);
    fChain->SetBranchAddress("Flag_goodVertices", &Flag_goodVertices);
    fChain->SetBranchAddress("Flag_eeBadScFilter", &Flag_eeBadScFilter);
    fChain->SetBranchAddress("Flag_ecalLaserCorrFilter", &Flag_ecalLaserCorrFilter);
    fChain->SetBranchAddress("Flag_trkPOGFilters", &Flag_trkPOGFilters);
    fChain->SetBranchAddress("Flag_chargedHadronTrackResolutionFilter", &Flag_chargedHadronTrackResolutionFilter);
    fChain->SetBranchAddress("Flag_muonBadTrackFilter", &Flag_muonBadTrackFilter);
    fChain->SetBranchAddress("Flag_BadChargedCandidateFilter", &Flag_BadChargedCandidateFilter);
    fChain->SetBranchAddress("Flag_BadPFMuonFilter", &Flag_BadPFMuonFilter);
    fChain->SetBranchAddress("Flag_BadChargedCandidateSummer16Filter", &Flag_BadChargedCandidateSummer16Filter);
    fChain->SetBranchAddress("Flag_BadPFMuonSummer16Filter", &Flag_BadPFMuonSummer16Filter);
    fChain->SetBranchAddress("Flag_trkPOG_manystripclus53X", &Flag_trkPOG_manystripclus53X);
    fChain->SetBranchAddress("Flag_trkPOG_toomanystripclus53X", &Flag_trkPOG_toomanystripclus53X);
    fChain->SetBranchAddress("Flag_trkPOG_logErrorTooManyClusters", &Flag_trkPOG_logErrorTooManyClusters);
    fChain->SetBranchAddress("Flag_METFilters", &METFilters);

    fChain->SetBranchAddress("nPhoton", &nPhoton);
    fChain->SetBranchAddress("Photon_pt", Photon_pt);
    fChain->SetBranchAddress("Photon_eta", Photon_eta);
    fChain->SetBranchAddress("Photon_phi", Photon_phi);
    fChain->SetBranchAddress("Photon_eCorr", Photon_eCorr);
    fChain->SetBranchAddress("Photon_energyErr", Photon_energyErr);
    fChain->SetBranchAddress("Photon_hoe", Photon_hoe);
    fChain->SetBranchAddress("Photon_mvaID", Photon_mvaID);
    fChain->SetBranchAddress("Photon_mvaID17", Photon_mvaID17);
    fChain->SetBranchAddress("Photon_pfRelIso03_all", Photon_pfRelIso03_all);
    fChain->SetBranchAddress("Photon_pfRelIso03_chg", Photon_pfRelIso03_chg);
    fChain->SetBranchAddress("Photon_r9", Photon_r9);
    fChain->SetBranchAddress("Photon_sieie", Photon_sieie);
    //fChain->SetBranchAddress("Photon_cutBased", Photon_cutBased);
    //fChain->SetBranchAddress("Photon_cutBased17Bitmap", Photon_cutBased17Bitmap);
    fChain->SetBranchAddress("Photon_jetIdx", Photon_jetIdx);
    fChain->SetBranchAddress("Photon_vidNestedWPBitmap", Photon_vidNestedWPBitmap);
    fChain->SetBranchAddress("Photon_electronVeto", Photon_electronVeto);
    //fChain->SetBranchAddress("Photon_isScEtaEB", Photon_isScEtaEB);
    //fChain->SetBranchAddress("Photon_isScEtaEE", Photon_isScEtaEE);
    //fChain->SetBranchAddress("Photon_mvaID17_WP80", Photon_mvaID17_WP80);
    //fChain->SetBranchAddress("Photon_mvaID17_WP90", Photon_mvaID17_WP90);
    //fChain->SetBranchAddress("Photon_mvaID_WP80", Photon_mvaID_WP80);
    //fChain->SetBranchAddress("Photon_mvaID_WP90", Photon_mvaID_WP90);
    //fChain->SetBranchAddress("Photon_pixelSeed", Photon_pixelSeed);
    //fChain->SetBranchAddress("Photon_seedGain", Photon_seedGain);
    if (mIsDT) {
        fChain->SetBranchAddress("HLT_Photon22", &HLT_Photon22);
        fChain->SetBranchAddress("HLT_Photon30", &HLT_Photon30);
        fChain->SetBranchAddress("HLT_Photon36", &HLT_Photon36);
        fChain->SetBranchAddress("HLT_Photon50", &HLT_Photon50);
        fChain->SetBranchAddress("HLT_Photon75", &HLT_Photon75);
        fChain->SetBranchAddress("HLT_Photon90", &HLT_Photon90);
        fChain->SetBranchAddress("HLT_Photon120", &HLT_Photon120);
        fChain->SetBranchAddress("HLT_Photon175", &HLT_Photon175);
        fChain->SetBranchAddress("HLT_Photon500", &HLT_Photon500);
        fChain->SetBranchAddress("HLT_Photon600", &HLT_Photon600);
        fChain->SetBranchAddress("HLT_Photon22_R9Id90_HE10_IsoM", &mP22);
        fChain->SetBranchAddress("HLT_Photon30_R9Id90_HE10_IsoM", &mP30);
        fChain->SetBranchAddress("HLT_Photon36_R9Id90_HE10_IsoM", &mP36);
        fChain->SetBranchAddress("HLT_Photon50_R9Id90_HE10_IsoM", &mP50);
        fChain->SetBranchAddress("HLT_Photon75_R9Id90_HE10_IsoM", &mP75);
        fChain->SetBranchAddress("HLT_Photon90_R9Id90_HE10_IsoM", &mP90);
        fChain->SetBranchAddress("HLT_Photon120_R9Id90_HE10_IsoM", &mP120);
        fChain->SetBranchAddress("HLT_Photon165_R9Id90_HE10_IsoM", &mP165);
    }

    fChain->SetBranchStatus("*",0);
    if (!mIsDT) {
        fChain->SetBranchStatus("nGenJet",1);
        fChain->SetBranchStatus("GenJet_eta",1);
        fChain->SetBranchStatus("GenJet_mass",1);
        fChain->SetBranchStatus("GenJet_phi",1);
        fChain->SetBranchStatus("GenJet_pt",1);
        fChain->SetBranchStatus("nGenPart",1);
        fChain->SetBranchStatus("GenPart_eta",1);
        fChain->SetBranchStatus("GenPart_mass",1);
        fChain->SetBranchStatus("GenPart_phi",1);
        fChain->SetBranchStatus("GenPart_pt",1);
        fChain->SetBranchStatus("GenPart_genPartIdxMother",1);
        fChain->SetBranchStatus("GenPart_pdgId",1);
        fChain->SetBranchStatus("GenPart_status",1);
        fChain->SetBranchStatus("GenPart_statusFlags",1);
    }
    fChain->SetBranchStatus("Jet_pt",1);
    fChain->SetBranchStatus("Jet_eta",1);
    fChain->SetBranchStatus("Jet_phi",1);
    fChain->SetBranchStatus("Jet_mass",1);
    fChain->SetBranchStatus("Jet_jetId",1);
    fChain->SetBranchStatus("Jet_qgl",1);
    fChain->SetBranchStatus("Jet_nMuons",1);
    fChain->SetBranchStatus("Jet_rawFactor",1);
    fChain->SetBranchStatus("Jet_chEmEF",1);
    fChain->SetBranchStatus("Jet_chHEF",1);
    fChain->SetBranchStatus("Jet_muEF",1);
    fChain->SetBranchStatus("Jet_neEmEF",1);
    fChain->SetBranchStatus("Jet_neHEF",1);
    fChain->SetBranchStatus("Flag_METFilters",1);
    fChain->SetBranchStatus("nPhoton",1);
    fChain->SetBranchStatus("Photon_pt",1);
    fChain->SetBranchStatus("Photon_eta",1);
    fChain->SetBranchStatus("Photon_phi",1);
    fChain->SetBranchStatus("Photon_hoe",1);
    fChain->SetBranchStatus("Photon_mvaID",1);
    fChain->SetBranchStatus("Photon_mvaID17",1);
    fChain->SetBranchStatus("Photon_pfRelIso03_all",1);
    fChain->SetBranchStatus("Photon_pfRelIso03_chg",1);
    fChain->SetBranchStatus("Photon_r9",1);
    fChain->SetBranchStatus("Photon_sieie",1);
    fChain->SetBranchStatus("Photon_cutBased",1);
    fChain->SetBranchStatus("Photon_jetIdx",1);
    fChain->SetBranchStatus("Photon_electronVeto",1);
    //fChain->SetBranchStatus("Photon_mvaID17_WP80",1);
    //fChain->SetBranchStatus("Photon_mvaID17_WP90",1);
    //fChain->SetBranchStatus("Photon_mvaID_WP80",1);
    //fChain->SetBranchStatus("Photon_mvaID_WP90",1);
    fChain->SetBranchStatus("PuppiMET_phi", 1);
    fChain->SetBranchStatus("PuppiMET_pt", 1);
    fChain->SetBranchStatus("RawMET_phi", 1);
    fChain->SetBranchStatus("RawMET_pt", 1);
    fChain->SetBranchStatus("GenMET_phi", 1);
    fChain->SetBranchStatus("GenMET_pt", 1);
    fChain->SetBranchStatus("MET_phi", 1);
    fChain->SetBranchStatus("MET_pt", 1);
    fChain->SetBranchStatus("ChsMET_phi", 1);
    fChain->SetBranchStatus("ChsMET_pt", 1);
    fChain->SetBranchStatus("ChsMET_sumEt", 1);
    fChain->SetBranchStatus("genWeight", 1);
    if (mIsDT) {
        fChain->SetBranchStatus("HLT_Photon22_R9Id90_HE10_IsoM",1);
        fChain->SetBranchStatus("HLT_Photon30_R9Id90_HE10_IsoM",1);
        fChain->SetBranchStatus("HLT_Photon36_R9Id90_HE10_IsoM",1);
        fChain->SetBranchStatus("HLT_Photon50_R9Id90_HE10_IsoM",1);
        fChain->SetBranchStatus("HLT_Photon75_R9Id90_HE10_IsoM",1);
        fChain->SetBranchStatus("HLT_Photon90_R9Id90_HE10_IsoM",1);
        fChain->SetBranchStatus("HLT_Photon120_R9Id90_HE10_IsoM",1);
        fChain->SetBranchStatus("HLT_Photon165_R9Id90_HE10_IsoM",1);
        fChain->SetBranchStatus("HLT_Photon22",1);
    }
}

void MPF::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
#endif // #ifdef MPF_cxx
