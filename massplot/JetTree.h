//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Sep 19 09:19:13 2016 by ROOT version 6.07/07
// from TTree JetTree/Tree with jet data
// found on file: pythia8_ttbarjet_ghostphys_1000000.root
//////////////////////////////////////////////////////////

#ifndef JetTree_h
#define JetTree_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "Math/GenVector/PxPyPzE4D.h"

class JetTree {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.
   static const Int_t kMaxfJets = 100;

   // Declaration of leaf types
 //JetEvent        *event;
   Int_t           fJets;
   Double_t        fX[kMaxfJets];   //[fJets_]
   Double_t        fY[kMaxfJets];   //[fJets_]
   Double_t        fZ[kMaxfJets];   //[fJets_]
   Double_t        fT[kMaxfJets];   //[fJets_]
   Int_t           fFlav[kMaxfJets];   //[fJets_]
   Float_t         fChf[kMaxfJets];   //[fJets_]
   Float_t         fNhf[kMaxfJets];   //[fJets_]
   Float_t         fPhf[kMaxfJets];   //[fJets_]
   Float_t         fElf[kMaxfJets];   //[fJets_]
   Float_t         fMuf[kMaxfJets];   //[fJets_]
   Float_t         fJets_fPartonPT[kMaxfJets];   //[fJets_]
   Float_t         fJets_fMatchPT[kMaxfJets];   //[fJets_]
   Int_t           fJets_fConstituents[kMaxfJets];   //[fJets_]
   Float_t         fJets_fPTD[kMaxfJets];   //[fJets_]
   Float_t         fJets_fSigma2[kMaxfJets];   //[fJets_]
   Float_t         fJets_fDR[kMaxfJets];   //[fJets_]
   Float_t         fJets_fAlpha[kMaxfJets];   //[fJets_]
   Float_t         fJets_fDPhi[kMaxfJets];   //[fJets_]
   Double_t        fWeight;

   // List of branches
   TBranch        *b_event_fJets_;   //!
   TBranch        *b_fJets_fP4_fCoordinates_fX;   //!
   TBranch        *b_fJets_fP4_fCoordinates_fY;   //!
   TBranch        *b_fJets_fP4_fCoordinates_fZ;   //!
   TBranch        *b_fJets_fP4_fCoordinates_fT;   //!
   TBranch        *b_fJets_fFlav;   //!
   TBranch        *b_fJets_fChf;   //!
   TBranch        *b_fJets_fNhf;   //!
   TBranch        *b_fJets_fPhf;   //!
   TBranch        *b_fJets_fElf;   //!
   TBranch        *b_fJets_fMuf;   //!
   TBranch        *b_fJets_fPartonPT;   //!
   TBranch        *b_fJets_fMatchPT;   //!
   TBranch        *b_fJets_fConstituents;   //!
   TBranch        *b_fJets_fPTD;   //!
   TBranch        *b_fJets_fSigma2;   //!
   TBranch        *b_fJets_fDR;   //!
   TBranch        *b_fJets_fAlpha;   //!
   TBranch        *b_fJets_fDPhi;   //!
   TBranch        *b_event_fWeight;   //!

   JetTree(TTree *tree=0);
   virtual ~JetTree();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef JetTree_cxx
JetTree::JetTree(TTree *tree) : fChain(0)
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("pythia8_ttbarjet_ghostphys_1000000.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("pythia8_ttbarjet_ghostphys_1000000.root");
      }
      f->GetObject("JetTree",tree);

   }
   Init(tree);
}

JetTree::~JetTree()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t JetTree::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t JetTree::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void JetTree::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("fJets", &fJets, &b_event_fJets_);
   fChain->SetBranchAddress("fJets.fP4.fCoordinates.fX", fX, &b_fJets_fP4_fCoordinates_fX);
   fChain->SetBranchAddress("fJets.fP4.fCoordinates.fY", fY, &b_fJets_fP4_fCoordinates_fY);
   fChain->SetBranchAddress("fJets.fP4.fCoordinates.fZ", fZ, &b_fJets_fP4_fCoordinates_fZ);
   fChain->SetBranchAddress("fJets.fP4.fCoordinates.fT", fT, &b_fJets_fP4_fCoordinates_fT);
   fChain->SetBranchAddress("fJets.fFlav", fFlav, &b_fJets_fFlav);
   fChain->SetBranchAddress("fJets.fChf", fChf, &b_fJets_fChf);
   fChain->SetBranchAddress("fJets.fNhf", fNhf, &b_fJets_fNhf);
   fChain->SetBranchAddress("fJets.fPhf", fPhf, &b_fJets_fPhf);
   fChain->SetBranchAddress("fJets.fElf", fElf, &b_fJets_fElf);
   fChain->SetBranchAddress("fJets.fMuf", fMuf, &b_fJets_fMuf);
   fChain->SetBranchAddress("fJets.fPartonPT", fJets_fPartonPT, &b_fJets_fPartonPT);
   fChain->SetBranchAddress("fJets.fMatchPT", fJets_fMatchPT, &b_fJets_fMatchPT);
   fChain->SetBranchAddress("fJets.fConstituents", fJets_fConstituents, &b_fJets_fConstituents);
   fChain->SetBranchAddress("fJets.fPTD", fJets_fPTD, &b_fJets_fPTD);
   fChain->SetBranchAddress("fJets.fSigma2", fJets_fSigma2, &b_fJets_fSigma2);
   fChain->SetBranchAddress("fJets.fDR", fJets_fDR, &b_fJets_fDR);
   fChain->SetBranchAddress("fJets.fAlpha", fJets_fAlpha, &b_fJets_fAlpha);
   fChain->SetBranchAddress("fJets.fDPhi", fJets_fDPhi, &b_fJets_fDPhi);
   fChain->SetBranchAddress("fWeight", &fWeight, &b_event_fWeight);
   Notify();
}

Bool_t JetTree::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void JetTree::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t JetTree::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef JetTree_cxx
